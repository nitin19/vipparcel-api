<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStoreOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('store_orders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('store_id');
            $table->dateTime('order_date')->nullable();
            $table->dateTime('shipped_date')->nullable();
            $table->string('order_id', 191)->nullable();
            $table->text('order_details')->nullable();
            $table->string('order_status', 191)->nullable();
            $table->string('label_id', 191)->nullable();
            $table->boolean('processed_by_owner')->nullable();
            $table->integer('processor_id')->nullable();
            $table->string('tracking_id ', 191)->nullable();
            $table->text('shipping_address ', 191)->nullable();
            $table->string('customer_name', 191)->nullable();
            $table->string('customer_email', 191)->nullable();
            $table->string('customer_phone', 191)->nullable();
            $table->enum('status',['1', '0'])->default('1')->comment="1=Active,0=Deactive";
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('store_orders');
    }
}

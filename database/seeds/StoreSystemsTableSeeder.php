<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;


class StoreSystemsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('store_systems')->insert([
            'name' => 'amazon',
            'title' => 'Amazon',
            'logo_image' => 'amazon.jpg',
            'status' => '1',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
         DB::table('store_systems')->insert([
            'name' => 'ebay',
            'title' => 'eBay',
            'logo_image' => 'ebay.jpg',
            'status' => '1',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
         DB::table('store_systems')->insert([
            'name' => 'shopify',
            'logo_image' => 'shopify.jpg',
            'title' => 'Shopify',
            'status' => '1',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
         DB::table('store_systems')->insert([
            'name' => 'magento',
            'title' => 'Magento',
            'logo_image' => 'magento.jpg',
            'status' => '1',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('store_systems')->insert([
            'name' => 'X-cart',
            'title' => 'X-cart',
            'logo_image' => 'X-cart.jpg',
            'status' => '1',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
    }
}

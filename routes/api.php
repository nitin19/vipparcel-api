<?php

use Illuminate\Http\Request;
use Illuminate\Support\Str;

Route::group(['middleware' => ['api','cors']], function ($router) {

  Route::post('login', 'Api\AuthController@login');
  Route::post('logout', 'Api\AuthController@logout');
  Route::post('register', 'Api\AuthController@register');
  Route::get('getCountries', 'Api\CountryController@get_Countries');
  Route::get('getStates', 'Api\StateController@get_States');

  Route::post('/password/create', 'Api\PasswordResetController@create');
  Route::get('/password/find/{token}', 'Api\PasswordResetController@find');
  Route::post('/password/reset', 'Api\PasswordResetController@reset');
  Route::get('/payment/status/{userid}', 'Api\Payment\PaymentController@getPaymentStatus');
  
});

Route::group(['middleware' => ['jwt.verify','cors']], function() {

    Route::post('refresh', 'Api\AuthController@refresh');
    Route::post('verify-sms', 'Api\AuthController@smsVerify');
    Route::post('resend-sms', 'Api\AuthController@resendSms');

    Route::get('user', 'Api\UserController@getAuthenticatedUser');
    Route::put('update', 'Api\UserController@put_update');
    Route::post('requesttoken', 'Api\UserController@post_apitoken');
    Route::get('gettoken', 'Api\UserController@get_apitoken');

    Route::get('/accounts/card/getList', 'Api\Creditcard\CardsInfoController@getList');
    Route::post('/accounts/card/add', 'Api\Creditcard\CardsInfoController@create');
    Route::post('/accounts/card/verify', 'Api\Creditcard\CardsInfoController@card_verification');
    Route::post('/accounts/card/chargeverify', 'Api\Creditcard\CardsInfoController@charge_verification');
    Route::get('/accounts/card/iscardverified', 'Api\Creditcard\CardsInfoController@IsCardVerified');
    Route::get('/accounts/card/details/', 'Api\Creditcard\CardsInfoController@getdetails');
    Route::post('/accounts/card/delete', 'Api\Creditcard\CardsInfoController@deletecard');
    Route::post('/accounts/card/default', 'Api\Creditcard\CardsInfoController@defaultcard');
    Route::get('/accounts/card/getverifycard', 'Api\Creditcard\CardsInfoController@getverifycard');

    Route::post('/payment/paypal', 'Api\Payment\PaymentController@payWithpaypal');
    Route::post('/payment/card', 'Api\Payment\PaymentController@payWithcards');
    Route::get('/payment/balance', 'Api\Payment\PaymentController@getbalance');
    
});

    Route::prefix('v1')->namespace('Api\Shipping')->group(function () {
      Route::fallback(function(Request $request){
                    $uri = $request->path();
                    $response = [
                      'requestId' => strtolower(Str::random(30)),
                      'statusCode'    => 405,
                      'error' => 'The requested URL '. $uri .' was not found on this server.',
                      ];
                      return response()->json($response);
      });
      Route::middleware('APIToken')->group(function () {
          
          Route::get('/shipping/label/getList', 'LabelController@get_getList');
          Route::get('/shipping/label/getInfo/{id}', 'LabelController@get_getInfo');
          Route::get('/shipping/label/getImages/{id}', 'LabelController@get_getImages');
          Route::post('/shipping/label/calculate', 'LabelController@action_post_calculate');
          Route::post('/shipping/label/print', 'LabelController@action_post_print');


          Route::post('/shipping/scanForm/create', 'ScanformController@post_create');
          Route::get('/shipping/scanForm/getLabels', 'ScanformController@get_getLabels');
          Route::get('/shipping/scanForm/getInfo/{id}', 'ScanformController@get_getInfo');
          Route::get('/shipping/scanForm/getList', 'ScanformController@get_getList');
      
          Route::get('/shipping/refund/getList', 'RefundController@get_getList');
          Route::get('/shipping/refund/getInfo/{id}', 'RefundController@get_getInfo');
          Route::get('/shipping/refund/getLabels', 'RefundController@get_getLabels');
          Route::post('/shipping/refund/request', 'RefundController@post_request');

          Route::get('/shipping/pickup/getList', 'PickupController@get_getList');
          Route::get('/shipping/pickup/getLabels', 'PickupController@get_getLabels');
          Route::get('/shipping/pickup/getInfo/{id}', 'PickupController@get_getInfo');
          Route::post('/shipping/pickup/request', 'PickupController@post_request');
      });
      Route::get('/shipping/label/mailClasses', 'LabelController@get_mailClasses');
    });

    Route::prefix('v1')->namespace('Api\Account')->group(function () {
      Route::fallback(function(Request $request){
                  $uri = $request->path();
                  $response = [
                    'requestId' => strtolower(Str::random(30)),
                    'statusCode'    => 405,
                    'error' => 'The requested URL '. $uri .' was not found on this server.',
                    ];
                    return response()->json($response);
      });
      Route::middleware('APIToken')->group(function () {
        Route::get('/account/personalInfo/details', 'PersonalInfoController@get_details');
        Route::put('/account/personalInfo/update', 'PersonalInfoController@put_update');
        Route::get('/account/balance/getHistory', 'BalanceController@get_getHistory');
        Route::get('/account/balance/getCurrent', 'BalanceController@get_getCurrent');
        Route::get('/account/address/getList', 'AddressController@get_getList');
        Route::get('/account/address/getInfo/{id}', 'AddressController@get_getInfo');
        Route::delete('/account/address/delete/{id}', 'AddressController@delete_delete');
        Route::post('/account/address/create', 'AddressController@post_create');
        Route::put('/account/address/update/{id}', 'AddressController@put_update');
      });
    });

    Route::prefix('v1')->namespace('Api\Location')->group(function () {
      Route::fallback(function(Request $request){
                  $uri = $request->path();
                  $response = [
                    'requestId' => strtolower(Str::random(30)),
                    'statusCode'    => 405,
                    'error' => 'The requested URL '. $uri .' was not found on this server.',
                    ];
                    return response()->json($response);
      });
      Route::get('/location/country/getList', 'CountryController@get_getList');
      Route::get('/location/state/getList', 'StateController@get_getList');
    });


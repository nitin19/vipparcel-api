<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Payments extends Model
{
  const CREATED_AT = 'created';

  const UPDATED_AT = 'updated';

  protected $table = 'payments';

  protected $fillable = ['from_user_id', 'to_user_id', 'amount', 'description', 'created', 'updated', 'type', 'update_user_id', 'commission', 'txn_id', 'txn_ppref', 'discount', 'balance', 'old_balance', 'commission_additional', 'refund_amount', 'linked_payment_id', 'visible_for_client'];

    
}

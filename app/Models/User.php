<?php

namespace App\Models;

use Tymon\JWTAuth\Contracts\JWTSubject;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Http\Resources\UserResource;

use App\Models\UserRole;
use App\Models\Profile;
use Carbon;

class User extends Authenticatable implements JWTSubject
{
  use Notifiable;

  protected $table = 'users';
  
  const CREATED_AT = 'created';

  const UPDATED_AT = 'updated';

  protected $fillable = [
    'username', 'email', 'password', 'created', 'updated', 'active', 'active_code', 'new_pass_code',
    'view_message', 'hear_about_us', 'insurance_commission', 'domestic_commission_additional',
    'disable_check_print_label', 'balance', 'cnt_labels', 'inheritance_ignore_fields', 'phone_verification_request_id', 'type', 'verified', 'provider_account', 'referrer_url', 'coupon_code_id', 'register_ipinfo_id',
  ];

  protected $hidden = [
      'password', 'remember_token',
  ];

  public function profile()
  {
    return $this->hasOne(Profile::class);
  }
  
  public function getJWTIdentifier()
  {
      return $this->getKey();
  }

  public function getJWTCustomClaims()
  {
      return [];
  }

}

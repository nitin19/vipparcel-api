<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Models\User;
use App\Models\Profile;
use App\Http\Resources\ProfileResource;

class UserResource extends JsonResource
{
  /**
   * Transform the resource into an array.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return array
   */
  public function toArray($request)
  {
    return [
      'id' => $this->id,
      'username' => $this->username,
      'email' => $this->email,        
      'profile'  => new ProfileResource($this->profile),
      'balance' => $this->balance,
      'active' => $this->active,
      'logins' => $this->logins,
      'sms_request_id' => $this->phone_verification_request_id,
      'created' => $this->created->format('Y-m-d H:i:s'),
      'type' => 'US',
    ];  
  }
}

<?php
namespace App\Http\Services;

require 'vendor/autoload.php';
use App\Http\Controllers\Controller; 
use Illuminate\Support\Facades\Redirect;
use App\Models\Store; 
use App\Models\StoreOrder;
use App\Models\StoreOrderItem;
use Illuminate\Http\Request; 
use \DTS\eBaySDK\Trading\Services;
use \DTS\eBaySDK\Trading\Types;
use \DTS\eBaySDK\Trading\Enums;
use Validator;
use URL;
use Shopify;
use Illuminate\Support\Facades\Auth; 
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Config;
use Bnmetrics\laravelshopifyapi;

trait Shopifyservice
{

  public $successStatus = 200;
  private $config;
  private $request;
  protected $shop = "vipparcel-2.myshopify.com";
  protected $foo;
  protected $scopes = ['read_orders','write_orders'];


    /**
     * Save Store Credentials for Shopify
     *
     *@params array [ varchar $storename , varchar $code ], int $userId
     *@return json [Boolean msg, Boolean status]
     */
     /*public function shopifySaveStoreCredentials($data,$userId){
         $validator = Validator::make($data, [ 
           'storeName' => 'required',
           'code' => 'required',
            
         ]);
         
         if ($validator->fails()) { 
         	return response()->json(['error'=>$validator->errors()], $this->errorStatus);            
        }
         else{   
	       	 $code = "v^1.1".$data['code'];
	      	 $auth = $this->ebayAuthAccepted($code);

	      	 if(!$auth){
	      	 	$msg = "Something Wrong Happend.";
	      	 	return response()->json(['msg'=>$msg], $this->errorStatus);
	      	 }

           $storeName = $this->formatEbayName($data['storeName']);
	         $stores = new Store;        
	         $stores->owner_id = $userId;
	         $stores->store_system_id = 2;
	         $stores->slug = rand(34,98).time().rand(340,99999);
	         $stores->auth = $auth;
	         $stores->store_name = $storeName;
	         $stores->status = 1;
	         $stores->save();
	         $success = 1;
	         return response()->json(['success'=>$success], $this->successStatus); 
        }
     }*/

    /**
     * Provide Shopify Authroize url to redirect user on Shopify
     *
     *@params varchar $storename
     *@return json [varchar url, Boolean status]
     */
     public function shopifyGetAuthrozieRedirectUrl($data,$userId){
         $user = Auth::user();
         $state = $data['storeName'].'-'.$user->id;
         $store_domain_name = $this->shop;
         $store_scopes = implode(',', $this->scopes);
         $clientID = env('SHOPIFY_KEY');
         $clientSecret = env('SHOPIFY_SECRET');
         $redirectUrl = env('SHOPIFY_REDIRECT');
         $url = "https://{$store_domain_name}/admin/oauth/authorize?client_id={$clientID}&redirect_uri={$redirectUrl}&scope={$store_scopes}&response_type=code&state={$state}";
         return response()->json(['url' => $url], $this->successStatus);    
  	 }
  
  	
    /**
     * Ebay seller has Authroize our Ebay developer account
     *
     *@params varchar $authCode
     *@return Boolean true/false
     */
     /*public function ebayAuthAccepted($authCode){
		     $clientID = config('ebay.clientID');
		     $clientSecret = config('ebay.clientSecret');
		     $ruName = config('ebay.ruName');
			   $url = config('ebay.authToeknUrl');
			 $headers = [
		        'Content-Type: application/x-www-form-urlencoded',
		        'Authorization: Basic '.base64_encode($clientID.':'.$clientSecret)
		     ];
		     $body = http_build_query([
		        'grant_type'   => 'authorization_code',
		        'code'         => $authCode,
		        'redirect_uri' => $ruName
		     ]);
		     $curl = curl_init();
		     curl_setopt_array($curl, array(
		        CURLOPT_URL            => $url,
		        CURLOPT_RETURNTRANSFER => true,
		        CURLOPT_CUSTOMREQUEST  => 'POST',
		        CURLOPT_POSTFIELDS     => $body,
		        CURLOPT_HTTPHEADER     => $headers
		     ));
		     $response = curl_exec($curl);
		     $err = curl_error($curl);
		     curl_close($curl);

		     if ($err) {
		        return false;
		     } else {
		        return $response;
		     }
     }*/


  /**
     * Ebay token is short lived . So we refresh it everytime we use its services
     *
     *@params varchar $refreshToken
     *@return Boolean false, varchar $refreshToken;
     */
     /*public function ebayAuthRefreshToken($refreshToken){
		     
		     $clientID = config('ebay.clientID');
		     $clientSecret = config('ebay.clientSecret');
		     $ruName = config('ebay.ruName');
		     $rT = $refreshToken;
		     $url   =  config('ebay.authToeknUrl');
		     $scope =  config('ebay.scope');
		     
		     $headers = [
		        'Content-Type: application/x-www-form-urlencoded',
		        'Authorization: Basic '.base64_encode($clientID.':'.$clientSecret)
		     ];
		     
		     $body = http_build_query([
		        'grant_type'    => 'refresh_token',
		        'refresh_token' => $rT,
		        'scope'  => $scope
		     ]);
		     
		     $curl = curl_init();
		     
		     curl_setopt_array($curl, array(
		        CURLOPT_URL            => $url,
		        CURLOPT_RETURNTRANSFER => true,
		        CURLOPT_CUSTOMREQUEST  => 'POST',
		        CURLOPT_POSTFIELDS     => $body,
		        CURLOPT_HTTPHEADER     => $headers
		     ));
		     
		     $response = curl_exec($curl);		     
		     $err = curl_error($curl);		     
		     curl_close($curl);	 

		     if($err) {
		        return false;
		     }else {
            $responseMessage = json_decode($response);
		        return $responseMessage->access_token;
		     }    

     }*/

   /**
     * Pull Ebay Orders from its stores
     *
     *@params int $storeId
     *@return array [$numberoforder, $numberoforderItem, $status];
     */
 	 public function shopifyGetShopifyOrders($storeId) { 
         $clientID = env('SHOPIFY_KEY');
         $clientSecret = env('SHOPIFY_SECRET');
         $ruName = env('SHOPIFY_REDIRECT');
         $stores = Store::find($storeId);
         $auth = json_decode($stores->auth);
         $access_token = $auth->access_token;
         $this->foo = Shopify::retrieve($this->shop, $access_token);
         $products = $this->foo->get("orders", ["limit"=>20, "page" => 1]);

         //$user = $this->foo->auth()->getUser();
         print_r($products);
         exit();
         $fff = $this->foo->getProductsAll();
         //$fff = $this->foo->getAllOrders();

         //
         //$result = Shopify::get(['URL' => '/admin/orders.json']);
         /*print_r($fff);
         exit();*/

         /*$authorization = $this->ebayAuthRefreshToken($auth->refresh_token);

   		   $startDate = date('Y-m-d',strtotime("-15 days"));

         $endDate = date('Y-m-d');


   		   if(!$authorization){
      			$msg = "Something Wrong Happend.";
   	 			  return response()->json(['msg'=>$msg], $this->errorStatus);
      		}
         
         $service = new Services\TradingService(array(              
              'authorization' => $authorization,
              'apiVersion' => $this->config['tradingApiVersion'],
              'siteId' => 0,
              'sandbox'=> $sandbox,
              'credentials' => [
              'appId'  => $clientID,
              'certId' => $clientSecret,
              'devId'  => $devID
            ]
         ));

         $request = new Types\GetMyeBaySellingRequestType();        
         $args = array(
           "OrderStatus"   => "All",
           "SortingOrder"  => "Ascending",*/
           //"OrderRole"     => "Seller",
           /*"CreateTimeFrom"   => new \DateTime($startDate),
           "CreateTimeTo"   => new \DateTime($endDate),
         );
         
         $getOrders = new Types\GetOrdersRequestType($args); 
         $getOrders->RequesterCredentials = new Types\CustomSecurityHeaderType(); 
         $getOrders->IncludeFinalValueFee = true;
         $getOrders->Pagination = new Types\PaginationType();
         $getOrders->Pagination->EntriesPerPage = 10;*/
         
         //$getOrders->OrderIDArray = new Types\OrderIDArrayType();
         
         /*$getOrdersPageNum = 1;
         $response = $service->getOrders($getOrders);        
         if($response->Ack =='Failure'){
           $message = "Some error occured";
           return array("status" => 0 ,"message" => $message );
         }
         $numberofOrders = 0;
         $numberofOrderItems = 0;*/
         // echo count($response->OrderArray->Order);
         /*foreach ($response->OrderArray->Order as $order) {
           $storeId = $storeId; 
           $orderDate = $order->CreatedTime; 
           $orderId = $order->OrderID;             
           $haverecords = StoreOrder::wherestoreId($storeId)->whereorderId($orderId)->count();
            if(!$haverecords){                
              $storeOrders = new StoreOrder;
              $orderDate = $order->CreatedTime->format('Y-m-d H:i:s');
              $storeOrders->order_date = trim(str_replace(['T', 'Z']," ",$orderDate));
              $storeOrders->order_id = $orderId;
              $storeOrders->store_id = $storeId;
              $val['PurchaseDate'] = $order->CreatedTime; 
              $val['OrderStatus'] = $order->OrderStatus; 
              $val['OrderTotal']["Amount"] = $order->Total->value; 
              $val['OrderTotal']["CurrencyCode"] = $order->Total->currencyID; 
              $val['ShippingService'] = $order->ShippingServiceSelected->ShippingService; 
              $val['ShippingAddress']["Name"] = $order->ShippingAddress->Name;
              $val['ShippingAddress']["Street1"] = $order->ShippingAddress->Street1;
              $val['ShippingAddress']["Street2"] = $order->ShippingAddress->Street2;
              $val['ShippingAddress']["CityName"] = $order->ShippingAddress->CityName;
              $val['ShippingAddress']["StateOrProvince"] = $order->ShippingAddress->StateOrProvince;
              $val['ShippingAddress']["Country"] = $order->ShippingAddress->Country;
              $val['ShippingAddress']["CountryName"] = $order->ShippingAddress->CountryName;
              $val['ShippingAddress']["Phone"] = $order->ShippingAddress->Phone;
              $val['ShippingAddress']["PostalCode"] = $order->ShippingAddress->PostalCode;
              $val['ShippingAddress']["AddressID"] = $order->ShippingAddress->AddressID;
              $val['ShippingAddress']["AddressOwner"] = $order->ShippingAddress->AddressOwner;
              $val['ShippingAddress']["ExternalAddressID"] = $order->ShippingAddress->ExternalAddressID;
              $val['BuyerEmail'] = $order->TransactionArray->Transaction[0]->Buyer->Email;
              $val['BuyerName'] = "Anatoliy Yanovskiy";
              $storeOrders->order_details = json_encode($val);                
              $storeOrders->save();
              $storeOrderId = $storeOrders->id;*/

              /* Storing Items in tables */
              /*foreach($order->TransactionArray->Transaction as $Items){
                        $Item = $Items->Item;
                        $sellersku = $Item->SKU;
                        $OrderItemId = $Item->ItemID;
                        $haverecords = StoreOrderItem::wherestoreOrderId($storeOrderId)->whereorderItemId($OrderItemId)->count();
                        $countOrderItem = 0;
                        if(!$haverecords)
                          {
                            $item_details['SellerSKU'] = $Item->SKU;
                            $item_details['OrderItemId'] = $Item->ItemID;
                            $item_details['Title'] = $Item->Title;
                            $item_details['QuantityOrdered'] = $Items->QuantityPurchased;
                            $item_details['ItemPrice']['Amount'] = $Items->TransactionPrice->value;
                            $item_details['ItemPrice']['CurrencyCode'] = $Items->TransactionPrice->currencyID;
                            $orderItemId = $Item->ItemID;
                            $storeItems = new StoreOrderItem;
                            $storeItems->store_order_id = $storeOrderId;
                            $storeItems->asin = 'NA';
                            $storeItems->sellersku = $Item->SKU;
                            $storeItems->order_item_id = $Item->ItemID;
                            $storeItems->item_details = json_encode($item_details);  
                            $storeItems->save();    
                            $countOrderItem ++;        
                         }
               }*/

              /*$numberofOrderItems += $countOrderItem;
              $numberofOrders++;
              }
              return array("status" => 1 ,"orders" =>$numberofOrders , "orderItems" => $numberofOrderItems);
          
         }*/
      

     }
 
     /**
     * After generating Labels we are marking prodcut as shipped
     *
     *@params int $storeId, varchar $carrierUsed, int $trackingNumber, int $orderId
     *@return Boolean 
     */
     /*public function ebayMarkedProductAsShipped($storeId,$carrierUsed,$trackingNumber,$orderId){
        $clientID = config('ebay.clientID');
        $clientSecret = config('ebay.clientSecret');
        $ruName = config('ebay.ruName');
        $devID = config('ebay.devID');
        $sandbox = config('ebay.sandbox');
        $stores = Store::find($storeId);
        $auth = json_decode($stores->auth);
        $authorization = $this->ebayAuthRefreshToken($auth->refresh_token);       
        
        if(!$authorization){
            $msg = "Something Wrong Happend.";
            return response()->json(['msg'=>$msg], $this->errorStatus);
        }  
        
        $service = new Services\TradingService(array(              
              'authorization' => $authorization,
              'apiVersion' => $this->config['tradingApiVersion'],
              'siteId' => 0,
              'sandbox'=> $sandbox,
              'credentials' => [
              'appId'  => $clientID,
              'certId' => $clientSecret,
              'devId'  => $devID
            ]
        ));

        $ship = new Types\ShipmentTrackingDetailsType();
        $ship->ShippingCarrierUsed = $carrierUsed;
        $ship->ShipmentTrackingNumber = $trackingNumber;

        $request = new Types\CompleteSaleRequestType();
        $request->OrderID = $orderId;
        $request->Shipment = new Types\ShipmentType();
        $request->Shipment->ShipmentTrackingDetails = [ $ship ];
        $request->MessageID = 'Order updated on: ' . date('Y-m-d H:i:s');
        $request->Shipped = TRUE;
        $response = $service->CompleteSale($request);
        if($response->Ack =='Success'){           
           return true;
        }else{
           return false;
        }

     }*/


     /*public function formatEbayName($store){

          $store = substr($store,7);

          return $store = substr($store,0,-15);


     }*/

     public function shopifySaveUserToken($data){


      $code = $data['code'];
      $query = array(
            "client_id" => env('SHOPIFY_KEY'), 
            "client_secret" => env('SHOPIFY_SECRET'), 
            "code" => $data['code']
      );
      $access_token_url = "https://" . $data['shop'] . "/admin/oauth/access_token";
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_URL, $access_token_url);
      curl_setopt($ch, CURLOPT_POST, count($query));
      curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($query));
      $access_token = curl_exec($ch);
      curl_close($ch);
      $UserTokenResponse =  $access_token;
      $TokenResponse = json_decode($UserTokenResponse);
      if($data['state'] && $data['state']!=''){
          $statedata = explode('-', $data['state']);
          $storename = $statedata[0];
          $userId = $statedata[1];
      } else {
          $storename = '';
          $userId = '0';
      }
      if(array_key_exists('access_token', $TokenResponse)){
          $stores = new Store;        
          $stores->owner_id = $userId;
          $stores->store_system_id = 3;
          $stores->slug = rand(34,98).time().rand(340,99999);
          $stores->auth = $UserTokenResponse;
          $stores->store_name = $storename;
          $stores->status = 1;
          $stores->save();
          return $response = [
          'requestId' => strtolower(Str::random(30)),
          'statusCode' => 201,
          'message' => "success",
          'status' => "1",
          ];
      } else {
          return $response = [
          'requestId' => strtolower(Str::random(30)),
          'statusCode' => 400,
          'message' => "error",
          'status' => "0",
          ];
      }
    }


}
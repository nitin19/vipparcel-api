<?php
namespace App\Http\Services;

require 'vendor/autoload.php';
use App\Http\Controllers\Controller; 
use Illuminate\Support\Facades\Redirect;
use App\Models\Store; 
use App\Models\StoreOrder;
use App\Models\StoreOrderItem;
use Illuminate\Http\Request; 
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use \DTS\eBaySDK\Trading\Services;
use \DTS\eBaySDK\Trading\Types;
use \DTS\eBaySDK\Trading\Enums;
use Validator;
use URL;
use \DTS\eBaySDK\OAuth\Services\OAuthService;
use \DTS\eBaySDK\OAuth\Types\GetUserTokenRestRequest;
use \DTS\eBaySDK\OAuth\Types\GetUserTokenRestResponse;
use \DTS\eBaySDK\OAuth\Types\RefreshUserTokenRestRequest;
use \DTS\eBaySDK\Types\BaseType;
use Session;

trait Ebay
{

  private $config;
  private $request;


    /**
     * Save Store Credentials for Ebay
     *
     *@params array [ varchar $storename , varchar $code ], int $userId
     *@return json [Boolean msg, Boolean status]
     */
     public function eBaySaveStoreCredentials($data,$userId){
         
         $validator = Validator::make($data, [ 
           'storeName' => 'required',
         ]);
         
         if ($validator->fails()) { 
          return response()->json(['error'=>$validator->errors()], $this->errorStatus);            
        }
         else{ 
           $user = Auth::user();
           $clientID = config('ebay.clientID');
           $clientSecret = config('ebay.clientSecret');
           $devId = config('ebay.devID');
           $ruName = config('ebay.ruName');
           $url = config('ebay.authToeknUrl');
           $sandbox = config('ebay.sandbox');

           $credentials = array(
              'devId' => $devId,
              'appId' => $clientID,
              'certId'=> $clientSecret,
          );

          $service = new OAuthService([
              'credentials' => $credentials,
              'ruName'      => $ruName,
              'sandbox'     => $sandbox
          ]);

          $ebayTokenurl =  $service->redirectUrlForUser([
          'state' => $data['storeName'].'-'.$user->id,
          'scope' => [
              'https://api.ebay.com/oauth/api_scope/sell.fulfillment',
              'https://api.ebay.com/oauth/api_scope/sell.fulfillment.readonly' //You can ad more sope here
          ]
      ]); 

        $response = [
        'requestId' => strtolower(Str::random(30)),
        'url' => $ebayTokenurl,
        'statusCode'    => 200,
        'status' => 'success',
        ];
        return response()->json($response);

        //return Redirect::away($ebayTokenurl);
            
        }

     }

    /**
     * Provide Ebay Authroize url to redirect user on Ebay
     *
     *@params varchar $storename
     *@return json [varchar url, Boolean status]
     */
     public function ebayGetAuthrozieRedirectUrl(Request $request){ 
         $data = $request->all();   
         $storeName = $data['eBaystore'];
         $clientID = config('ebay.clientID');
         $clientSecret = config('ebay.clientSecret');
         $ruName = config('ebay.ruName');
         $authUrl = config('ebay.authUrl');
         $url = "{$authUrl}authorize?client_id={$clientID}&redirect_uri={$ruName}&response_type=code&state={$storeName}&scope=https://api.ebay.com/oauth/api_scope/sell.account%20https://api.ebay.com/oauth/api_scope/sell.inventory&prompt=login";     
         return response()->json(['url' => $url], $this->successStatus);  
     }
  
    
    /**
     * Ebay seller has Authroize our Ebay developer account
     *
     *@params varchar $authCode
     *@return Boolean true/false
     */
     public function ebayAuthAccepted($authCode){
       $clientID = config('ebay.clientID');
       $clientSecret = config('ebay.clientSecret');
       $devId = config('ebay.devID');
       $ruName = config('ebay.ruName');
       $url = config('ebay.authTokenUrl');
       $sandbox = config('ebay.sandbox');

        $credentials = array(
            'devId' => $devId,
            'appId' => $clientID,
            'certId'=> $clientSecret,
        );

        $service = new OAuthService([
            'credentials' => $credentials,
            'ruName'      => $ruName,
            'sandbox'     => true
        ]);

       $headers = [
            'Content-Type: application/x-www-form-urlencoded',
            'Authorization: Basic '.base64_encode($clientID.':'.$clientSecret)
         ];
         $body = http_build_query([
            'grant_type'   => 'authorization_code',
            'code'         => $authCode,
            'redirect_uri' => $ruName
         ]);
         $curl = curl_init();
         curl_setopt_array($curl, array(
            CURLOPT_URL            => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_CUSTOMREQUEST  => 'POST',
            CURLOPT_POSTFIELDS     => $body,
            CURLOPT_HTTPHEADER     => $headers
         ));
         $response = curl_exec($curl);
         $err = curl_error($curl);
         curl_close($curl);

         if ($err) {
            return false;
         } else {
            return $response;
         }
     }


  /**
     * Ebay token is short lived . So we refresh it everytime we use its services
     *
     *@params varchar $refreshToken
     *@return Boolean false, varchar $refreshToken;
     */
     public function ebayAuthRefreshToken($refreshToken){
         
         $scope =  config('ebay.scope');
         $clientID = config('ebay.clientID');
         $clientSecret = config('ebay.clientSecret');
         $devId = config('ebay.devID');
         $ruName = config('ebay.ruName');
         $url = config('ebay.authTokenUrl');

         $headers = [
            'Content-Type: application/x-www-form-urlencoded',
            'Authorization: Basic '.base64_encode($clientID.':'.$clientSecret)
         ];

         $scope = 'https://api.ebay.com/oauth/api_scope/sell.inventory https://api.ebay.com/oauth/api_scope/sell.account';
         
         $body = http_build_query([
            'grant_type'    => 'refresh_token',
            'refresh_token' => $refreshToken,
            'scope'  => urlencode($scope)
         ]);

         $curl = curl_init();
         
         curl_setopt_array($curl, array(
            CURLOPT_URL            => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_CUSTOMREQUEST  => 'POST',
            CURLOPT_POSTFIELDS     => $body,
            CURLOPT_HTTPHEADER     => $headers
         ));
         
         $response = curl_exec($curl);         
         $err = curl_error($curl);         
         curl_close($curl);  

         if($err) {
            return false;
         }else {
            $responseMessage = json_decode($response);
            if(array_key_exists('access_token', $responseMessage)){
                return $responseMessage->access_token;
            } else {
              return false;
            }
         }    

     }

   /**
     * Pull Ebay Orders from its stores
     *
     *@params int $storeId
     *@return array [$numberoforder, $numberoforderItem, $status];
     */
   public function ebayGetEbayOrders($storeId) { 
         
         $clientID = config('ebay.clientID');
         $clientSecret = config('ebay.clientSecret');
         $ruName = config('ebay.ruName');
         $devID = config('ebay.devID');
         $sandbox = config('ebay.sandbox');
         $stores = Store::find($storeId);
         $auth = json_decode($stores->auth);

         //$authorization = $this->ebayAuthRefreshToken($auth->refresh_token);

         $currenttime = date('Y-m-d h:i:s');
         $TokenExpireTime = date('Y-m-d h:i:s',strtotime('+2 hour',strtotime($stores->created_at)));
         if($currenttime > $TokenExpireTime){
            $authorization = $this->ebayAuthRefreshToken($auth->refresh_token);
         } else {
            $authorization = $auth->access_token;
         }

         $startDate = date('Y-m-d',strtotime("-15 days"));
         $endDate = date('Y-m-d');


         if(!$authorization){
            $message = "Error";
            return $response = [
            'requestId' => strtolower(Str::random(30)),
            'statusCode' => 403,
            'message' => $message,
            'status' => "0",
            ];
          }
         
         $service = new Services\TradingService(array(              
              'authorization' => $authorization,
              'apiVersion' => $this->config['tradingApiVersion'],
              'siteId' => 0,
              'sandbox'=> $sandbox,
              'credentials' => [
              'appId'  => $clientID,
              'certId' => $clientSecret,
              'devId'  => $devID
            ]
         ));

         $request = new Types\GetMyeBaySellingRequestType();        
         $args = array(
           "OrderStatus"   => "All",
           "SortingOrder"  => "Ascending",
           //"OrderRole"     => "Seller",
           "CreateTimeFrom"   => new \DateTime($startDate),
           "CreateTimeTo"   => new \DateTime($endDate),
         );
         
         $getOrders = new Types\GetOrdersRequestType($args); 
         $getOrders->RequesterCredentials = new Types\CustomSecurityHeaderType(); 
         $getOrders->IncludeFinalValueFee = true;
         $getOrders->Pagination = new Types\PaginationType();
         $getOrders->Pagination->EntriesPerPage = 10;
         
         //$getOrders->OrderIDArray = new Types\OrderIDArrayType();
         
         $getOrdersPageNum = 1;
         $response = $service->getOrders($getOrders);        
         if($response->Ack =='Failure'){
           $message = "Some error occured";
           return $response = [
            'requestId' => strtolower(Str::random(30)),
            'statusCode' => 403,
            'message' => $message,
            'status' => "0",
            ];
         }
         $numberofOrders = 0;
         $numberofOrderItems = 0;
         if(count($response->OrderArray->Order) > 0){ 
         foreach ($response->OrderArray->Order as $order) {
           $storeId = $storeId; 
           $orderDate = $order->CreatedTime; 
           $orderId = $order->OrderID;             
           $haverecords = StoreOrder::wherestoreId($storeId)->whereorderId($orderId)->count();
            if(!$haverecords){                
              $storeOrders = new StoreOrder;
              $orderDate = $order->CreatedTime->format('Y-m-d H:i:s');
              $storeOrders->order_date = trim(str_replace(['T', 'Z']," ",$orderDate));
              $storeOrders->order_id = $orderId;
              $storeOrders->store_id = $storeId;
              $val['PurchaseDate'] = $order->CreatedTime; 
              $val['OrderStatus'] = $order->OrderStatus; 
              $val['OrderTotal']["Amount"] = $order->Total->value; 
              $val['OrderTotal']["CurrencyCode"] = $order->Total->currencyID; 
              $val['ShippingService'] = $order->ShippingServiceSelected->ShippingService; 
              $val['ShippingAddress']["Name"] = $order->ShippingAddress->Name;
              $val['ShippingAddress']["Street1"] = $order->ShippingAddress->Street1;
              $val['ShippingAddress']["Street2"] = $order->ShippingAddress->Street2;
              $val['ShippingAddress']["CityName"] = $order->ShippingAddress->CityName;
              $val['ShippingAddress']["StateOrProvince"] = $order->ShippingAddress->StateOrProvince;
              $val['ShippingAddress']["Country"] = $order->ShippingAddress->Country;
              $val['ShippingAddress']["CountryName"] = $order->ShippingAddress->CountryName;
              $val['ShippingAddress']["Phone"] = $order->ShippingAddress->Phone;
              $val['ShippingAddress']["PostalCode"] = $order->ShippingAddress->PostalCode;
              $val['ShippingAddress']["AddressID"] = $order->ShippingAddress->AddressID;
              $val['ShippingAddress']["AddressOwner"] = $order->ShippingAddress->AddressOwner;
              $val['ShippingAddress']["ExternalAddressID"] = $order->ShippingAddress->ExternalAddressID;
              $val['BuyerEmail'] = $order->TransactionArray->Transaction[0]->Buyer->Email;
              $val['BuyerName'] = "Anatoliy Yanovskiy";
              $storeOrders->order_details = json_encode($val);                
              $storeOrders->save();
              $storeOrderId = $storeOrders->id;

              /* Storing Items in tables */
              foreach($order->TransactionArray->Transaction as $Items){
                $Item = $Items->Item;
                $sellersku = $Item->SKU;
                $OrderItemId = $Item->ItemID;
                $haverecords = StoreOrderItem::wherestoreOrderId($storeOrderId)->whereorderItemId($OrderItemId)->count();
                $countOrderItem = 0;
                if(!$haverecords)
                  {
                    $item_details['SellerSKU'] = $Item->SKU;
                    $item_details['OrderItemId'] = $Item->ItemID;
                    $item_details['Title'] = $Item->Title;
                    $item_details['QuantityOrdered'] = $Items->QuantityPurchased;
                    $item_details['ItemPrice']['Amount'] = $Items->TransactionPrice->value;
                    $item_details['ItemPrice']['CurrencyCode'] = $Items->TransactionPrice->currencyID;
                    $orderItemId = $Item->ItemID;
                    $storeItems = new StoreOrderItem;
                    $storeItems->store_order_id = $storeOrderId;
                    $storeItems->asin = 'NA';
                    $storeItems->item_name = $Item->Title;
                    $storeItems->sellersku = $Item->SKU;
                    $storeItems->order_item_id = $Item->ItemID;
                    $storeItems->item_details = json_encode($item_details);
                    $storeItems->item_status = 'new';  
                    $storeItems->save();    
                    $countOrderItem ++;        
                }
              }

              $numberofOrderItems += $countOrderItem;
              $numberofOrders++;
              }
                return $response = [
                'requestId' => strtolower(Str::random(30)),
                'statusCode' => 200,
                'message' => "Sync Successfully",
                'status' => "1",
                "orders" =>$numberofOrders,
                "orderItems" => $numberofOrderItems 
                ];
            }
          } else {
            return $response = [
            'requestId' => strtolower(Str::random(30)),
            'statusCode' => 200,
            'message' => "Sync Successfully",
            'status' => "1",
            "orders" =>0,
            "orderItems" => 0 
            ];
          }

     }
 
     /**
     * After generating Labels we are marking prodcut as shipped
     *
     *@params int $storeId, varchar $carrierUsed, int $trackingNumber, int $orderId
     *@return Boolean 
     */
     public function ebayMarkedProductAsShipped($storeId,$carrierUsed,$trackingNumber,$orderId){
        $clientID = config('ebay.clientID');
        $clientSecret = config('ebay.clientSecret');
        $ruName = config('ebay.ruName');
        $devID = config('ebay.devID');
        $sandbox = config('ebay.sandbox');
        $stores = Store::find($storeId);
        $auth = json_decode($stores->auth);
        $authorization = $this->ebayAuthRefreshToken($auth->refresh_token);       
        
        if(!$authorization){
            $msg = "Something Wrong Happend.";
            return response()->json(['msg'=>$msg], $this->errorStatus);
        }  
        
        $service = new Services\TradingService(array(              
              'authorization' => $authorization,
              'apiVersion' => $this->config['tradingApiVersion'],
              'siteId' => 0,
              'sandbox'=> $sandbox,
              'credentials' => [
              'appId'  => $clientID,
              'certId' => $clientSecret,
              'devId'  => $devID
            ]
        ));

        $ship = new Types\ShipmentTrackingDetailsType();
        $ship->ShippingCarrierUsed = $carrierUsed;
        $ship->ShipmentTrackingNumber = $trackingNumber;

        $request = new Types\CompleteSaleRequestType();
        $request->OrderID = $orderId;
        $request->Shipment = new Types\ShipmentType();
        $request->Shipment->ShipmentTrackingDetails = [ $ship ];
        $request->MessageID = 'Order updated on: ' . date('Y-m-d H:i:s');
        $request->Shipped = TRUE;
        $response = $service->CompleteSale($request);
        if($response->Ack =='Success'){           
           return true;
        }else{
           return false;
        }

     }


     public function formatEbayName($store){

          $store = substr($store,7);

          return $store = substr($store,0,-15);


     }


    public function ebaySaveUserToken($data){
      $UserTokenResponse =  $this->ebayAuthAccepted($data['code']);

      $TokenResponse = json_decode($UserTokenResponse);
      $user = Auth::user();
      if($data['state'] && $data['state']!=''){
          $statedata = explode('-', $data['state']);
          $storename = $statedata[0];
          $userId = $statedata[1];
      } else {
          $storename = '';
          $userId = '0';
      }
      
      if(array_key_exists('access_token', $TokenResponse)){
          $storeName = $this->formatEbayName($storename);
          $stores = new Store;        
          $stores->owner_id = $userId;
          $stores->store_system_id = 2;
          $stores->slug = rand(34,98).time().rand(340,99999);
          $stores->auth = $UserTokenResponse;
          $stores->store_name = $storename;
          $stores->status = 1;
          $stores->save();
          return $response = [
          'requestId' => strtolower(Str::random(30)),
          'statusCode' => 201,
          'message' => "success",
          'status' => "1",
          ];
      } else {
          return $response = [
          'requestId' => strtolower(Str::random(30)),
          'statusCode' => 400,
          'message' => "error",
          'status' => "0",
          ];
      }
    }


}
<?php

namespace App\Http\Controllers\Api\Account;

use App\User;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Api\APIBaseController as APIBaseController;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Str;
use App\Profile;
use Auth;
use DB;
use Exception;
use Log;
use Session;
use Hash;
use Image;

class PersonalInfoController extends APIBaseController
{

    public $successStatus = 200;

    public function __construct()
    {
       //
    }

    public function get_details(Request $request)
    {   
        try {
            $token = trim($request->authToken);
            $uri = $request->path();
            if($token) {
                if($uri == 'api/v1/account/personalInfo/details') {
                    $auth_token = DB::table('user_api')->where('auth_token', $token)->first();
                    if (is_null($auth_token)) { 
                        $response = [
                        'requestId' => strtolower(Str::random(30)),
                        'message'    => 'Token does not exist.',
                        'statusCode' => 401,
                        ];
                    } else {
                        if($auth_token->active == 1) {
                        $user = DB::table('users')->where('id', $auth_token->user_id)->where('in_archive', 0)->first();
                        if ($user) {
                            $profileInfo = DB::table('profile')->where('user_id', $auth_token->user_id)->first();
                            if ($profileInfo) {
                                $response = [
                                'requestId' => strtolower(Str::random(30)),
                                'id' => (int)$profileInfo->id,
                                'lastName' => $profileInfo->last_name,
                                'firstName' => $profileInfo->first_name,
                                'phone' => $profileInfo->phone,
                                'streetAddress1' => $profileInfo->address,
                                'streetAddress2' => $profileInfo->address_2,
                                'city' => $profileInfo->city,
                                'state' => $profileInfo->state,
                                'postalCode' => $profileInfo->zip,
                                'user_id' => $profileInfo->user_id,
                                'statusCode' => $this->successStatus,
                               ];
                              } else {
                                $response = [
                                'requestId' => strtolower(Str::random(30)),
                                'message'    => 'Record does not exist.',
                                'statusCode' => 401,
                               ];
                            }

                            $api_statistics = DB::table('user_api_statistics')->where('user_id', $auth_token->user_id)->where('resource_path', $request->path())->first();

                            if (is_null($api_statistics)) { 
                               $insId = DB::table('user_api_statistics')->insertGetId(
                                 ['user_id' => $auth_token->user_id, 'resource_path' => $request->path(), 'count_requests' => 1, 'created' => date('Y-m-d H:i:s')]
                                );
                            } else {
                                DB::table('user_api_statistics')->where('id', $api_statistics->id)->update(['count_requests' => $api_statistics->count_requests + 1, 'updated' => date('Y-m-d H:i:s')]);
                            }
                         } else {
                                $response = [
                                'requestId' => strtolower(Str::random(30)),
                                'statusCode' => 403,
                                'message'    => "You don't have permission to access.",
                               ];
                            }    
                        } else {
                            $response = [
                            'requestId' => strtolower(Str::random(30)),
                            'message' => 'Token is locked.',
                            'statusCode'    => 403,
                            ];
                        }
                    }  
                } else {
                        $response = [
                        'requestId' => strtolower(Str::random(30)),
                        'statusCode'    => 405,
                        'error' => 'The requested URL '. $uri .'was not found on this server.',
                        ];
                }
            } else {
                $response = [
                    'requestId' => strtolower(Str::random(30)),
                    'statusCode' => 400,
                    'error'    => 'Required parameter not passed: authToken',
                ];
            }
            return response()->json($response);
        } catch (\Exception $e) {
            return $e->getMessage();
        }    
    }

    public function put_update(Request $request)
    {   
        try {
            $token = trim($request->authToken);
            $uri = $request->path();
            if($token) {
                if($uri == 'api/v1/account/personalInfo/update') {
                    $auth_token = DB::table('user_api')->where('auth_token', $token)->first();
                    if (is_null($auth_token)) { 
                        $response = [
                        'requestId' => strtolower(Str::random(30)),
                        'message'    => 'Token does not exist.',
                        'statusCode' => 401,
                        ];
                    } else {
                        if($auth_token->active == 1) {
                        $user = DB::table('users')->where('id', $auth_token->user_id)->where('in_archive', 0)->first();
                        if ($user) {    
                            $profileInfo = DB::table('profile')->where('user_id', $auth_token->user_id)->first();
                            if ($profileInfo) {
                                if ($profileInfo->is_international==0) {
                                $rules = array (
                                                'firstName' => 'required|max:35|regex:/^[a-zA-Z0-9_\s]+$/',
                                                'lastName' => 'required|max:35|regex:/^[a-zA-Z0-9_\s]+$/',
                                                'phone' => 'required',
                                                'streetAddress1' => 'required',
                                                'city' => 'required',
                                                'state' => 'required|max:2|min:2',
                                                'postalCode' => 'required',
                                            );
                                $validator = Validator::make($request->all(), $rules);
                                if($validator->fails()) {
                                    $response = [
                                    'requestId' => strtolower(Str::random(30)),
                                    'message'    => 'Validation Error.',
                                    'error'    => $validator->errors(),
                                    'statusCode' => 422,
                                   ];
                                 } else {
                                $updateProfileInfo = DB::table('profile')
                                         ->where('user_id', $auth_token->user_id)
                                         ->update(['last_name' => $request->lastName, 'first_name' => $request->firstName, 'phone' => $request->phone, 'address' => $request->streetAddress1, 'address_2' => $request->streetAddress2, 'city' => $request->city, 'state' => $request->state, 'zip' => $request->postalCode, 'driver_licence' => $request->driverLicence]);
                                if ($updateProfileInfo) {
                                    $response = [
                                    'requestId' => strtolower(Str::random(30)),
                                    'id' => (int)$profileInfo->id,
                                    'user_id' => $auth_token->user_id,
                                    'statusCode' => $this->successStatus,
                                   ];
                                  } else {
                                    $response = [
                                    'requestId' => strtolower(Str::random(30)),
                                    'message'    => 'No content changed.',
                                    'statusCode' => 204,
                                   ];
                                }
                              } 

                             } else {
                                $response = [
                                    'requestId' => strtolower(Str::random(30)),
                                    'statusCode' => 403,
                                    'error'    => 'Forbidden.',
                                   ];
                             } 
                            } else {
                                $response = [
                                'requestId' => strtolower(Str::random(30)),
                                'message'    => 'Record does not exist.',
                                'statusCode' => 401,
                               ];
                            }
                            $api_statistics = DB::table('user_api_statistics')->where('user_id', $auth_token->user_id)->where('resource_path', $request->path())->first();

                            if (is_null($api_statistics)) { 
                               $insId = DB::table('user_api_statistics')->insertGetId(
                                 ['user_id' => $auth_token->user_id, 'resource_path' => $request->path(), 'count_requests' => 1, 'created' => date('Y-m-d H:i:s')]
                                );
                            } else {
                                DB::table('user_api_statistics')->where('id', $api_statistics->id)->update(['count_requests' => $api_statistics->count_requests + 1, 'updated' => date('Y-m-d H:i:s')]);
                            }
                        } else {
                                $response = [
                                'requestId' => strtolower(Str::random(30)),
                                'statusCode' => 403,
                                'message'    => "You don't have permission to access.",
                               ];
                            }    
                        } else {
                            $response = [
                            'requestId' => strtolower(Str::random(30)),
                            'message' => 'Token is locked.',
                            'statusCode'    => 403,
                            ];
                        }
                    }  
                } else {
                        $response = [
                        'requestId' => strtolower(Str::random(30)),
                        'statusCode'    => 405,
                        'error' => 'The requested URL '. $uri .'was not found on this server.',
                        ];
                }
            } else {
                $response = [
                    'requestId' => strtolower(Str::random(30)),
                    'statusCode' => 400,
                    'error'    => 'Required parameter not passed: authToken',
                ];
            }
            return response()->json($response);
        } catch (\Exception $e) {
            return $e->getMessage();
        }    
    }

}
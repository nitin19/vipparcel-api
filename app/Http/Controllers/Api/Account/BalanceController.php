<?php

namespace App\Http\Controllers\Api\Account;

use App\User;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Api\APIBaseController as APIBaseController;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Str;
use App\Balance;
use Auth;
use DB;
use Exception;
use Log;
use Session;
use Hash;
use Image;

class BalanceController extends APIBaseController
{

    public $successStatus = 200;

    public function __construct()
    {
       //
    }

    public function get_getHistory(Request $request)
    {   
        try {
            $token = trim($request->authToken);
            $uri = $request->path();
            $fullUrl = $request->fullUrl(); 
            $records = array();
            if($token) {
                if($uri == 'api/v1/account/balance/getHistory') {
                    $auth_token = DB::table('user_api')->where('auth_token', $token)->first();
                    if (is_null($auth_token)) { 
                        $response = [
                        'requestId' => strtolower(Str::random(30)),
                        'message'    => 'Token does not exist.',
                        'statusCode' => 401,
                        ];
                    } else {
                        if($auth_token->active == 1) {
                        $user = DB::table('users')->where('id', $auth_token->user_id)->where('in_archive', 0)->first();
                            if ($user) {

                                $limit = trim($request->limit);
                                if($limit) {
                                    if($limit <= 150) {
                                        $limit = trim($request->limit);
                                    } else {
                                        $response = [
                                            'requestId' => strtolower(Str::random(30)),
                                            'statusCode' => 400,
                                            'error'    => "Invalid parameter: limit",
                                           ];
                                        return response()->json($response);   
                                    }
                                } else {
                                    $limit = 30;
                                }

                                $offset = trim($request->offset);
                                if($offset) {
                                   $offset = trim($request->offset);
                                } else {
                                   $offset = 0; 
                                }

                        $orderByArr = array('id', 'discountPercent', 'balance', 'value', 'description', 'created');
                                if (is_array($request->orderBy)) {
                                   foreach($request->orderBy as $Key=>$Value) {
                                     break;
                                   }

                                  if($Key) {
                                    if(in_array(trim($Key), $orderByArr)) {
                                    if(($Value!='') && ( $Value == 'asc' || $Value == 'ASC' || $Value == 'desc' || $Value == 'DESC' )){
                                           $sortKey = $Key;
                                           $sortValue = $Value;
                                       } else {
                                        $response = [
                                            'requestId' => strtolower(Str::random(30)),
                                            'statusCode' => 400,
                                            'error'    => "Invalid parameter: orderBy",
                                           ];
                                        return response()->json($response);
                                       }
                                    } else {
                                    $response = [
                                            'requestId' => strtolower(Str::random(30)),
                                            'statusCode' => 400,
                                            'error'    => "Field \"$Key\" is not sortable",
                                           ];
                                        return response()->json($response);
                                   }
                                 } else {
                                   $response = [
                                            'requestId' => strtolower(Str::random(30)),
                                            'statusCode' => 400,
                                            'error'    => "Field \"0\" is not sortable",
                                           ];
                                        return response()->json($response);
                                  } 

                                } else {

                                $orderBy = trim($request->orderBy);
                                if($orderBy) {
                                   if(in_array(trim($request->orderBy), $orderByArr)) {
                                     switch ($orderBy) {
                                        case 'id':
                                            $sortKey = 'id';
                                            $sortValue = 'asc';
                                            break;
                                        case 'discountPercent':
                                            $sortKey = 'discount';
                                            $sortValue = 'asc';
                                            break;
                                        case 'balance':
                                            $sortKey = 'balance';
                                            $sortValue = 'asc';
                                            break;
                                        case 'value':
                                            $sortKey = 'amount';
                                            $sortValue = 'asc';
                                            break;
                                        case 'description':
                                            $sortKey = 'description';
                                            $sortValue = 'asc';
                                            break;            
                                        case 'created':
                                            $sortKey = 'created';
                                            $sortValue = 'asc';
                                            break;
                                        } 
                                   } else {
                                    $response = [
                                            'requestId' => strtolower(Str::random(30)),
                                            'statusCode' => 400,
                                            'error'    => "Field \"$orderBy\" is not sortable",
                                           ];
                                   return response()->json($response);
                                   }
                                } else {
                                 if (strpos($fullUrl,'orderBy') !== false) {
                                      $response = [
                                            'requestId' => strtolower(Str::random(30)),
                                            'statusCode' => 400,
                                            'error'    => "Invalid parameter: orderBy",
                                           ];
                                        return response()->json($response);
                                     } else {
                                      $sortKey = 'id';
                                      $sortValue = 'asc';
                                    }
                                  }     
                            }
                                
                          //  $result_payment = DB::table('payments')->select('id', 'amount', 'description', 'created', 'discount', 'balance')->where('from_user_id', '=', $auth_token->user_id)->orWhere('to_user_id', '=', $auth_token->user_id)->orderBy('id', 'asc')->get();

                            $result_payment = DB::table('payments')->where('from_user_id', '=', $auth_token->user_id)->orWhere('to_user_id', '=', $auth_token->user_id)->orderBy($sortKey, $sortValue)->offset($offset)->limit($limit)->get(); 

                            $record_payments = $result_payment->toArray();

                                if(count($record_payments) > 0) {
                                    foreach($record_payments as $payment) {
                                    $records[] = array(
                                        'id' => (int)$payment->id,
                                        'created' => $payment->created,
                                        'description' => $payment->description,
                                        'discountPercent' => round($payment->discount, 1),
                                        'value' => round($payment->amount, 2),
                                        'balance' => round($payment->balance, 2),
                                    );
                                   }
                                }
                                if (count($records) >0) {
                                    $response = [
                                    'requestId' => strtolower(Str::random(30)),
                                    'totalRecords'    => count($record_payments),
                                    'records'    => $records,
                                    'statusCode' => $this->successStatus,
                                   ];
                                  } else {
                                    $response = [
                                    'requestId' => strtolower(Str::random(30)),
                                    'message'    => 'Record does not exist.',
                                    'statusCode' => 401,
                                   ];
                                }    

                               $api_statistics = DB::table('user_api_statistics')->where('user_id', $auth_token->user_id)->where('resource_path', $request->path())->first();

                                if (is_null($api_statistics)) { 
                                   $insId = DB::table('user_api_statistics')->insertGetId(
                                     ['user_id' => $auth_token->user_id, 'resource_path' => $request->path(), 'count_requests' => 1, 'created' => date('Y-m-d H:i:s')]
                                    );
                                } else {
                                    DB::table('user_api_statistics')->where('id', $api_statistics->id)->update(['count_requests' => $api_statistics->count_requests + 1, 'updated' => date('Y-m-d H:i:s')]);
                                }
                              } else {
                                $response = [
                                'requestId' => strtolower(Str::random(30)),
                                'statusCode' => 403,
                                'message'    => "You don't have permission to access.",
                               ];
                            }
                        } else {
                            $response = [
                            'requestId' => strtolower(Str::random(30)),
                            'message' => 'Token is locked.',
                            'statusCode'    => 403,
                            ];
                        }
                    }  
                } else {
                        $response = [
                        'requestId' => strtolower(Str::random(30)),
                        'statusCode'    => 405,
                        'error' => 'The requested URL '. $uri .'was not found on this server.',
                        ];
                }
            } else {
                $response = [
                    'requestId' => strtolower(Str::random(30)),
                    'statusCode' => 400,
                    'error'    => 'Required parameter not passed: authToken',
                ];
            }
            return response()->json($response);
        } catch (\Exception $e) {
            return $e->getMessage();
        }    
    }

    public function get_getCurrent(Request $request)
    {   
        try {
            $token = trim($request->authToken);
            $uri = $request->path();
            if($token) {
                if($uri == 'api/v1/account/balance/getCurrent') {
                    $auth_token = DB::table('user_api')->where('auth_token', $token)->first();
                    if (is_null($auth_token)) { 
                        $response = [
                        'requestId' => strtolower(Str::random(30)),
                        'message'    => 'Token does not exist.',
                        'statusCode' => 401,
                        ];
                    } else {
                        if($auth_token->active == 1) {
                        $user = DB::table('users')->where('id', $auth_token->user_id)->where('in_archive', 0)->first();
                            if ($user) {
                                $response = [
                                'requestId' => strtolower(Str::random(30)),
                                'value' => round($user->balance, 2),
                                'statusCode' => $this->successStatus,
                               ];
                               $api_statistics = DB::table('user_api_statistics')->where('user_id', $auth_token->user_id)->where('resource_path', $request->path())->first();

                                if (is_null($api_statistics)) { 
                                   $insId = DB::table('user_api_statistics')->insertGetId(
                                     ['user_id' => $auth_token->user_id, 'resource_path' => $request->path(), 'count_requests' => 1, 'created' => date('Y-m-d H:i:s')]
                                    );
                                } else {
                                    DB::table('user_api_statistics')->where('id', $api_statistics->id)->update(['count_requests' => $api_statistics->count_requests + 1, 'updated' => date('Y-m-d H:i:s')]);
                                }
                              } else {
                                $response = [
                                'requestId' => strtolower(Str::random(30)),
                                'statusCode' => 403,
                                'message'    => "You don't have permission to access.",
                               ];
                            }
                        } else {
                            $response = [
                            'requestId' => strtolower(Str::random(30)),
                            'message' => 'Token is locked.',
                            'statusCode'    => 403,
                            ];
                        }
                    }  
                } else {
                        $response = [
                        'requestId' => strtolower(Str::random(30)),
                        'statusCode'    => 405,
                        'error' => 'The requested URL '. $uri .'was not found on this server.',
                        ];
                }
            } else {
                $response = [
                    'requestId' => strtolower(Str::random(30)),
                    'statusCode' => 400,
                    'error'    => 'Required parameter not passed: authToken',
                ];
            }
            return response()->json($response);
        } catch (\Exception $e) {
            return $e->getMessage();
        }    
    }
}
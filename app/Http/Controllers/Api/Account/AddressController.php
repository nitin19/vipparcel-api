<?php

namespace App\Http\Controllers\Api\Account;

use App\User;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Api\APIBaseController as APIBaseController;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Str;
use App\Balance;
use Auth;
use DB;
use Exception;
use Log;
use Session;
use Hash;
use Image;

class AddressController extends APIBaseController
{

    public $successStatus = 200;

    public function __construct()
    {
       //
    }

    public function get_getList(Request $request)
    {   
        try {
            $token = trim($request->authToken);
            $uri = $request->path();
            $fullUrl = $request->fullUrl(); 
            $records = array();
            if($token) {
                if($uri == 'api/v1/account/address/getList') {
                    $auth_token = DB::table('user_api')->where('auth_token', $token)->first();
                    if (is_null($auth_token)) { 
                        $response = [
                        'requestId' => strtolower(Str::random(30)),
                        'message'    => 'Token does not exist.',
                        'statusCode' => 401,
                        ];
                        return response()->json($response);
                    } else {
                        if($auth_token->active == 1) {
                        $user = DB::table('users')->where('id', $auth_token->user_id)->where('in_archive', 0)->first();
                            if ($user) {
                                $limit = trim($request->limit);
                                if($limit) {
                                    if($limit <= 150) {
                                        $limit = trim($request->limit);
                                    } else {
                                        $response = [
                                            'requestId' => strtolower(Str::random(30)),
                                            'statusCode' => 400,
                                            'error'    => "Invalid parameter: limit",
                                           ];
                                        return response()->json($response);   
                                    }
                                } else {
                                    $limit = 30;
                                }

                                $offset = trim($request->offset);
                                if($offset) {
                                   $offset = trim($request->offset);
                                } else {
                                   $offset = 0; 
                                }

                        $orderByArr = array('id', 'firstName', 'lastName', 'city', 'clientId', 'created');
                                if (is_array($request->orderBy)) {
                                   foreach($request->orderBy as $Key=>$Value) {
                                     break;
                                   }

                                  if($Key) {
                                    if(in_array(trim($Key), $orderByArr)) {
                                    if(($Value!='') && ( $Value == 'asc' || $Value == 'ASC' || $Value == 'desc' || $Value == 'DESC' )){

                                        switch ($Key) {
                                        case 'id':
                                            $sortKey = 'id';
                                            $sortValue = $Value;
                                            break;
                                        case 'firstName':
                                            $sortKey = 'first_name';
                                            $sortValue = $Value;
                                            break;
                                        case 'lastName':
                                            $sortKey = 'last_name';
                                            $sortValue = $Value;
                                            break;
                                        case 'city':
                                            $sortKey = 'city';
                                            $sortValue = $Value;
                                            break;
                                        case 'clientId':
                                            $sortKey = 'client_id';
                                            $sortValue = $Value;
                                            break;            
                                        case 'created':
                                            $sortKey = 'created';
                                            $sortValue = $Value;
                                            break;
                                        }

                                         //  $sortKey = $Key;
                                         //  $sortValue = $Value;
                                       } else {
                                        $response = [
                                            'requestId' => strtolower(Str::random(30)),
                                            'statusCode' => 400,
                                            'error'    => "Invalid parameter: orderBy",
                                           ];
                                        return response()->json($response);
                                       }
                                    } else {
                                    $response = [
                                            'requestId' => strtolower(Str::random(30)),
                                            'statusCode' => 400,
                                            'error'    => "Field \"$Key\" is not sortable",
                                           ];
                                        return response()->json($response);
                                   }
                                 } else {
                                   $response = [
                                            'requestId' => strtolower(Str::random(30)),
                                            'statusCode' => 400,
                                            'error'    => "Field \"0\" is not sortable",
                                           ];
                                        return response()->json($response);
                                  } 

                                } else {

                                $orderBy = trim($request->orderBy);
                                if($orderBy) {
                                   if(in_array(trim($request->orderBy), $orderByArr)) {
                                     switch ($orderBy) {
                                        case 'id':
                                            $sortKey = 'id';
                                            $sortValue = 'asc';
                                            break;
                                        case 'firstName':
                                            $sortKey = 'first_name';
                                            $sortValue = 'asc';
                                            break;
                                        case 'lastName':
                                            $sortKey = 'last_name';
                                            $sortValue = 'asc';
                                            break;
                                        case 'city':
                                            $sortKey = 'city';
                                            $sortValue = 'asc';
                                            break;
                                        case 'clientId':
                                            $sortKey = 'client_id';
                                            $sortValue = 'asc';
                                            break;            
                                        case 'created':
                                            $sortKey = 'created';
                                            $sortValue = 'asc';
                                            break;
                                        } 
                                   } else {
                                    $response = [
                                            'requestId' => strtolower(Str::random(30)),
                                            'statusCode' => 400,
                                            'error'    => "Field \"$orderBy\" is not sortable",
                                           ];
                                   return response()->json($response);
                                   }
                                } else {
                                 if (strpos($fullUrl,'orderBy') !== false) {
                                      $response = [
                                            'requestId' => strtolower(Str::random(30)),
                                            'statusCode' => 400,
                                            'error'    => "Invalid parameter: orderBy",
                                           ];
                                        return response()->json($response);
                                     } else {
                                      $sortKey = 'id';
                                      $sortValue = 'asc';
                                    }
                                  }     
                            }  

                            //$result_address = DB::table('user_address')->where('user_id', '=', $auth_token->user_id)->orderBy($orderBy,'ASC')->skip($offset*$limit)->take($limit)->get(); 

                            $result_address = DB::table('user_address')->where('user_id', '=', $auth_token->user_id)->orderBy($sortKey, $sortValue)->offset($offset)->limit($limit)->get(); 

                            $record_addresses = $result_address->toArray();

                                if(count($record_addresses) > 0) {
                                    foreach($record_addresses as $address_item) {
                                        $zip4 = NULL;
                                        $postalCode =  $address_item->postalcode;
                                         $postalCodeExp = explode('-', $postalCode);
                                        if (count($postalCodeExp) == 2)
                                        {
                                            $postalCode = $postalCodeExp[0];
                                            $zip4 = $postalCodeExp[1];
                                        }    
                                    $data = array(
                                                'id' => $address_item->id,
                                                'addressType' => ($address_item->to_address ? 'recipient' : 'sender'),
                                                'firstName' => $address_item->first_name,
                                                'lastName' => $address_item->last_name,
                                                'city' => $address_item->city,
                                                'postalCode' => $postalCode,
                                                'zip4'=> $zip4,
                                                'address' => $address_item->address,
                                                'phone' => $address_item->phone,
                                                'email' => $address_item->email,
                                                'clientId' => $address_item->client_id,
                                            );
                                        if ($address_item->state) {
                                                $data['state'] = $address_item->state;
                                            }
                                        if ($address_item->country_id) {
                                                $data['countryId'] = (int) $address_item->country_id;
                                            }
                                        if ($address_item->to_address) {
                                            $data['locationType'] = ($address_item->international ? 'international' : 'domestic');    
                                        }
                                        if ($data['addressType'] == 'recipient' AND $data['locationType'] == 'domestic'){
                                             $data['countryId'] = 233;
                                        }
                                        
                                        $records[] = $data;    
                                   }
                                }
                                if (count($records) >0) {
                                    $response = [
                                    'requestId' => strtolower(Str::random(30)),
                                    'totalRecords'    => count($record_addresses),
                                    'records'    => $records,
                                    'statusCode' => $this->successStatus,
                                   ];
                                   return response()->json($response);
                                  } else {
                                    $response = [
                                    'requestId' => strtolower(Str::random(30)),
                                    'message'    => 'Record does not exist.',
                                    'statusCode' => 401,
                                   ];
                                   return response()->json($response);
                                }    

                               $api_statistics = DB::table('user_api_statistics')->where('user_id', $auth_token->user_id)->where('resource_path', $request->path())->first();

                                if (is_null($api_statistics)) { 
                                   $insId = DB::table('user_api_statistics')->insertGetId(
                                     ['user_id' => $auth_token->user_id, 'resource_path' => $request->path(), 'count_requests' => 1, 'created' => date('Y-m-d H:i:s')]
                                    );
                                } else {
                                    DB::table('user_api_statistics')->where('id', $api_statistics->id)->update(['count_requests' => $api_statistics->count_requests + 1, 'updated' => date('Y-m-d H:i:s')]);
                                }
                              } else {
                                $response = [
                                'requestId' => strtolower(Str::random(30)),
                                'statusCode' => 403,
                                'message'    => "You don't have permission to access.",
                               ];
                               return response()->json($response);
                            }
                        } else {
                            $response = [
                            'requestId' => strtolower(Str::random(30)),
                            'message' => 'Token is locked.',
                            'statusCode'    => 403,
                            ];
                            return response()->json($response);
                        }
                    }  
                } else {
                        $response = [
                        'requestId' => strtolower(Str::random(30)),
                        'statusCode'    => 405,
                        'error' => 'The requested URL '. $uri .'was not found on this server.',
                        ];
                        return response()->json($response);
                }
            } else {
                $response = [
                    'requestId' => strtolower(Str::random(30)),
                    'statusCode' => 400,
                    'error'    => 'Required parameter not passed: authToken',
                ];
                return response()->json($response);
            }
           // return response()->json($response);
        } catch (\Exception $e) {
            return $e->getMessage();
        }    
    }

    public function get_getInfo(Request $request, $id)
    {   
        try {
            $token = trim($request->authToken);
            $uri = $request->path(); 
            $records = array();
            if($token) {
                if($uri == 'api/v1/account/address/getInfo/'.$id) {
                    $auth_token = DB::table('user_api')->where('auth_token', $token)->first();
                    if (is_null($auth_token)) { 
                        $response = [
                        'requestId' => strtolower(Str::random(30)),
                        'message'    => 'Token does not exist.',
                        'statusCode' => 401,
                        ];
                        return response()->json($response);
                    } else {
                        if($auth_token->active == 1) {
                        $user = DB::table('users')->where('id', $auth_token->user_id)->where('in_archive', 0)->first();
                            if ($user) {
                            $user_address = DB::table('user_address')->where('id', $id)->first();
                            if ($user_address) {
                                     $zip4 = NULL;
                                     $postalCode =  $user_address->postalcode;
                                     $postalCodeExp = explode('-', $postalCode);
                                    if (count($postalCodeExp) == 2)
                                    {
                                        $postalCode = $postalCodeExp[0];
                                        $zip4 = $postalCodeExp[1];
                                    }    
                                    $data = array(
                                        'requestId' => strtolower(Str::random(30)),
                                        'statusCode'  => $this->successStatus,
                                        'firstName'  => $user_address->first_name,
                                        'lastName'=> $user_address->last_name,
                                        'city' => $user_address->city,
                                        'state' => $user_address->state,
                                        'postalCode'=> $postalCode,
                                        'address' => $user_address->address,
                                        'phone' => $user_address->phone
                                    );
                                    switch ($user_address->to_address)
                                    {
                                        case '0':
                                            $data['addressType'] = 'from';
                                        break;
                                        case '1': {
                                            $data['addressType'] = 'to';
                                            $data['state'] = $user_address->state;

                                            switch ($user_address->international)
                                            {
                                                case '0':
                                                    $data['locationType'] = 'domestic';
                                                    $data['countryId'] = 233;
                                                break;
                                                case '1':
                                                    $data['locationType'] = 'international';
                                                    $data['countryId'] = (int)$user_address->country_id;
                                                break;
                                            };
                                        };
                                        break;
                                    }

                                    $data['id'] = $user_address->id;
                                    $response = $data;
                                    return response()->json($response);
                                    
                                  } else {
                                    $response = [
                                    'requestId' => strtolower(Str::random(30)),
                                    'message'    => 'Record does not exist.',
                                    'statusCode' => 401,
                                   ];
                                   return response()->json($response);
                                }   

                               $api_statistics = DB::table('user_api_statistics')->where('user_id', $auth_token->user_id)->where('resource_path', $request->path())->first();

                                if (is_null($api_statistics)) { 
                                   $insId = DB::table('user_api_statistics')->insertGetId(
                                     ['user_id' => $auth_token->user_id, 'resource_path' => $request->path(), 'count_requests' => 1, 'created' => date('Y-m-d H:i:s')]
                                    );
                                } else {
                                    DB::table('user_api_statistics')->where('id', $api_statistics->id)->update(['count_requests' => $api_statistics->count_requests + 1, 'updated' => date('Y-m-d H:i:s')]);
                                }
                              } else {
                                $response = [
                                'requestId' => strtolower(Str::random(30)),
                                'statusCode' => 403,
                                'message'    => "You don't have permission to access.",
                               ];
                               return response()->json($response);
                            }
                        } else {
                            $response = [
                            'requestId' => strtolower(Str::random(30)),
                            'message' => 'Token is locked.',
                            'statusCode'    => 403,
                            ];
                            return response()->json($response);
                        }
                    }  
                } else {
                        $response = [
                        'requestId' => strtolower(Str::random(30)),
                        'statusCode'    => 405,
                        'error' => 'The requested URL '. $uri .'was not found on this server.',
                        ];
                        return response()->json($response);
                }
            } else {
                $response = [
                    'requestId' => strtolower(Str::random(30)),
                    'statusCode' => 400,
                    'error'    => 'Required parameter not passed: authToken',
                ];
                return response()->json($response);
            }
          //  return response()->json($response);
        } catch (\Exception $e) {
            return $e->getMessage();
        }    
    }

    public function delete_delete(Request $request, $id)
    {   
        try {
            $token = trim($request->authToken);
            $uri = $request->path(); 
            $records = array();
            if($token) {
                if($uri == 'api/v1/account/address/delete/'.$id) {
                    $auth_token = DB::table('user_api')->where('auth_token', $token)->first();
                    if (is_null($auth_token)) { 
                        $response = [
                        'requestId' => strtolower(Str::random(30)),
                        'message'    => 'Token does not exist.',
                        'statusCode' => 401,
                        ];
                        return response()->json($response);
                    } else {
                        if($auth_token->active == 1) {
                        $user = DB::table('users')->where('id', $auth_token->user_id)->where('in_archive', 0)->first();
                            if ($user) {
                            $user_address = DB::table('user_address')->where('id', $id)->first();
                            if ($user_address) {
                                    DB::table('user_address')->where('id', $id)->delete();
                                    $response = [
                                    'requestId' => strtolower(Str::random(30)),
                                    'statusCode' => $this->successStatus,
                                   ];
                                   return response()->json($response);
                                  } else {
                                    $response = [
                                    'requestId' => strtolower(Str::random(30)),
                                    'message'    => 'Record does not exist.',
                                    'statusCode' => 401,
                                   ];
                                   return response()->json($response);
                                }   

                               $api_statistics = DB::table('user_api_statistics')->where('user_id', $auth_token->user_id)->where('resource_path', $request->path())->first();

                                if (is_null($api_statistics)) { 
                                   $insId = DB::table('user_api_statistics')->insertGetId(
                                     ['user_id' => $auth_token->user_id, 'resource_path' => $request->path(), 'count_requests' => 1, 'created' => date('Y-m-d H:i:s')]
                                    );
                                } else {
                                    DB::table('user_api_statistics')->where('id', $api_statistics->id)->update(['count_requests' => $api_statistics->count_requests + 1, 'updated' => date('Y-m-d H:i:s')]);
                                }
                              } else {
                                $response = [
                                'requestId' => strtolower(Str::random(30)),
                                'statusCode' => 403,
                                'message'    => "You don't have permission to access.",
                               ];
                               return response()->json($response);
                            }
                        } else {
                            $response = [
                            'requestId' => strtolower(Str::random(30)),
                            'message' => 'Token is locked.',
                            'statusCode'    => 403,
                            ];
                            return response()->json($response);
                        }
                    }  
                } else {
                        $response = [
                        'requestId' => strtolower(Str::random(30)),
                        'statusCode'    => 405,
                        'error' => 'The requested URL '. $uri .'was not found on this server.',
                        ];
                        return response()->json($response);
                }
            } else {
                $response = [
                    'requestId' => strtolower(Str::random(30)),
                    'statusCode' => 400,
                    'error'    => 'Required parameter not passed: authToken',
                ];
                return response()->json($response);
            }
           // return response()->json($response);
        } catch (\Exception $e) {
            return $e->getMessage();
        }    
    }


    public function post_create(Request $request)
    {   
        try {
            $token = trim($request->authToken);
            $uri = $request->path();
            $records = array();
            if($token) {
                if($uri == 'api/v1/account/address/create') {
                    $auth_token = DB::table('user_api')->where('auth_token', $token)->first();
                    if (is_null($auth_token)) { 
                        $response = [
                        'requestId' => strtolower(Str::random(30)),
                        'message'    => 'Token does not exist.',
                        'statusCode' => 401,
                        ];
                        return response()->json($response);
                    } else {
                        if($auth_token->active == 1) {
                        $user = DB::table('users')->where('id', $auth_token->user_id)->where('in_archive', 0)->first();
                            if ($user) {

                                $address_type = trim($request->addressType);
                                $location_type = trim($request->locationType);
                                $country_id = trim($request->countryId);
                                $state_code = trim($request->state);

                                if($address_type == 'recipient') {
                                    if($location_type == 'international') {
                                        $rules = array (
                                                'addressType' => 'required',
                                                'locationType' => 'required',
                                                'firstName' => 'required|max:24|regex:/^[a-zA-Z0-9_\s]+$/',
                                                'lastName' => 'required|max:24|regex:/^[a-zA-Z0-9_\s]+$/',
                                                'address' => 'required',
                                                'city' => 'required',
                                                'postalCode' => 'required|max:10|regex:/^[0-9-]+$/',
                                                'countryId' => 'required|regex:/^[0-9]+$/|max:3',
                                                'state' => 'regex:/^[a-zA-Z]+$/|max:2|min:2',
                                                'phone' => 'regex:/^[0-9]+$/|max:10',
                                            );
                                    } else {
                                        $rules = array (
                                                'addressType' => 'required',
                                                'locationType' => 'required',
                                                'firstName' => 'required|max:24|regex:/^[a-zA-Z0-9_\s]+$/',
                                                'lastName' => 'required|max:24|regex:/^[a-zA-Z0-9_\s]+$/',
                                                'address' => 'required',
                                                'city' => 'required',
                                                'postalCode' => 'required|max:10|regex:/^[0-9-]+$/',
                                                'state' => 'required|regex:/^[a-zA-Z]+$/|max:2|min:2',
                                                'countryId' => 'regex:/^[0-9]+$/|max:3',
                                                'phone' => 'regex:/^[0-9]+$/|max:10',
                                            );
                                    }
                                } else {
                                    $rules = array (
                                                'addressType' => 'required',
                                               // 'locationType' => 'required',
                                                'firstName' => 'required|max:24|regex:/^[a-zA-Z0-9_\s]+$/',
                                                'lastName' => 'required|max:24|regex:/^[a-zA-Z0-9_\s]+$/',
                                                'address' => 'required',
                                                'city' => 'required',
                                                'postalCode' => 'required|max:10|regex:/^[0-9-]+$/',
                                                'state' => 'required|regex:/^[a-zA-Z]+$/|max:2|min:2',
                                                'countryId' => 'regex:/^[0-9]+$/|max:3',
                                                'phone' => 'regex:/^[0-9]+$/|max:10',
                                            );
                                }
                                
                                $validator = Validator::make($request->all(), $rules);

                                if($validator->fails()) {
                                    $response = [
                                    'requestId' => strtolower(Str::random(30)),
                                    'message'    => 'Validation Error.',
                                    'error'    => $validator->errors(),
                                    'statusCode' => 422,
                                   ];
                                   return response()->json($response);

                                 } else {

                                $data = array (
                                                'user_id' => trim($auth_token->user_id),
                                                'first_name' => trim($request->firstName),
                                                'last_name' => trim($request->lastName),
                                                'city' => trim($request->city),
                                                'address' => trim($request->address),
                                                'phone' => trim($request->phone),
                                                'postalcode' => trim($request->postalCode),
                                                'created' => date('Y-m-d H:i:s'),
                                            );

                                switch ($address_type)
                                {
                                    case 'sender':
                                        $data['to_address'] = 0;
                                        $data['state'] = trim($request->state);
                                    break;
                                    case 'recipient': {
                                        $data['to_address'] = 1;
                                        switch ($location_type)
                                        {
                                            case 'domestic':
                                                $data['international'] = 0;
                                                $data['state'] = trim($request->state);
                                            break;
                                            case 'international':
                                                $data['international'] = 1;
                                                $data['country_id'] = trim($request->countryId);
                                            break;
                                        };
                                    };
                                    break;
                                }

                                 $insId = DB::table('user_address')->insertGetId($data);
                                 $response = [
                                    'requestId' => strtolower(Str::random(30)),
                                    'id' => (int)$insId,
                                    'user_id' => $auth_token->user_id,
                                    'statusCode' => $this->successStatus,
                                   ];
                                   return response()->json($response);
                               } 
                                
                               $api_statistics = DB::table('user_api_statistics')->where('user_id', $auth_token->user_id)->where('resource_path', $request->path())->first();

                                if (is_null($api_statistics)) { 
                                   $insId = DB::table('user_api_statistics')->insertGetId(
                                     ['user_id' => $auth_token->user_id, 'resource_path' => $request->path(), 'count_requests' => 1, 'created' => date('Y-m-d H:i:s')]
                                    );
                                } else {
                                    DB::table('user_api_statistics')->where('id', $api_statistics->id)->update(['count_requests' => $api_statistics->count_requests + 1, 'updated' => date('Y-m-d H:i:s')]);
                                }
                              } else {
                                $response = [
                                'requestId' => strtolower(Str::random(30)),
                                'statusCode' => 403,
                                'message'    => "You don't have permission to access.",
                               ];
                               return response()->json($response);
                            }
                        } else {
                            $response = [
                            'requestId' => strtolower(Str::random(30)),
                            'message' => 'Token is locked.',
                            'statusCode'    => 403,
                            ];
                            return response()->json($response);
                        }
                    }  
                } else {
                        $response = [
                        'requestId' => strtolower(Str::random(30)),
                        'statusCode'    => 405,
                        'error' => 'The requested URL '. $uri .'was not found on this server.',
                        ];
                        return response()->json($response);
                }
            } else {
                $response = [
                    'requestId' => strtolower(Str::random(30)),
                    'statusCode' => 400,
                    'error'    => 'Required parameter not passed: authToken',
                ];
                return response()->json($response);
            }
           // return response()->json($response);
        } catch (\Exception $e) {
            return $e->getMessage();
        }    
    }

    public function put_update(Request $request, $id)
    {   
        try {
            $token = trim($request->authToken);
            $uri = $request->path();
            $records = array();
            if($token) {
                if($uri == 'api/v1/account/address/update/'.$id) {
                    $auth_token = DB::table('user_api')->where('auth_token', $token)->first();
                    if (is_null($auth_token)) { 
                        $response = [
                        'requestId' => strtolower(Str::random(30)),
                        'message'    => 'Token does not exist.',
                        'statusCode' => 401,
                        ];
                        return response()->json($response);
                    } else {
                        if($auth_token->active == 1) {
                        $user = DB::table('users')->where('id', $auth_token->user_id)->where('in_archive', 0)->first();
                            if ($user) {

                                $user_address = DB::table('user_address')->where('id', $id)->first();
                                if($user_address) {

                                    $address_type = trim($request->addressType);
                                    $location_type = trim($request->locationType);
                                    $country_id = trim($request->countryId);
                                    $state_code = trim($request->state);
                                    if($address_type == 'recipient') {
                                    if($location_type == 'international') {
                                        $rules = array (
                                                'addressType' => 'required',
                                                'locationType' => 'required',
                                                'firstName' => 'required|max:24|regex:/^[a-zA-Z0-9_\s]+$/',
                                                'lastName' => 'required|max:24|regex:/^[a-zA-Z0-9_\s]+$/',
                                                'address' => 'required',
                                                'city' => 'required',
                                                'postalCode' => 'required|max:10|regex:/^[0-9-]+$/',
                                                'countryId' => 'required|regex:/^[0-9]+$/|max:3',
                                                'state' => 'regex:/^[a-zA-Z]+$/|max:2|min:2',
                                                'phone' => 'regex:/^[0-9]+$/|max:10',
                                            );
                                    } else {
                                        $rules = array (
                                                'addressType' => 'required',
                                                'locationType' => 'required',
                                                'firstName' => 'required|max:24|regex:/^[a-zA-Z0-9_\s]+$/',
                                                'lastName' => 'required|max:24|regex:/^[a-zA-Z0-9_\s]+$/',
                                                'address' => 'required',
                                                'city' => 'required',
                                                'postalCode' => 'required|max:10|regex:/^[0-9-]+$/',
                                                'state' => 'required|regex:/^[a-zA-Z]+$/|max:2|min:2',
                                                'countryId' => 'regex:/^[0-9]+$/|max:3',
                                                'phone' => 'regex:/^[0-9]+$/|max:10',
                                            );
                                    }
                                } else {
                                    $rules = array (
                                                'addressType' => 'required',
                                               // 'locationType' => 'required',
                                                'firstName' => 'required|max:24|regex:/^[a-zA-Z0-9_\s]+$/',
                                                'lastName' => 'required|max:24|regex:/^[a-zA-Z0-9_\s]+$/',
                                                'address' => 'required',
                                                'city' => 'required',
                                                'postalCode' => 'required|max:10|regex:/^[0-9-]+$/',
                                                'state' => 'required|regex:/^[a-zA-Z]+$/|max:2|min:2',
                                                'countryId' => 'regex:/^[0-9]+$/|max:3',
                                                'phone' => 'regex:/^[0-9]+$/|max:10',
                                            );
                                }
                                    $validator = Validator::make($request->all(), $rules);
                                    if($validator->fails()) {
                                    $response = [
                                    'requestId' => strtolower(Str::random(30)),
                                    'message'    => 'Validation Error.',
                                    'error'    => $validator->errors(),
                                    'statusCode' => 422,
                                   ];
                                   return response()->json($response);
                                 } else {

                                $data = array (
                                                'user_id' => trim($auth_token->user_id),
                                                'first_name' => trim($request->firstName),
                                                'last_name' => trim($request->lastName),
                                                'city' => trim($request->city),
                                                'address' => trim($request->address),
                                                'phone' => trim($request->phone),
                                                'postalcode' => trim($request->postalCode),
                                                'updated' => date('Y-m-d H:i:s'),
                                            );    

                                switch ($address_type)
                                {
                                    case 'sender':
                                        $data['to_address'] = 0;
                                        $data['state'] = trim($request->state);
                                    break;
                                    case 'recipient': {
                                        $data['to_address'] = 1;
                                        switch ($location_type)
                                        {
                                            case 'domestic':
                                                $data['international'] = 0;
                                                $data['state'] = trim($request->state);
                                            break;
                                            case 'international':
                                                $data['international'] = 1;
                                                $data['country_id'] = trim($request->countryId);
                                            break;
                                        };
                                    };
                                    break;
                                }

                                $updateArrdress = DB::table('user_address')
                                        ->where('id', $id)
                                        ->update($data);

                                 if ($updateArrdress) {
                                    $response = [
                                    'requestId' => strtolower(Str::random(30)),
                                    'id' => (int)$id,
                                    'user_id' => $auth_token->user_id,
                                    'statusCode' => $this->successStatus,
                                   ];
                                   return response()->json($response);
                                  } else {
                                    $response = [
                                    'requestId' => strtolower(Str::random(30)),
                                    'message'    => 'No content changed.',
                                    'statusCode' => 204,
                                   ];
                                   return response()->json($response);
                                  }
                                } 

                                } else {
                                    $response = [
                                    'requestId' => strtolower(Str::random(30)),
                                    'message'    => 'Record does not exist.',
                                    'statusCode' => 401,
                                   ];
                                   return response()->json($response);
                                }
                                
                               $api_statistics = DB::table('user_api_statistics')->where('user_id', $auth_token->user_id)->where('resource_path', $request->path())->first();

                                if (is_null($api_statistics)) { 
                                   $insId = DB::table('user_api_statistics')->insertGetId(
                                     ['user_id' => $auth_token->user_id, 'resource_path' => $request->path(), 'count_requests' => 1, 'created' => date('Y-m-d H:i:s')]
                                    );
                                } else {
                                    DB::table('user_api_statistics')->where('id', $api_statistics->id)->update(['count_requests' => $api_statistics->count_requests + 1, 'updated' => date('Y-m-d H:i:s')]);
                                }
                              } else {
                                $response = [
                                'requestId' => strtolower(Str::random(30)),
                                'statusCode' => 403,
                                'message'    => "You don't have permission to access.",
                               ];
                               return response()->json($response);
                            }
                        } else {
                            $response = [
                            'requestId' => strtolower(Str::random(30)),
                            'message' => 'Token is locked.',
                            'statusCode'    => 403,
                            ];
                            return response()->json($response);
                        }
                    }  
                } else {
                        $response = [
                        'requestId' => strtolower(Str::random(30)),
                        'statusCode'    => 405,
                        'error' => 'The requested URL '. $uri .'was not found on this server.',
                        ];
                        return response()->json($response);
                }
            } else {
                $response = [
                    'requestId' => strtolower(Str::random(30)),
                    'statusCode' => 400,
                    'error'    => 'Required parameter not passed: authToken',
                ];
                return response()->json($response);
            }
          //  return response()->json($response);
        } catch (\Exception $e) {
            return $e->getMessage();
        }    
    }
}
<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\User;
use App\Http\Resources\UserResource;
use App\Models\Profile;
use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\Hash;
use Carbon\Carbon; 
use JWTFactory;
use JWTAuth;
use Validator;
use Config;
use Log;
use Event;
use App\Events\UserRegistered;

use DB;
use App\Models\MailClass;
use App\Models\Country;
use App\Models\State;
use App\Models\Apirequests;
use App\Models\Userapi;
use App\Models\Userapistatistics;
use Illuminate\Support\Str;
use Tymon\JWTAuth\Exceptions\JWTException;

class MailClassController extends Controller
{   

    public function get_Classes()
    {

        $mailclasses = MailClass::where('mail_type', '=', 0)->get()->toArray();
        if (count($mailclasses) > 0) {
            $response = [
            'requestId' => strtolower(Str::random(30)),
            'records'    => $mailclasses,
            'statusCode' => 200,
            'status' => 1
           ];

          } else {
            $response = [
            'requestId' => strtolower(Str::random(30)),
            'message'    => 'Record does not exist.',
            'statusCode' => 404,
            'status' => 0
           ];
        }
        return response()->json($response);

    }
}
<?php

namespace App\Http\Controllers\Api\Payment;

use App\Models\User;
use App\Models\Profile;
use App\Models\UserCreditCards;
use App\Models\PaymentLogs;
use App\Models\Payments;
use App\Models\Country;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Str;


use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;

/** All Paypal Details class **/
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Rest\ApiContext;
use Redirect;
use URL;
use Auth;
use DB;
use Exception;
use Log;
use Session;
use Hash;
use Image;

class PaymentController extends Controller
{
   /**
   * Create a new AuthController instance.
   *
   * @return void
   */


  private $_api_context;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

        /** PayPal api context **/
        $paypal_conf = \Config::get('paypal');
        $this->_api_context = new ApiContext(new OAuthTokenCredential(
            $paypal_conf['client_id'],
            $paypal_conf['secret'])
        );
        $this->_api_context->setConfig($paypal_conf['settings']);

    }

 public function payWithpaypal(Request $request)
    {

        try {

        if (! $user = JWTAuth::parseToken()->authenticate()) {
            return response()->json(['user_not_found'], 404);
        } else {
                
        $AMT= trim($request->amount);
        $rules[] = '';

        $rules['amount'] = 'required|regex:/^[0-9]+$/|max:5|min:2';

        $validator = Validator::make($request->all(), $rules);

        if($validator->fails() || $AMT < 10) {

        $response = [
        'requestId'  => strtolower(Str::random(30)),
        'message'    => 'Validation Error.',
        'error'      => $validator->errors(),
        'statusCode' => 422,
        ];
        if($AMT < 10){
            $response = [
            'Amount' => 'Enter amount greater than $10.',
            ];
            }

        return response()->json($response);  

        }else{

        $payer = new Payer();
        $payer->setPaymentMethod('paypal');

        $item_1 = new Item();

        $item_1->setName('Item 1') /** item name **/
            ->setCurrency('USD')
            ->setQuantity(1)
            ->setPrice($request->get('amount')); /** unit price **/

        $item_list = new ItemList();
        $item_list->setItems(array($item_1));
    
        $amount = new Amount();
        $amount->setCurrency('USD')
            ->setTotal($request->get('amount'));

        $transaction = new Transaction();
        $transaction->setAmount($amount)
            ->setItemList($item_list)
            ->setDescription('Your transaction description');

        $redirect_urls = new RedirectUrls();

        if($request->redirecturl!='' && $request->cancelurl!=''){
            $redirect_urls->setReturnUrl(URL::to($request->redirecturl.$user->id)) /** Specify return URL **/
                      ->setCancelUrl(URL::to($request->cancelurl));
        } else {
            $base_url = url('/');
            $redirect_urls->setReturnUrl(URL::to($base_url.'/api/payment/status/'.$user->id)) /** Specify return URL **/
                      ->setCancelUrl(URL::to('http://vipparcel.com')); 
        }
        
        //$redirect_urls->setReturnUrl(URL::to('http://localhost:3000/addfunds')) /** Specify return URL **/
                      //->setCancelUrl(URL::to('http://vipparcel.com'));

                      

        $payment = new Payment();
        $payment->setIntent('Sale')
            ->setPayer($payer)
            ->setRedirectUrls($redirect_urls)
            ->setTransactions(array($transaction));

        /** dd($payment->create($this->_api_context));exit; **/
        try {

            $payment->create($this->_api_context);

       
        } catch (\PayPal\Exception\PPConnectionException $ex) {

            if (\Config::get('app.debug')) {

                \Session::put('error', 'Connection timeout');
                return Redirect::to('/');

            } else {

                \Session::put('error', 'Some error occur, sorry for inconvenient');
                return Redirect::to('/');

            }

        }

        foreach ($payment->getLinks() as $link) {

            if ($link->getRel() == 'approval_url') {

                $redirect_url = $link->getHref();
                break;

            }

        }

        /** add payment ID to session **/

       Session::put('paypal_payment_id', $payment->getId());

        if (isset($redirect_url)) {

            /** redirect to paypal **/
             return response()->json([$redirect_url], 200);

        }

        \Session::put('error', 'Unknown error occurred');
        return Redirect::to('/');
        }
       } 

    } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

            return response()->json(['token_expired'], $e->getStatusCode());

    } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

            return response()->json(['token_invalid'], $e->getStatusCode());

    } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {

            return response()->json(['token_absent'], $e->getStatusCode());
    }
 }

    public function getPaymentStatus($userid, Request $request)
    {
        
    try {


        /** Get the payment ID before session clear **/

        //$payment_id = Session::get('paypal_payment_id');
        if($request){
            if($request->PayerID!='' && $request->token!=''){
                $payment_id = $request->paymentId;
                $payment = Payment::get($payment_id, $this->_api_context);
                $execution = new PaymentExecution();
                $execution->setPayerId($request->PayerID);
            } else {
                return Redirect::away($request->redirecturl);
            }

        } else {
           $payment_id = Input::get('paymentId');
            Session::forget('paypal_payment_id');
            if (empty(Input::get('PayerID')) || empty(Input::get('token'))) {

                \Session::put('error', 'Payment failed');
                
               return Redirect::to('/');

            }

            $payment = Payment::get($payment_id, $this->_api_context);
            $execution = new PaymentExecution();
            $execution->setPayerId(Input::get('PayerID')); 
        }
        

        /**Execute the payment **/
        $result = $payment->execute($execution, $this->_api_context);
  
          if ($result->getState() == 'approved') {
            $rr         = array();
            $sales_info = array();
            $rr         = $result->payer->payer_info;
            $sales_info = $result->transactions[0]->related_resources[0];
            $amount     = $result->transactions[0]->amount;

            $txn_id     = $sales_info->sale->id;

            $user = User::where('id', '=', $userid)->first();
            $newbalance = $amount->total + $user->balance;
                            

           $data = serialize([
                    'payer_email'=> $rr->email,
                    'first_name' => $rr->first_name,
                    'last_name'  => $rr->last_name,
                    'billing'    => $rr->shipping_address,
                    'response'   => $payment->toArray()
            ]);

            $payment_log = PaymentLogs::insertGetId(
                            ['user_id' => $userid, 'service' =>'paypal' ,'txn_id'=> $txn_id ,'status'=>'1','data'=> $data] 
                           );

            if($payment_log){
                $created = date('Y-m-d h:i:s');
                $payments = Payments::insertGetId(
                            ['to_user_id' => $userid, 'amount'=>$amount->total, 'created' => $created, 'type' =>'paypal' ,'txn_id'=> $txn_id ] 
                            );
                if($payments){  
                    $updateuserbalance = User::where('id', '=', $userid)
                     ->update(['balance' => $newbalance]);
                }
                if($request->redirecturl){
                    return Redirect::away($request->redirecturl);
                } else {
                    return Redirect::away('http://vipparcel.com');
                }
                
            }else{
            
            return response()->json("Server Error occured", 400);

            }
         
       }
      
     } catch (\Exception $e) {
         return response()->json(['status' => 0, 'message' => $e->getMessage()], 403);
        }

    }  

   public function payWithcards(Request $request)
    {

        try {

        if (! $user = JWTAuth::parseToken()->authenticate()) {
            return response()->json(['user_not_found'], 404);
        } else {

            $rules = [];
            $rules['cvv'] = 'required|regex:/^[0-9]+$/|max:3|min:3';
            $rules['amount'] = 'required|regex:/^[0-9]+$/|max:5|min:2';
            
            $validator = Validator::make($request->all(), $rules);
            if($validator->fails() ||  $request->amount < 10) {

            $response = [
            'requestId'  => strtolower(Str::random(30)),
            'message'    => 'Validation Error.',
            'error'      => $validator->errors(),
            'statusCode' => 422,
            'status' => 0,
            ];

            return response()->json($response);
            } else {

            $cvv    = trim($request->cvv);
            $cardId    = trim($request->cardId);

            $cardInfo = UserCreditCards::where('id', '=', $cardId)->first();
            if($cardInfo->cvv!=$cvv){
                $response = [
                'requestId' => strtolower(Str::random(30)),
                'statusCode' => 403,
                'message'    => "CVV is not matched",
                'status' => 0,
               ];
               return response()->json($response);
            } else {

                $userDetail = Profile::where('user_id', '=', $user->id)->first();
                $countryDetail = Country::where('idCountry', '=', $userDetail->country_id)->first();

                if($user->test_mode=='1'){
                    $PayflowUrl = 'https://pilot-payflowpro.paypal.com/';
                } else {
                    $PayflowUrl = 'https://payflowpro.paypal.com';
                }
                $payment_log = array();
                $transactions = array();

                $AMT = $request->amount;
                $request_params = array(
                    'PARTNER' => 'PayPal', 
                    'USER'    => env('PAYFLOW_USER'), 
                    'PWD'     => env('PAYFLOW_PWD'), 
                    'VENDOR'  => env('PAYFLOW_VENDOR'), 
                    'TENDER'  => 'C', 
                    'TRXTYPE' => 'S',                   
                    'IPADDRESS' => '192.168.1.185',
                    'VERBOSITY' => 'MEDIUM',
                    'ACCT'      => $cardInfo->number,                        
                    'EXPDATE'   => $cardInfo->exp_month.$cardInfo->exp_year,           
                    'CVV2'      => $cardInfo->cvv, 
                    'FIRSTNAME' => $userDetail->first_name, 
                    'LASTNAME'  => $userDetail->last_name, 
                    'STREET'    => $userDetail->address.' '.$userDetail->address_2, 
                    'CITY'      => $userDetail->city, 
                    'STATE'     => $userDetail->state,                     
                    'COUNTRYCODE' => $countryDetail->countryCode, 
                    'ZIP'       => $userDetail->zip, 
                    'AMT'       => $AMT, 
                );
                $nvp_string = '';
                foreach($request_params as $var=>$val){
                    $nvp_string .= '&'.$var.'='.urlencode($val);    
                }
                $nvp_string = substr($nvp_string, 1);
                $curl = curl_init();
                curl_setopt($curl, CURLOPT_VERBOSE, 1);
                curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
                curl_setopt($curl, CURLOPT_TIMEOUT, 30);
                curl_setopt($curl, CURLOPT_URL, $PayflowUrl);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($curl, CURLOPT_POSTFIELDS, $nvp_string);
                $headers = array();
                $headers[] = "Content-Type: application/x-www-form-urlencoded";
                curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
                $NVPString = curl_exec($curl);   
                curl_close($curl);

                $transaction = array();
                $payment_log = array();
                while(strlen($NVPString)){
                    $keypos= strpos($NVPString,'=');
                    $keyval = substr($NVPString,0,$keypos);
                    $valuepos = strpos($NVPString,'&') ? strpos($NVPString,'&'): strlen($NVPString);
                    $valval = substr($NVPString,$keypos+1,$valuepos-$keypos-1);
                    $transaction[$keyval] = urldecode($valval);
                    $NVPString = substr($NVPString,$valuepos+1,strlen($NVPString));
                }

                $payment_log['card'] = array('card_name'=>$cardInfo->name,'card_number'=>$cardInfo->number,'card_month'=>$cardInfo->exp_month,'card_year'=>$cardInfo->exp_year);

                $payment_log['billing'] = array('id'=>$userDetail->id,'user_id'=>$userDetail->user_id,'last_name'=>$userDetail->last_name,'middle_name'=>$userDetail->middle_name,'first_name'=>$userDetail->first_name,'reg_last_name'=>$userDetail->reg_last_name,'reg_first_name'=>$userDetail->reg_first_name,'phone'=>$userDetail->phone,'created'=>$userDetail->created,'updated'=>$userDetail->updated,'address'=>$userDetail->address,'address_2'=>$userDetail->address_2,'city'=>$userDetail->city,'state'=>$userDetail->state,'country_id'=>$userDetail->country_id,'zip'=>$userDetail->zip,'skype'=>$userDetail->skype,'driver_licence'=>$userDetail->driver_licence,'is_international'=>$userDetail->is_international,'phone_number_info'=>$userDetail->phone_number_info,'phone_number_info_request_id'=>$userDetail->phone_number_info_request_id,'province'=>$userDetail->province,'phone_number_info_error'=>$userDetail->phone_number_info_error);

                $payment_log['response'] = $transaction;
                $PaymentLog = serialize($payment_log); 
                $created = date('Y-m-d h:i:s');

                $newtransaction = PaymentLogs::insertGetId(['user_id' => $user->id, 'service' => 'paypal', 'txn_id' => $transaction['PNREF'] , 'status' => '1', 'data' => serialize($payment_log), 'created' => $created]);

                $payment = Payments::insertGetId(['to_user_id' => $user->id, 'amount' => $AMT, 'description' => '' , 'created' => $created, 'type' =>'card', 'txn_id' => $transaction['PNREF'],'txn_ppref' => $transaction['PPREF']]);

                    if($payment){ 
                        $newbalance = $AMT + $user->balance;
                        $updateuserbalance = User::where('id', '=', $user->id)
                                    ->update(['balance' => $newbalance]);
                        $response = [
                        'requestId' => strtolower(Str::random(30)),
                        'statusCode' => 200,
                        'message'    => "Funds Added successfully.",
                        'status' => 1,
                       ];
                       return response()->json($response);
                    } else {
                        $response = [
                        'requestId' => strtolower(Str::random(30)),
                        'statusCode' => 403,
                        'message'    => "Failed",
                        'status' => 0,
                       ];
                       return response()->json($response);
                    }

                }

            }

        }

    } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

            return response()->json(['token_expired'], $e->getStatusCode());

    } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

            return response()->json(['token_invalid'], $e->getStatusCode());

    } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {

            return response()->json(['token_absent'], $e->getStatusCode());
    }
 }

 public function getbalance(Request $request){

        //$ownerId = Auth::user()->id;
        if (! $user = JWTAuth::parseToken()->authenticate()) {
            return response()->json(['user_not_found'], 404);
        } else {

        $ownerId = $user->id;
        echo $ownerId;
        exit();

        $queryUser = User::query();
        $getbalance =  $queryUser->where('id','=',$ownerId)
                                        ->first();

        if(empty($getbalance)) {
            return response()->json(['status' => false, 'total_balance' =>$getbalance]);  
        }
        return response()->json(['status' => true, 'total_balance' =>$getbalance]);  
    }
  }
 
}
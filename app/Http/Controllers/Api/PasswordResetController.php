<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Notifications\PasswordResetRequest;
use App\Notifications\PasswordResetSuccess;
use App\Models\User;
use App\Models\Profile;
use App\Models\PasswordReset;
use Illuminate\Support\Facades\Hash;
use Validator;
use Config;
use Log;
use Event;

class PasswordResetController extends Controller
{
    /**
     * Create token password reset
     *
     * @param  [string] email
     * @return [string] message
     */
    public function create(Request $request)
    {

        $rules = [];

        $rules['email'] = 'required|string|email';

        $validator = Validator::make($request->all(), $rules);

        if($validator->fails()){
          return response()->json(['error' => $validator->errors()], 409);
        }

        $user = User::where('email', $request->email)->first();

        if (!$user)
            return response()->json([
                'message' => 'We can,t find a user with that e-mail address.'
            ], 404);

        $findRequest   = PasswordReset::where('email', '=', $user->email)->first();

        if($findRequest){
        $update_req = PasswordReset::where('email', $user->email)->update([
                                'email' => $user->email,
                                'token' => str_random(60)
                            ]);
        if($update_req){
            $passwordReset   = PasswordReset::where('email', '=', $user->email)->first();
        }

        }else{
            $passwordReset = PasswordReset::create(
                            [
                                'email' => $user->email,
                                'token' => str_random(60)
                            ]
                        );
        }

  
        if ($user && $passwordReset)
            $user->notify(
                new PasswordResetRequest($passwordReset->token,$passwordReset->email)
            );

        return response()->json([
            'message' => 'We have e-mailed your password reset link!',
            'status' => 'Success'
        ]);
    }

    /**
     * Find token password reset
     *
     * @param  [string] $token
     * @return [string] message
     * @return [json] passwordReset object
     */
    public function find($token)
    {
        $passwordReset = PasswordReset::where('token', $token)->first();

        if (!$passwordReset)
            return response()->json([
                'message' => 'This password reset token is invalid.'
            ], 404);
        if (Carbon::parse($passwordReset->updated_at)->addMinutes(720)->isPast()) {
            PasswordReset::where('token', $token)->delete();
            return response()->json([
                'message' => 'This password reset token is invalid.'
            ], 404);
        }

        return response()->json($passwordReset);
    }

     /**
     * Reset password
     *
     * @param  [string] email
     * @param  [string] password
     * @param  [string] password_confirmation
     * @param  [string] token
     * @return [string] message
     * @return [json] user object
     */
    public function reset(Request $request)
    {

        $passwordReset = PasswordReset::where('token', trim($request->token))->first();

        if (!$passwordReset)
            return response()->json([
                'message' => 'This password reset token is invalid.',
                'status' => '0'
            ], 404);
        if (Carbon::parse($passwordReset->updated_at)->addMinutes(720)->isPast()) {
            PasswordReset::where('token', $token)->delete();
            return response()->json([
                'message' => 'This password reset token is invalid.',
                'status' => '0'
            ], 404);
        }

        //return response()->json($passwordReset);

        $rules = [];

        $rules['email'] = 'required|string|email';
        $rules['password'] = 'required';
      //  $rules['token'] = 'required|string';


        $validator = Validator::make($request->all(), $rules);

        if($validator->fails()){
          return response()->json(['message' => 'Validation error', 'status' => '0'], 409);
        }

        $passwordReset = PasswordReset::where([
            ['token', trim($request->token)],
            ['email', trim($request->email)]
        ])->first();

        if (!$passwordReset)
            return response()->json([
                'message' => 'This password reset token is invalid.',
                'status' => '0'
            ], 404);

        $user = User::where('email', $passwordReset->email)->first();

        if (!$user)
            return response()->json([
                'message' => 'We can,t find a user with that e-mail address.',
                'status' => '0'
            ], 404);

        $password = Hash::make(trim($request->password));

        $updateuser = User::where('id', $user->id)
                           ->update(['password' => $password]);

        if($updateuser){
         PasswordReset::where('token', $passwordReset->token)->delete();
         $userprofile = Profile::where('user_id', $user->id)->first();
        $user->notify(new PasswordResetSuccess($passwordReset));

       

            $responsemsg = [
            'message' => 'Password reset successfully',
            'status' => 1,
            ];
            return response()->json($responsemsg);
        } else {
            $responsemsg = [
            'message' => 'Password not reset',
            'status' => 0,
            ];
            return response()->json($responsemsg);
        }

    }
}
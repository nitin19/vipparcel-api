<?php

namespace App\Http\Controllers\Api\Creditcard;

use App\Models\User;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Str;
use Carbon\Carbon;
use Auth;
use DB;
use App\Models\Country;
use App\Models\UserCreditCards;
use App\Models\BlackListCards;
use App\Models\Profile;
use App\Models\PaymentLogs;
use App\Models\CardVerifications;
use App\Models\Payments;
use App\Models\Userapistatistics;
use Exception;
use Log;
use Session;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class CardsInfoController extends Controller
{

    public $successStatus = 200;

    public function __construct()
    {
       //
    }
    public function getList(Request $request){
        try{
            if(!$user = JWTAuth::parseToken()->authenticate()) {
                $response = [
                'requestId' => strtolower(Str::random(30)),
                'statusCode' => 404,
                'message'    => "user_not_found",
                'status' => 0,
               ];
               return response()->json($response);
                } else {
                if($user){

                   $api_statistics = Userapistatistics::where('user_id', $user->id)->where('resource_path', $request->path())->first();
                        if (is_null($api_statistics)) {  
                           $insId = Userapistatistics::insertGetId([
                            'user_id' => $user->id,
                            'resource_path' => $request->path(),
                            'count_requests' => 1,
                            'created' => Carbon::now()
                          ]);
                        } else {
                            Userapistatistics::where('id', $api_statistics->id)->update([
                              'count_requests' => $api_statistics->count_requests + 1,
                              'updated' => Carbon::now()
                             ]);
                        } 
                         
                   $user_cards = UserCreditCards::where('user_id', '=', $user->id)->where(
                   function($query){
                        $query->whereNull('is_deleted');
                        $query->orWhere('is_deleted', '=', '0');
                    })->get();

                   $record_cards = $user_cards->toArray();
                   $records = array();
                   if(count($record_cards) > 0){
                    $cardtype = array(
                        "visa"       => "/^4[0-9]{12}(?:[0-9]{3})?$/",
                        "mastercard" => "/^5[1-5][0-9]{14}$/",
                        "amex"       => "/^3[47][0-9]{13}$/",
                        "discover"   => "/^6(?:011|5[0-9]{2})[0-9]{12}$/",
                        "jcb"   => "/^(?:2131|1800|35\d{3})\d{11}$/",
                        "dinersclub"   => "/^3(?:0[0-5]|[68][0-9])[0-9]{11}$/",
                    );
                    
                        foreach($record_cards as $carditem){
                            $card_item = (object)$carditem;
                            if($card_item->is_verified=='1'){$verified='true';} else {$verified='false';}
                            if($card_item->is_default=='1'){$default='true';} else {$default='false';}
                            if (preg_match($cardtype['visa'],$card_item->number)){ $type= "Visa"; }
                            else if (preg_match($cardtype['mastercard'],$card_item->number))
                            { $type= "Mastercard"; }
                            else if (preg_match($cardtype['amex'],$card_item->number))
                            { $type= "American Express"; }
                            else if (preg_match($cardtype['discover'],$card_item->number))
                            { $type= "Discover"; }
                            else if (preg_match($cardtype['jcb'],$card_item->number))
                            { $type= "JCB"; }
                            else if (preg_match($cardtype['dinersclub'],$card_item->number))
                            { $type= "Diners Club"; }
                            else
                            { $type = "Unknown"; }

                        $data = array(
                            'id' => $card_item->id,
                            'name' => $card_item->name,
                            'type' => $type,
                            'ending' => substr($card_item->number, -4),
                            'expires' => $card_item->exp_month.'/20'.$card_item->exp_year,
                            'verified' => $verified,
                            'default' => $default,
                            'created' => $card_item->created,
                        );

                            $records[] = $data;
                        }
                   }
                   if (count($records) > 0) {
                        $response = [
                        'requestId' => strtolower(Str::random(30)),
                        'totalRecords' => count($record_cards),
                        'records'    => $records,
                        'statusCode' => 200,
                       ];
                       return response()->json($response);
                      } else {
                        $response = [
                        'requestId' => strtolower(Str::random(30)),
                        'totalRecords' => count($record_cards),
                        'message'    => 'Record does not exist.',
                        'statusCode' => 204,
                       ];
                       return response()->json($response);
                    }
                } else {
                    $response = [
                    'requestId' => strtolower(Str::random(30)),
                    'statusCode' => 401,
                    'message'    => "You don't have permission to access.",
                   ];
                   return response()->json($response);
                }
            }
        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
            return response()->json(['token_expired'], $e->getStatusCode());
        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
            return response()->json(['token_invalid'], $e->getStatusCode());
        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {
            return response()->json(['token_absent'], $e->getStatusCode());
        }
    
    }

    public function create(Request $request){
        try{
        $current =  date('ym');
        $cardexpire = trim($request->year).trim($request->month);
        $rules = [];
        $rules['name'] = 'required|max:35|regex:/^[a-zA-Z0-9_\s]+$/';
        $rules['number'] = 'required|regex:/^[0-9]+$/|max:19|min:13';
        $rules['month'] = 'required|regex:/^[0-9]+$/|max:2|min:2|date_format:m';
        $rules['year'] = 'required|regex:/^[0-9]+$/|max:2|min:2|date_format:y';
        $rules['cvv'] = 'required|regex:/^[0-9]+$/|max:3|min:3';

        $cardtype = array(
            "visa"       => "/^4[0-9]{12}(?:[0-9]{3})?$/",
            "mastercard" => "/^5[1-5][0-9]{14}$/",
            "amex"       => "/^3[47][0-9]{13}$/",
            "discover"   => "/^6(?:011|5[0-9]{2})[0-9]{12}$/",
            "jcb"        => "/^(?:2131|1800|35\d{3})\d{11}$/",
            "dinersclub" => "/^3(?:0[0-5]|[68][0-9])[0-9]{11}$/",
        );

            if(!$user = JWTAuth::parseToken()->authenticate()) {
                $response = [
                'requestId' => strtolower(Str::random(30)),
                'statusCode' => 404,
                'message'    => "user_not_found",
                'status' => 0,
               ];
               return response()->json($response);
            } else {
                $validator = Validator::make($request->all(), $rules);
                if($validator->fails() || $current >= $cardexpire || (preg_match($cardtype['amex'],$request->number) && $user->allow_card_american_express=='0')) {
                    $response = [
                    'requestId' => strtolower(Str::random(30)),
                    'message'    => 'Validation Error.',
                    'error'    => $validator->errors(),
                    'statusCode' => 422,
                    'status' => 0,
                   ];

                   if($current >= $cardexpire){
                    $response = [
                        'requestId' => strtolower(Str::random(30)),
                        'message' => 'Enter valid month or year',
                        'statusCode' => 422,
                        'status' => 0,
                       ];
                   }

                   if(preg_match($cardtype['amex'],$request->number) && $user->allow_card_american_express=='0'){
                    $response = [
                        'requestId' => strtolower(Str::random(30)),
                        'message' => 'Not Authorised with American Express Card',
                        'statusCode' => 422,
                        'status' => 0,
                       ];
                   }

                   return response()->json($response);
                 } else {

                    $api_statistics = Userapistatistics::where('user_id', $user->id)->where('resource_path', $request->path())->first();
                        if (is_null($api_statistics)) {  
                           $insId = Userapistatistics::insertGetId([
                            'user_id' => $user->id,
                            'resource_path' => $request->path(),
                            'count_requests' => 1,
                            'created' => Carbon::now()
                          ]);
                        } else {
                            Userapistatistics::where('id', $api_statistics->id)->update([
                              'count_requests' => $api_statistics->count_requests + 1,
                              'updated' => Carbon::now()
                             ]);
                        } 

                    $name = trim($request->name);
                    $number = trim($request->number);
                    $month = trim($request->month);
                    $year = trim($request->year);
                    $cvv = trim($request->cvv);
                    $default = trim($request->default);

                    $blacklistcard = BlackListCards::where('number', '=', $number)->first();
                    if($blacklistcard){
                        $response = [
                        'requestId' => strtolower(Str::random(30)),
                        'message' => 'Card is Blacklisted',
                        'statusCode'    => 403,
                        'status' => 0,
                        ];
                        return response()->json($response);
                    } else {
                        $usedcard = UserCreditCards::where('number', '=', $number)->first();
                       if($usedcard){
                            $response = [
                            'requestId' => strtolower(Str::random(30)),
                            'message' => 'Card number already is used',
                            'statusCode'    => 403,
                            'status' => 0,
                            ];
                            return response()->json($response);
                        } else {
                            $created = date('Y-m-d h:i:s');
                            $newcard = UserCreditCards::insertGetId(['user_id' => $user->id, 'name' => $name, 'number' => $number , 'exp_month' => $month, 'exp_year' => $year , 'cvv' => $cvv, 'created' => $created, 'updated' => $created, 'is_verified' => '0', 'is_deleted' => '0']);

                            if($newcard){
                                if($default=='1' || $default=='true'){
                                    $updateusercards = UserCreditCards::where('user_id', '=', $user->id)
                                     ->where('id', '!=', $newcard)
                                     ->update(['is_default' => '0']);

                                     $updateusercards = UserCreditCards::where('id', $newcard)
                                     ->update(['is_default' => '1']);
                                   }
                                $response = [
                                'requestId' => strtolower(Str::random(30)),
                                'message' => 'Created',
                                'statusCode'    => 200,
                                'status' => 1,
                                ];
                                return response()->json($response);
                            } else {
                                $response = [
                                'requestId' => strtolower(Str::random(30)),
                                'message' => 'Error',
                                'statusCode'    => 204,
                                'status' => 0,
                                ];
                                return response()->json($response);
                            }
                        }
                    }
                }
            }
        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
            return response()->json(['token_expired'], $e->getStatusCode());
        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
            return response()->json(['token_invalid'], $e->getStatusCode());
        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {
            return response()->json(['token_absent'], $e->getStatusCode());
        }
        
    }

    public function card_verification(Request $request){
        try{
        $cardId = trim($request->cardId);
            if(!$user = JWTAuth::parseToken()->authenticate()) {
                    $response = [
                    'requestId' => strtolower(Str::random(30)),
                    'statusCode' => 404,
                    'message'    => "user_not_found",
                    'status' => 0,
                   ];
                   return response()->json($response);
            } else {

                $api_statistics = Userapistatistics::where('user_id', $user->id)->where('resource_path', $request->path())->first();
                    if (is_null($api_statistics)) {  
                       $insId = Userapistatistics::insertGetId([
                        'user_id' => $user->id,
                        'resource_path' => $request->path(),
                        'count_requests' => 1,
                        'created' => Carbon::now()
                      ]);
                    } else {
                        Userapistatistics::where('id', $api_statistics->id)->update([
                          'count_requests' => $api_statistics->count_requests + 1,
                          'updated' => Carbon::now()
                         ]);
                    } 

                if($cardId){
                $cardDetail = UserCreditCards::where('id', '=', $cardId)->first();

                $userDetail = Profile::where('user_id', '=', $user->id)->first();
                if($userDetail){
                    $countryDetail = Country::where('idCountry', '=', $userDetail->country_id)->first();
                    $countrycode = $countryDetail->countryCode;  
                } else {
                   $countrycode = 'US'; 
                }
                    if($user->id==$cardDetail->user_id && $userDetail){
                        if($user->test_mode=='1'){
                            $PayflowUrl = 'https://pilot-payflowpro.paypal.com/';
                        } else {
                            $PayflowUrl = 'https://payflowpro.paypal.com';
                        }
                        $payment_log = array();
                        $transactions = array();
                    for($i=1; $i<=2; $i++){
                        $AMT = '';
                        for($j=0; $j<3; $j++){$AMT .=rand(0,4);}
                        $AMT = $AMT/100;
                        $sandbox = TRUE;
                        $request_params = array(
                        'PARTNER' => 'PayPal', 
                        'USER' => env('PAYFLOW_USER'), 
                        'PWD' => env('PAYFLOW_PWD'), 
                        'VENDOR' => env('PAYFLOW_VENDOR'), 
                        'TENDER' => 'C', 
                        'TRXTYPE' => 'S',                   
                        'IPADDRESS' => '192.168.1.185',
                        'VERBOSITY' => 'MEDIUM',
                        'ACCT' => $cardDetail->number,                        
                        'EXPDATE' => $cardDetail->exp_month.$cardDetail->exp_year,           
                        'CVV2' => $cardDetail->cvv, 
                        'FIRSTNAME' => $userDetail->first_name, 
                        'LASTNAME' => $userDetail->last_name, 
                        'STREET' => $userDetail->address.' '.$userDetail->address_2, 
                        'CITY' => $userDetail->city, 
                        'STATE' => $userDetail->state,                     
                        'COUNTRYCODE' => $countrycode, 
                        'ZIP' => $userDetail->zip, 
                        'AMT' => $AMT, 
                        );
                        $nvp_string = '';
                        foreach($request_params as $var=>$val){
                            $nvp_string .= '&'.$var.'='.urlencode($val);    
                        }
                        $nvp_string = substr($nvp_string, 1);
                        $curl = curl_init();
                        curl_setopt($curl, CURLOPT_VERBOSE, 1);
                        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
                        curl_setopt($curl, CURLOPT_TIMEOUT, 30);
                        curl_setopt($curl, CURLOPT_URL, $PayflowUrl);
                        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
                        curl_setopt($curl, CURLOPT_POSTFIELDS, $nvp_string);
                        $headers = array();
                        $headers[] = "Content-Type: application/x-www-form-urlencoded";
                        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
                        $NVPString = curl_exec($curl);   
                        curl_close($curl);
                        $transaction = array();
                        while(strlen($NVPString)){
                            $keypos= strpos($NVPString,'=');
                            $keyval = substr($NVPString,0,$keypos);
                            $valuepos = strpos($NVPString,'&') ? strpos($NVPString,'&'): strlen($NVPString);
                            $valval = substr($NVPString,$keypos+1,$valuepos-$keypos-1);
                            $transaction[$keyval] = urldecode($valval);
                            $NVPString = substr($NVPString,$valuepos+1,strlen($NVPString));
                        }

                        $payment_log['card'] = array('card_name'=>$cardDetail->name,'card_number'=>$cardDetail->number,'card_month'=>$cardDetail->exp_month,'card_year'=>$cardDetail->exp_year);

                        $payment_log['billing'] = array('id'=>$userDetail->id,'user_id'=>$userDetail->user_id,'last_name'=>$userDetail->last_name,'middle_name'=>$userDetail->middle_name,'first_name'=>$userDetail->first_name,'reg_last_name'=>$userDetail->reg_last_name,'reg_first_name'=>$userDetail->reg_first_name,'phone'=>$userDetail->phone,'created'=>$userDetail->created,'updated'=>$userDetail->updated,'address'=>$userDetail->address,'address_2'=>$userDetail->address_2,'city'=>$userDetail->city,'state'=>$userDetail->state,'country_id'=>$userDetail->country_id,'zip'=>$userDetail->zip,'skype'=>$userDetail->skype,'driver_licence'=>$userDetail->driver_licence,'is_international'=>$userDetail->is_international,'phone_number_info'=>$userDetail->phone_number_info,'phone_number_info_request_id'=>$userDetail->phone_number_info_request_id,'province'=>$userDetail->province,'phone_number_info_error'=>$userDetail->phone_number_info_error);

                        $payment_log['response'] = $transaction;
                        $PaymentLog = serialize($payment_log); 
                        $created = date('Y-m-d h:i:s');

                        $newtransaction = PaymentLogs::insertGetId(['user_id' => $user->id, 'service' => 'paypal', 'txn_id' => $transaction['PNREF'] , 'status' => '1', 'data' => serialize($payment_log), 'created' => $created]);

                        if($newtransaction){ 
                            $transaction['AMT'] = $AMT;
                            array_push($transactions,$transaction); 
                        } else {
                            $transaction['AMT'] = '';
                        }
                    }


                       if(count($transactions) > 0 && $transactions[0]['RESULT']=='0' && $transactions[1]['RESULT']=='0'){
                            $verifiedcard = CardVerifications::insertGetId(['card_id' => $cardId, 'transaction_id_1' => $transactions[0]['PNREF'], 'transaction_id_2' => $transactions[1]['PNREF'] , 'date' => $created, 'amount_1' => $transactions[0]['AMT'], 'amount_2' => $transactions[1]['AMT']]);
                            if($verifiedcard){
                                $response = [
                                'requestId' => strtolower(Str::random(30)),
                                'statusCode' => 200,
                                'message'    => "Verified",
                                'status' => 1,
                               ];
                               return response()->json($response);
                            } else {
                                $response = [
                                'requestId' => strtolower(Str::random(30)),
                                'statusCode' => 403,
                                'message'    => "Failed",
                                'status' => 0,
                               ];
                               return response()->json($response);
                            }
                        } else {
                            $response = [
                            'requestId' => strtolower(Str::random(30)),
                            'statusCode' => 403,
                            'message'    => "Failed",
                            'status' => 0,
                           ];
                           return response()->json($response);
                        }
                    } else {
                        $response = [
                        'requestId' => strtolower(Str::random(30)),
                        'statusCode' => 401,
                        'message'    => "You don't have permission to access.",
                        'status' => 0,
                       ];
                       return response()->json($response);
                    }
                } else {
                    $response = [
                    'requestId' => strtolower(Str::random(30)),
                    'statusCode' => 400,
                    'message'    => 'Required parameter not passed: cardId',
                    'status' => 0,
                    ];
                    return response()->json($response);
                }
            }
        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
            return response()->json(['token_expired'], $e->getStatusCode());
        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
            return response()->json(['token_invalid'], $e->getStatusCode());
        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {
            return response()->json(['token_absent'], $e->getStatusCode());
        }
        
    }

    public function charge_verification(Request $request){
        try{
        $rules = [];
        $rules['amount1'] = 'required';
        $rules['amount2'] = 'required';
        //$rules['cardid'] = 'required';
            if(!$user = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['user_not_found'], 404);
            } else {
                $validator = Validator::make($request->all(), $rules);
                if($validator->fails()) {
                    $response = [
                    'requestId' => strtolower(Str::random(30)),
                    'message'    => 'Validation Error.',
                    'error'    => $validator->errors(),
                    'statusCode' => 422,
                    'status' => 0,
                   ];
                   return response()->json($response);
                 } else {

                    $api_statistics = Userapistatistics::where('user_id', $user->id)->where('resource_path', $request->path())->first();
                        if (is_null($api_statistics)) {  
                           $insId = Userapistatistics::insertGetId([
                            'user_id' => $user->id,
                            'resource_path' => $request->path(),
                            'count_requests' => 1,
                            'created' => Carbon::now()
                          ]);
                        } else {
                            Userapistatistics::where('id', $api_statistics->id)->update([
                              'count_requests' => $api_statistics->count_requests + 1,
                              'updated' => Carbon::now()
                             ]);
                        } 

                    $cardId = trim($request->cardId);
                    $amount1 = trim($request->amount1);
                    $amount2 = trim($request->amount2);
                    $verifyDetail = CardVerifications::where('card_id', $cardId)->first();
                    if(($amount1==$verifyDetail->amount_1 && $amount2==$verifyDetail->amount_2) || ($amount1==$verifyDetail->amount_2 && $amount2==$verifyDetail->amount_1)){
                        $paymentlogs = PaymentLogs::where('txn_id', $verifyDetail->transaction_id_1)->orWhere('txn_id', $verifyDetail->transaction_id_2)->get(); 
                        if(count($paymentlogs) > 0){
                            foreach($paymentlogs as $logs){
                                $created = date('Y-m-d h:i:s');
                                $logdata = unserialize($logs->data);
                                if($logdata['response']['PNREF']==$verifyDetail->transaction_id_1){
                                    $payamount = $verifyDetail->amount_1;
                                    $paydesc = 'Verification (1)';
                                } else {
                                    $payamount = $verifyDetail->amount_2;
                                    $paydesc = 'Verification (2)';
                                }
                                
                                $newpayment = Payments::insertGetId(['to_user_id' => $user->id, 'amount' => $payamount, 'description' => $paydesc , 'created' => $created, 'type' => 'card', 'commission' => '0.00', 'txn_id' => $logdata['response']['PNREF'], 'txn_ppref' => $logdata['response']['PPREF'], 'discount' => '0.00', 'old_balance' => '0.00', 'visible_for_client' => '1']);
                            }
                            $updated = date('Y-m-d h:i:s');
                            $updateuserbalance = UserCreditCards::where('id', '=', $cardId)
                            ->update(['is_verified' => '1', 'updated' => $updated]);

                            $newbalance = $verifyDetail->amount_1 + $verifyDetail->amount_2 + $user->balance;
                            $updateuserbalance = User::where('id', '=', $user->id)
                             ->update(['balance' => $newbalance]);

                            $response = [
                            'requestId' => strtolower(Str::random(30)),
                            'statusCode' => 200,
                            'message'    => "Verified",
                            'status' => 1,
                           ];
                           return response()->json($response);
                        
                        } else {
                            $response = [
                            'requestId' => strtolower(Str::random(30)),
                            'statusCode' => 403,
                            'message'    => "Failed",
                            'status' => 0,
                           ];
                           return response()->json($response);
                        }
                    } else {
                        $attempts = $verifyDetail->attempt+1;
                        if($verifyDetail->attempt_data!=''){
                            $attemptsdata = unserialize($verifyDetail->attempt_data);
                            $newattempt = array('amount_1' => $amount1, 'amount_2' => $amount2);
                            array_push($attemptsdata,$newattempt);
                          } else {
                            $attemptsdata = array();
                            $newattempt = array('amount_1' => $amount1, 'amount_2' => $amount2);
                            array_push($attemptsdata,$newattempt);
                         }
                        $updatecardsverify = CardVerifications::where('card_id', '=', $cardId)
                         ->update(['attempt' => $attempts, 'attempt_data' => serialize($attemptsdata)]);
                        $response = [
                        'requestId' => strtolower(Str::random(30)),
                        'statusCode' => 403,
                        'message'    => "Failed",
                        'status' => 0,
                       ];
                       return response()->json($response);
                    }
                }
            }
        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
            return response()->json(['token_expired'], $e->getStatusCode());
        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
            return response()->json(['token_invalid'], $e->getStatusCode());
        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {
            return response()->json(['token_absent'], $e->getStatusCode());
        }
        
    }

    public function IsCardVerified(Request $request){
        try{
        $cardId = trim($request->cardId);
        if(!$user = JWTAuth::parseToken()->authenticate()) {
            return response()->json(['user_not_found'], 404);
        } else {
            $verifyDetail = CardVerifications::where('card_id', $cardId)->first();
            if($verifyDetail){
                $response = [
                'requestId' => strtolower(Str::random(30)),
                'statusCode' => 200,
                'message'    => "Verified",
                'status' => 1,
               ];
               return response()->json($response);
            } else {
                $response = [
                'requestId' => strtolower(Str::random(30)),
                'statusCode' => 200,
                'message'    => "Not verified",
                'status' => 0,
               ];
               return response()->json($response);
            }
        }
        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
            return response()->json(['token_expired'], $e->getStatusCode());
        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
            return response()->json(['token_invalid'], $e->getStatusCode());
        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {
            return response()->json(['token_absent'], $e->getStatusCode());
        }
    }

    public function getdetails(Request $request){
        $cardId = trim($request->cardId);
        if(!$user = JWTAuth::parseToken()->authenticate()) {
            return response()->json(['user_not_found'], 404);
        } else {
            $cardDetails = UserCreditCards::where('id', '=', $cardId)->first();
            $card_item = $cardDetails->toArray();
            $cardtype = array(
                "visa"       => "/^4[0-9]{12}(?:[0-9]{3})?$/",
                "mastercard" => "/^5[1-5][0-9]{14}$/",
                "amex"       => "/^3[47][0-9]{13}$/",
                "discover"   => "/^6(?:011|5[0-9]{2})[0-9]{12}$/",
                "jcb"   => "/^(?:2131|1800|35\d{3})\d{11}$/",
                "dinersclub"   => "/^3(?:0[0-5]|[68][0-9])[0-9]{11}$/",
            );
             

            if($card_item){
                
                if (preg_match($cardtype['visa'],$card_item['number'])){ $type= "Visa"; }
                else if (preg_match($cardtype['mastercard'],$card_item['number']))
                { $type= "Mastercard"; }
                else if (preg_match($cardtype['amex'],$card_item['number']))
                { $type= "American Express"; }
                else if (preg_match($cardtype['discover'],$card_item['number']))
                { $type= "Discover"; }
                else if (preg_match($cardtype['jcb'],$card_item['number']))
                { $type= "JCB"; }
                else if (preg_match($cardtype['dinersclub'],$card_item['number']))
                { $type= "Diners Club"; }
                else
                { $type = "Unknown"; }

                $cardinfo = array(
                    'type' => $type,
                    'ending' => substr($card_item['number'], -4)
                ); 

                $carddata = array_merge($cardinfo, $card_item);

                $response = [
                'requestId' => strtolower(Str::random(30)),
                'statusCode' => 200,
                'carddetails'    => $carddata,
                'status' => 1,
               ];
               return response()->json($response);
            } else {
                $response = [
                'requestId' => strtolower(Str::random(30)),
                'statusCode' => 200,
                'carddetails'    => "",
                'status' => 0,
               ];
               return response()->json($response);
            }
        }
    }

    public function deletecard(Request $request){
        $cardId = trim($request->cardId);
        if(!$user = JWTAuth::parseToken()->authenticate()) {
            return response()->json(['user_not_found'], 404);
        } else {
            $updated = date('Y-m-d h:i:s');
            $carddelete = UserCreditCards::where('id', '=', $cardId)
            ->update(['is_deleted' => '1', 'updated' => $updated]);
            if($carddelete){
                $response = [
                'requestId' => strtolower(Str::random(30)),
                'statusCode' => 200,
                'message'    => "Deleted",
                'status' => 1,
               ];
               return response()->json($response);
            } else {
                $response = [
                'requestId' => strtolower(Str::random(30)),
                'statusCode' => 200,
                'message'    => "Not deleted",
                'status' => 0,
               ];
               return response()->json($response);
            }
        }
    }


    public function defaultcard(Request $request){
        $cardId = trim($request->cardId);
        if(!$user = JWTAuth::parseToken()->authenticate()) {
            return response()->json(['user_not_found'], 404);
        } else {
            if($cardId){
            $updateusercards = UserCreditCards::where('user_id', '=', $user->id)
             ->where('id', '!=', $cardId)
             ->update(['is_default' => '0']);

             $updateusercards = UserCreditCards::where('id', $cardId)
             ->update(['is_default' => '1']);

                $response = [
                'requestId' => strtolower(Str::random(30)),
                'message' => 'Card is set default successfully',
                'statusCode'    => 201,
                'status' => 1,
                ];
                return response()->json($response);
           } else {
                $response = [
                'requestId' => strtolower(Str::random(30)),
                'statusCode' => 403,
                'message'    => "Error",
                'status' => 0,
               ];
               return response()->json($response);
            }
        }
    }

    public function getverifycard(Request $request){
        try{
            if(!$user = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['user_not_found'], 404);
                } else {
                if($user){

                   $api_statistics = Userapistatistics::where('user_id', $user->id)->where('resource_path', $request->path())->first();
                        if (is_null($api_statistics)) {  
                           $insId = Userapistatistics::insertGetId([
                            'user_id' => $user->id,
                            'resource_path' => $request->path(),
                            'count_requests' => 1,
                            'created' => Carbon::now()
                          ]);
                        } else {
                            Userapistatistics::where('id', $api_statistics->id)->update([
                              'count_requests' => $api_statistics->count_requests + 1,
                              'updated' => Carbon::now()
                             ]);
                        } 
                         
                   $user_cards = UserCreditCards::where('user_id', '=', $user->id)->where(
                   function($query){
                        $query->whereNull('is_deleted');
                        $query->orWhere('is_deleted', '=', '0');
                        $query->where('is_verified', '=', '1');
                    })->get();
                   $record_cards = $user_cards->toArray();
                   $records = array();
                   if(count($record_cards) > 0){
                    $cardtype = array(
                        "visa"       => "/^4[0-9]{12}(?:[0-9]{3})?$/",
                        "mastercard" => "/^5[1-5][0-9]{14}$/",
                        "amex"       => "/^3[47][0-9]{13}$/",
                        "discover"   => "/^6(?:011|5[0-9]{2})[0-9]{12}$/",
                        "jcb"   => "/^(?:2131|1800|35\d{3})\d{11}$/",
                        "dinersclub"   => "/^3(?:0[0-5]|[68][0-9])[0-9]{11}$/",
                    );
                    
                        foreach($record_cards as $carditem){
                            $card_item = (object)$carditem;
                            if($card_item->is_verified=='1'){$verified='true';} else {$verified='false';}
                        if (preg_match($cardtype['visa'],$card_item->number)){ $type= "Visa"; }
                        else if (preg_match($cardtype['mastercard'],$card_item->number))
                        { $type= "Mastercard"; }
                        else if (preg_match($cardtype['amex'],$card_item->number))
                        { $type= "American Express"; }
                        else if (preg_match($cardtype['discover'],$card_item->number))
                        { $type= "Discover"; }
                        else if (preg_match($cardtype['jcb'],$card_item->number))
                        { $type= "JCB"; }
                        else if (preg_match($cardtype['dinersclub'],$card_item->number))
                        { $type= "Diners Club"; }
                        else
                        { $type = "Unknown"; }

                        $data = array(
                            'id' => $card_item->id,
                            'name' => $card_item->name,
                            'type' => $type,
                            'ending' => substr($card_item->number, -4),
                            'expires' => $card_item->exp_month.'/20'.$card_item->exp_year,
                            'verified' => $verified,
                            'created' => $card_item->created,
                        );

                            $records[] = $data;
                        }
                   }
                   if (count($records) > 0) {
                        $response = [
                        'requestId' => strtolower(Str::random(30)),
                        'totalRecords' => count($record_cards),
                        'records'    => $records,
                        'statusCode' => 200,
                       ];
                       return response()->json($response);
                      } else {
                        $response = [
                        'requestId' => strtolower(Str::random(30)),
                        'totalRecords' => count($record_cards),
                        'message'    => 'Record does not exist.',
                        'statusCode' => 204,
                       ];
                       return response()->json($response);
                    }
                } else {
                    $response = [
                    'requestId' => strtolower(Str::random(30)),
                    'statusCode' => 401,
                    'message'    => "You don't have permission to access.",
                   ];
                   return response()->json($response);
                }
            }
        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
            return response()->json(['token_expired'], $e->getStatusCode());
        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
            return response()->json(['token_invalid'], $e->getStatusCode());
        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {
            return response()->json(['token_absent'], $e->getStatusCode());
        }
    
    }
}
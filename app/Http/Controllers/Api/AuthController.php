<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\User;
use App\Http\Resources\UserResource;
use App\Models\Profile;
use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\Hash;
use Carbon\Carbon; 
use JWTFactory;
use JWTAuth;
use Validator;
use Config;
use Log;
use Event;
use App\Events\UserRegistered;

use DB;
use Mail;
use App\Models\Country;
use App\Models\State;
use App\Models\Couponcode;
use App\Models\Setting;
use App\Models\Ipinfo;
use App\Models\Roles;
use App\Models\Referrals;
use App\Models\Apirequests;
use App\Models\Userapi;
use App\Models\Userapistatistics;
use Illuminate\Support\Str;
use Tymon\JWTAuth\Exceptions\JWTException;


class AuthController extends Controller
{   
  public function __construct()
  {
    $this->middleware('auth:api', ['except' => ['login', 'register']]);
    $this->nexmo_key  = Config::get('nexmo.api_key');
    $this->nexmo_secret = Config::get('nexmo.api_secret');
  }

  public function login(Request $request)
  {
    try {
      $credentials = $request->only('email', 'password');

      if ($token = $this->guard()->attempt($credentials)) {

        $user = $this->guard()->user(); 
        $auth_user = new UserResource($user);
        $errorMsg = '';
        /*******26 Dec start******************************/
         /* if($user->active=='0') {
             event(new UserRegistered($user));
          }*/
         /*******26 Dec end******************************/
        return response(compact('token', 'auth_user', 'errorMsg'), 200);

      } else{
       // return response()->json(['error' => 'invalid_credentials'], 401);
        /*******26 Dec start******************************/
              $errorMsg = 'Invalid credentials';
              return response(compact('errorMsg'));
        /*******26 Dec end******************************/      
      }
    }catch (\Exception $e) {
        return $e->getMessage();
    }
  }

    public function register(Request $request)
  {
    try {
      $userType = 'business';
      $userActive = 0;
      $userVerified = 0;
      $data =  array();

        $rules = [];

        $rules['firstName'] = 'required|max:35|regex:/^[a-zA-Z0-9_\s]+$/';

        $rules['lastName'] = 'required|max:35|regex:/^[a-zA-Z0-9_\s]+$/';

        $rules['email'] = 'required|string|email|max:191|unique:users';

        $rules['phone'] = 'required|regex:/^[0-9]+$/|min:10|max:12';

        $rules['password'] = 'required|string|min:8|confirmed';

        $rules['countryId'] = 'required|regex:/^[0-9]+$/|max:3';

        $rules['streetAddress1'] = 'required|string';

        $rules['city'] = 'required|string';

        $rules['postalCode'] = 'required|max:6|regex:/^[0-9-]+$/';

        if( trim($request->countryId) == 233 ) {
            $rules['state'] = 'required|regex:/^[a-zA-Z]+$/|max:2|min:2';
            $is_international = false;
            $state = trim($request->state);
            $province = '';
            $skype = '';
        } else {
            $rules['province'] = 'required|regex:/^[a-zA-Z]+$/';
            $is_international = true;
            $state = '';
            $province = trim($request->province);
            $skype = trim($request->skype);
        }

    $validator = Validator::make($request->all(), $rules);

    if($validator->fails()){
      return response()->json(['error' => $validator->errors()], 409);
    } 
    
      if(trim($request->countryId)) {
             $countryId = trim($request->countryId);
             $countryItem = Country::where('idCountry', '=', $countryId)->where('active', '=', 1)->first();
            if(!$countryItem){
                $response = [
                    'requestId' => strtolower(Str::random(30)),
                    'statusCode' => 400,
                    'error'    => "Invalid parameter: countryId",
                   ];
                return response()->json($response);
            }
        }

        if(trim($request->state)) {
              $stateName = trim($request->state);
              $stateItem = State::where('abbr', '=', $stateName)->where('military', '=', 0)->first();
              if(!$stateItem) {
                $response = [
                    'requestId' => strtolower(Str::random(30)),
                    'statusCode' => 400,
                    'error'    => "Invalid parameter: state",
                   ];
                return response()->json($response);
              }
            }

        if(trim($request->referralCode)) {
                $referralCode = trim($request->referralCode);
                $referralUser = User::where('salesman_code', '=', $referralCode)->where('active', '=', 1)->where('verified', '=', 1)->first();
                if(!$referralUser) {
                  $response = [
                    'requestId' => strtolower(Str::random(30)),
                    'statusCode' => 400,
                    'error'    => "Invalid parameter: referralCode",
                   ];
                  return response()->json($response);
                } else {
                  if( $referralUser->referral_autoverification == 1 ) {
                     $userVerified = 1;
                  }

                  $data['referrer_url'] = $referralUser->referrer_url;
                }
            }

        if(trim($request->promoCode)) {
                $promoCode = trim($request->promoCode);
                $applyDate = date('Y-m-d');
                $couponCodes = Couponcode::where('code', '=', $promoCode)->where('published', '=', 1)->where('start_at', '<=', $applyDate)->where('end_at', '>=', $applyDate)->first();
                if(!$couponCodes) {
                  $response = [
                    'requestId' => strtolower(Str::random(30)),
                    'statusCode' => 400,
                    'error'    => "Invalid parameter: promoCode",
                   ];
                  return response()->json($response);
                } else {
                  $data['coupon_code_id'] = $couponCodes->id; 
                }
            }  
       
        $settings = Setting::first(); 
        if($settings) {
           $defaultProvider = $settings->default_provider; 
        } else {
           $defaultProvider = 'stamps_default';  
        }

        $ip = $_SERVER['SERVER_ADDR'];
        if (PHP_OS == 'WINNT'){
            $ip = getHostByName(getHostName());
        }
        if (PHP_OS == 'Linux'){
            $ip = $_SERVER['SERVER_ADDR'];
            /*$command="/sbin/ifconfig";
            exec($command, $output);
            $pattern = '/inet addr:?([^ ]+)/';
            $ip = array();
            foreach ($output as $key => $subject) {
                $result = preg_match_all($pattern, $subject, $subpattern);
                if ($result == 1) {
                    if ($subpattern[1][0] != "127.0.0.1")
                    $ip = $subpattern[1][0];
                }
            }*/
        }

        $ipinfo = Ipinfo::where('ip', $ip)->first();
        if (is_null($ipinfo)) { 
             $ipId = Ipinfo::insertGetId([
              'ip' => $ip,
              'updated' => Carbon::now()
            ]);
        } else {
            $ipId = $ipinfo->id;
        } 
       
    //get phone number and check for standard. USA only eligible
    /*$number = $request->get('phone');
    if (substr($number, 0, 1) != '1')
      {
        $number = '1'.$number;
      }*/

    $number = trim($request->phone);  

    $data['username'] = substr(str_replace(array('@', '.'), '', trim($request->email)) . '_' . str_random(), 0, 20);

    $data['email'] = trim($request->email);

    $data['password'] = Hash::make(trim($request->password));  

    $data['active'] = $userActive;

    $data['active_code'] = '';

    $data['new_pass_code'] = '';

    $data['view_message'] = Carbon::now();

    $data['type'] = $userType;

    $data['hear_about_us'] = trim($request->hearAboutUs);

    $data['insurance_commission'] = '0.00';

    $data['domestic_commission_additional'] = '';

    $data['disable_check_print_label'] = '0';

    $data['verified'] = $userVerified;

    $data['provider_account'] = $defaultProvider; 

    $data['balance'] = '0.00';

    $data['cnt_labels'] = '0';

    $data['inheritance_ignore_fields'] = '';

    $data['register_ipinfo_id'] = $ipId;

    $user = User::create($data);
    
    if($user) {
      $user_id = $user->id;
      Mail::send('emails.signup', ['data' => $data], function($message) use ($data)
            {
                $message->from('dev2.bdpl@gmail.com', "Shipping Center");
                $message->subject("Welcome to Shipping Center");
                $message->to($data['email']);
            });

      $profile = Profile::insert([  
        'user_id'            => $user->id,
        'last_name'          => trim($request->lastName),
        'middle_name'        => trim($request->middleName),
        'first_name'         => trim($request->firstName),
        'reg_last_name'      => trim($request->lastName),
        'reg_first_name'     => trim($request->firstName),
        'phone'              => $number,
        'created'            => Carbon::now(),
        'updated'            => Carbon::now(),
        'address'            => trim($request->streetAddress1),  
        'address_2'          => trim($request->streetAddress2),
        'city'               => trim($request->city),
        'state'              => $state, 
        'country_id'         => trim($request->countryId), 
        'zip'                => trim($request->postalCode), 
        'skype'              => $skype,
        'is_international'   => $is_international,
        'province'           => $province,
        'driver_licence'     => '',    
    ]);

    $user_role = Roles::insert([
      'user_id' => $user->id,
      'role_id' => 1
     ]);

      if(trim($request->referralCode)) {
            $referralCode = trim($request->referralCode);
            $referralUser = User::where('salesman_code', '=', $referralCode)->where('active', '=', 1)->where('verified', '=', 1)->first();
        if($referralUser) {
            $user_referrals = Referrals::insert([
                'referral_user_id' => $referralUser->id,
                'referrer_user_id' => $user->id,
                'code' => $referralCode,
                'created' => Carbon::now(),
                'inheriting_settings' => 1
            ]);
        }
      }

      $apirequestExists = Apirequests::where('user_id', '=', $user->id)->first();
      if(!$apirequestExists) {
        $text = 'Shipping Center Customer';
        $siteAddress = 'https://somedomain.com';
        $planTime = date('Y-m-d');
        $parcelAmount = '100';
       
        $Apirequest = Apirequests::insert([
          'user_id' => $user->id,
          'active' => 0,
          'created' => Carbon::now(),
          'text' => $text,
          'site_address' => $siteAddress,
          'plan_time' => $planTime,
          'parcels_amount' => $parcelAmount
        ]);

        $local_token = Str::random(64);
        $userApi = Userapi::insert([
          'local_token' => $local_token,
          'user_id' => $user->id,
          'active' => 0,
          'created' => Carbon::now(),
          'updated' => Carbon::now(),
          'last_request' => Carbon::now()
        ]);
      }            
  }

    $token = JWTAuth::fromUser($user);
 //------- fire event UserRegistred for nexmo sms ------
      event(new UserRegistered($user));
      $auth_user = $user;
      return response()->json(compact('auth_user','token'),201);
    } catch (\Exception $e) {
        return $e->getMessage();
    }
  }

  public function smsVerify(Request $request)
  { 
    try {

      if (! $auth_user = $this->guard()->user()) {
            return response()->json(['user_not_found'], 404);
        }

        $auth_user = $this->guard()->user();
        $user = new UserResource($auth_user);
        $nexmo_request = $user->phone_verification_request_id;
        // check for errors
         if (!$nexmo_request){
          $number = $user->profile->phone;
          if($user->profile->phone_number_info_error){
            return response()->json(['error' => $user->profile->phone_number_info_error], 409);
          }
            if(!$number){
              return response()->json(['error' => 'No phone number stored'], 404); 
            }else{
              return response()->json(['error' => 'No sms request found'], 404);  
            }
        }
        // get nexmo code from verification form
        $nexmo_code = $request->get('code'); // make front end validation
        // create a nexmo client
        $client = new \Nexmo\Client(new \Nexmo\Client\Credentials\Basic($this->nexmo_key, $this->nexmo_secret)); 
        // check verification
        $verification = new \Nexmo\Verify\Verification($nexmo_request);
        try {
          $client->verify()->check($verification, $nexmo_code);
          // if ok update user table active to 1
          $user = User::find($user->id);
          $user->active = 1;
          $user->save();
          return response()->json($verification['status'], 200);  // got '0'
          // if error return satus and error;
        } catch (\Exception $e) {
          $verification = $e->getEntity(); 
          return response()->json(['error' => $verification['error_text'], 'code' =>$verification['status']]);
        }

    } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

              return response()->json(['token_expired'], $e->getStatusCode());

      } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

              return response()->json(['token_invalid'], $e->getStatusCode());

      } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {

              return response()->json(['token_absent'], $e->getStatusCode());
      }

  }

  public function resendSms(Request $request)
  { 
    try {

        if (! $user = $this->guard()->user()) {
            return response()->json(['user_not_found'], 404);
        } 
            
          $client = new \Nexmo\Client(new \Nexmo\Client\Credentials\Basic($this->nexmo_key, $this->nexmo_secret));
          // get user id from token and fetch info from db
          $user = $this->guard()->user();
          // get user nexmo request id'
          $nexmo_request = $user->phone_verification_request_id;
          if (!$nexmo_request){
            $number = $user->profile->phone;
            if($user->profile->phone_number_info_error){
              return response()->json(['status' => false, 'error' => $user->profile->phone_number_info_error], 409);
            }
              if(!$number){
                return response()->json(['status' => false, 'error' => 'No phone number stored'], 404); 
              }else{
                return response()->json(['status' => false, 'error' => 'No sms request found'], 404);  
              }
          }
          // send nexmo new request
          try{
            $verification = new \Nexmo\Verify\Verification($nexmo_request);
            $client->verify()->trigger($verification);
             return response()->json(['status' => true, 'success' => 'Request submitted'], 201);
          }catch (\Exception $e) {
            $verification = $e->getEntity();     
            return response()->json(['status' => false, 'error' => $verification['error_text'], 'code' =>$verification['status']]);
          }     

      } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

              return response()->json(['token_expired'], $e->getStatusCode());

      } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

              return response()->json(['token_invalid'], $e->getStatusCode());

      } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {

              return response()->json(['token_absent'], $e->getStatusCode());
      }
  
  }

  public function me(Request $request)
  {
    $user = $this->guard()->user(); 
    return (new UserResource($user))->response()->setStatusCode(202);
  }

  public function logout()
  {
    $this->guard()->logout();

    return response()->json(['message' => 'Successfully logged out']);
  }

  /**
   * Refresh a token.
   *
   * @return \Illuminate\Http\JsonResponse
   */
  public function refresh()
  {
    
    try {

      if (! $user = $this->guard()->user()) {
            return response()->json(['user_not_found'], 404);
        } 

        return $this->respondWithToken($this->guard()->refresh()); 

      } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

              return response()->json(['token_expired'], $e->getStatusCode());

      } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

              return response()->json(['token_invalid'], $e->getStatusCode());

      } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {

              return response()->json(['token_absent'], $e->getStatusCode());
      }
  }

  /**
   * Get the token array structure.
   *
   * @param  string $token
   *
   * @return \Illuminate\Http\JsonResponse
   */
  protected function respondWithToken($token)
  {
    return response()->json([
      'access_token' => $token,
      'token_type' => 'bearer',
      'expires_in' => $this->guard()->factory()->getTTL() * 60
    ]);
  }

  /**
   * Get the guard to be used during authentication.
   *
   * @return \Illuminate\Contracts\Auth\Guard
   */
  public function guard()
  {
    return Auth::guard();
  }
}

<?php

namespace App\Http\Controllers\Api\Location;

use App\User;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Api\APIBaseController as APIBaseController;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Str;
use App\Country;
use Auth;
use DB;
use Exception;
use Log;
use Session;
use Hash;
use Image;

class CountryController extends APIBaseController
{

    public $successStatus = 200;

    public function __construct()
    {
       //
    }

    public function get_getList(Request $request)
    {   
        try {
            $token = trim($request->authToken);
            $uri = $request->path();
            if($token) {
                if($uri == 'api/v1/location/country/getList') {
                    $auth_token = DB::table('user_api')->where('auth_token', $token)->first();
                    if (is_null($auth_token)) { 
                        $response = [
                        'requestId' => strtolower(Str::random(30)),
                        'message'    => 'Token does not exist.',
                        'statusCode' => 401,
                        ];
                    } else {
                        if($auth_token->active == 1) {
                        $user = DB::table('users')->where('id', $auth_token->user_id)->where('in_archive', 0)->first();
                            if ($user) {
                                    $records = DB::table('countries')->select('idCountry as id', 'countryCode as code', 'countryName as title')->orderBy('countryName', 'asc')->get();
                            if ($records->count()) {
                                $response = [
                                'requestId' => strtolower(Str::random(30)),
                                'records'    => $records,
                                'statusCode' => $this->successStatus,
                               ];
                              } else {
                                $response = [
                                'requestId' => strtolower(Str::random(30)),
                                'message'    => 'Record does not exist.',
                                'statusCode' => 401,
                               ];
                            }

                            $api_statistics = DB::table('user_api_statistics')->where('user_id', $auth_token->user_id)->where('resource_path', $request->path())->first();

                            if (is_null($api_statistics)) { 
                               $insId = DB::table('user_api_statistics')->insertGetId(
                                 ['user_id' => $auth_token->user_id, 'resource_path' => $request->path(), 'count_requests' => 1, 'created' => date('Y-m-d H:i:s')]
                                );
                            } else {
                                DB::table('user_api_statistics')->where('id', $api_statistics->id)->update(['count_requests' => $api_statistics->count_requests + 1, 'updated' => date('Y-m-d H:i:s')]);
                            }
                           } else {
                                $response = [
                                'requestId' => strtolower(Str::random(30)),
                                'statusCode' => 403,
                                'message'    => "You don't have permission to access.",
                               ];
                            }   
                        } else {
                            $response = [
                            'requestId' => strtolower(Str::random(30)),
                            'message' => 'Token is locked.',
                            'statusCode'    => 403,
                            ];
                        }
                    }  
                } else {
                        $response = [
                        'requestId' => strtolower(Str::random(30)),
                        'statusCode'    => 405,
                        'error' => 'The requested URL '. $uri .'was not found on this server.',
                        ];
                }
            } else {
                    if($uri == 'api/v1/location/country/getList') {
                            $records = DB::table('countries')->select('idCountry as id', 'countryCode as code', 'countryName as title')->orderBy('countryName', 'asc')->get();
                            if ($records->count()) {
                                $response = [
                                'requestId' => strtolower(Str::random(30)),
                                'records'    => $records,
                                'statusCode' => $this->successStatus,
                               ];
                              } else {
                                $response = [
                                'requestId' => strtolower(Str::random(30)),
                                'message'    => 'Record does not exist.',
                                'statusCode' => 401,
                               ];
                            }
                    } else {
                        $response = [
                        'requestId' => strtolower(Str::random(30)),
                        'statusCode'    => 405,
                        'error' => 'The requested URL '. $uri .'was not found on this server.',
                        ];
                    }
            }
            return response()->json($response);
        } catch (\Exception $e) {
            return $e->getMessage();
        }    
    }

}
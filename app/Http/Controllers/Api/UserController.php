<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\User;
use App\Http\Resources\UserResource;
use App\Http\Resources\ProfileResource;
use App\Models\Profile;
use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\Hash;
use Carbon\Carbon; 
use JWTFactory;
use JWTAuth;
use Validator;
use Config;
use Log;
use Event;
use App\Events\UserRegistered;

use DB;
use App\Models\Country;
use App\Models\State;
use App\Models\Apirequests;
use App\Models\Userapi;
use App\Models\Userapistatistics;
use Illuminate\Support\Str;
use Tymon\JWTAuth\Exceptions\JWTException;

use App\Models\UserDetail;
use URL;

class UserController extends Controller
{   

    public function getAuthenticatedUser(Request $request)
    {
                try {

	                if (! $user = JWTAuth::parseToken()->authenticate()) {
	                        return response()->json(['user_not_found'], 404);
	                } else {
                    $api_statistics = Userapistatistics::where('user_id', $user->id)->where('resource_path', $request->path())->first();
                        if (is_null($api_statistics)) {  
                           $insId = Userapistatistics::insertGetId([
                            'user_id' => $user->id,
                            'resource_path' => $request->path(),
                            'count_requests' => 1,
                            'created' => Carbon::now()
                          ]);
                        } else {
                            Userapistatistics::where('id', $api_statistics->id)->update([
                              'count_requests' => $api_statistics->count_requests + 1,
                              'updated' => Carbon::now()
                             ]);
                        } 

                    return (new UserResource($user))->response()->setStatusCode(202);
                        
                    }

                } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

                        return response()->json(['token_expired'], $e->getStatusCode());

                } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

                        return response()->json(['token_invalid'], $e->getStatusCode());

                } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {

                        return response()->json(['token_absent'], $e->getStatusCode());
                }
                    
    }

    public function put_update(Request $request)
    {
                try {

                    if (! $user = JWTAuth::parseToken()->authenticate()) {
                          return response()->json(['user_not_found'], 404);
                    } else {

                  $api_statistics = Userapistatistics::where('user_id', $user->id)->where('resource_path', $request->path())->first();
                        if (is_null($api_statistics)) {  
                           $insId = Userapistatistics::insertGetId([
                            'user_id' => $user->id,
                            'resource_path' => $request->path(),
                            'count_requests' => 1,
                            'created' => Carbon::now()
                          ]);
                        } else {
                            Userapistatistics::where('id', $api_statistics->id)->update([
                              'count_requests' => $api_statistics->count_requests + 1,
                              'updated' => Carbon::now()
                             ]);
                        }     
                        
                    $profileInfo = Profile::where('user_id', $user->id)->first();
                        if ($profileInfo) {

                        $rules = [];

                        if(trim($request->firstName)) {
                         $rules['firstName'] = 'required|max:35|regex:/^[a-zA-Z0-9_\s]+$/';   
                         $firstName = trim($request->firstName);
                        } else {
                         $firstName = $profileInfo->first_name;   
                        }

                        if(trim($request->lastName)) {
                         $rules['lastName'] = 'required|max:35|regex:/^[a-zA-Z0-9_\s]+$/'; 
                         $lastName = trim($request->lastName);  
                        } else {
                         $lastName = $profileInfo->last_name;     
                        }

                        if(trim($request->middleName)) {
                         $middleName = trim($request->middleName);
                        } else {
                         $middleName = $profileInfo->middle_name;   
                        }

                        if(trim($request->streetAddress1)) {
                         $rules['streetAddress1'] = 'required|string';
                         $streetAddress1 = trim($request->streetAddress1);   
                        } else {
                         $streetAddress1 = $profileInfo->address;    
                        }

                        if(trim($request->streetAddress2)) {
                         $streetAddress2 = trim($request->streetAddress2);   
                        } else {
                         $streetAddress2 = $profileInfo->address_2;    
                        }
                        
                        if(trim($request->city)) {
                         $rules['city'] = 'required|string'; 
                         $city = trim($request->city);  
                        } else {
                         $city = $profileInfo->city;       
                        }

                        if(trim($request->postalCode)) {
                         $rules['postalCode'] = 'required|max:6|regex:/^[0-9-]+$/'; 
                         $postalCode = trim($request->postalCode);
                        } else {
                         $postalCode = $profileInfo->zip;   
                        }

                        if(trim($request->state)) {

                         if ($profileInfo->is_international==0) { 

                          $rules['state'] = 'required|regex:/^[a-zA-Z]+$/|max:2|min:2';

                          $stateName = trim($request->state);
                          $stateItem = State::where('abbr', '=', $stateName)->where('military', '=', 0)->first();

                            if(!$stateItem) {
                                $state = $profileInfo->state;
                                $response = [
                                    'requestId' => strtolower(Str::random(30)),
                                    'statusCode' => 400,
                                    'message'    => "Invalid parameter: state",
                                   ];
                                return response()->json($response);
                              } else {
                                $state = trim($request->state);
                              }
                           } else {
                           $state = $profileInfo->state;  
                          }
                        } else {
                           $state = $profileInfo->state;  
                        }

                        if(trim($request->province)) {
                         if ($profileInfo->is_international==1) {   
                          $rules['province'] = 'required|regex:/^[a-zA-Z]+$/';
                           $province = trim($request->province);
                           } else {
                           $province = $profileInfo->province;  
                          }
                        } else {
                           $province = $profileInfo->province; 
                        }

                        if(trim($request->driverLicence)) {
                         $driverLicence = trim($request->driverLicence);
                        } else {
                         $driverLicence = $profileInfo->driver_licence;   
                        } 

                        if(trim($request->skype)) {
                         if ($profileInfo->is_international==1) {    
                           $skype = trim($request->skype);
                          } else {
                           $skype = $profileInfo->skype; 
                          }
                        } else {
                         $skype = $profileInfo->skype;   
                        }         

                        $validator = Validator::make($request->all(), $rules);
                        
                        if($validator->fails()) {
                                $response = [
                                    'requestId' => strtolower(Str::random(30)),
                                    'message'    => 'Validation Error.',
                                    'error'    => $validator->errors(),
                                    'statusCode' => 422,
                                   ];
                                return response()->json($response);

                            } else {
                            
                            $updateProfileInfo = Profile::where('user_id', $user->id)
                                     ->update([
                                      'last_name' => $lastName,
                                      'first_name' => $firstName,
                                      'middle_name' => $middleName,
                                      'reg_first_name' => $firstName,
                                      'reg_last_name' => $lastName,
                                      'address' => $streetAddress1,
                                      'address_2' => $streetAddress2,
                                      'city' => $city,
                                      'state' => $state,
                                      'zip' => $postalCode,
                                      'province' => $province,
                                      'driver_licence' => $driverLicence,
                                      'skype' => $skype
                                    ]);

                            $response = [
                                'requestId' => strtolower(Str::random(30)),
                                'id' => (int) $profileInfo->id,
                                'user_id' => (int) $user->id,
                                'statusCode' => 200,
                               ];
                            return response()->json($response);   

                            }    

                        } else {
                            $response = [
                            'requestId' => strtolower(Str::random(30)),
                            'message'    => 'Record does not exist.',
                            'statusCode' => 404,
                           ];
                        }
                    }

                } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

                        return response()->json(['token_expired'], $e->getStatusCode());

                } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

                        return response()->json(['token_invalid'], $e->getStatusCode());

                } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {

                        return response()->json(['token_absent'], $e->getStatusCode());

                }

    }

    public function post_apitoken(Request $request)
    {
                try {

                    if (! $user = JWTAuth::parseToken()->authenticate()) {
                            return response()->json(['user_not_found'], 404);
                    } else {
                      $rules = array (
                          'siteAddress' => 'required|regex:/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/',
                          'planTime' => 'required|date_format:Y-m-d',
                          'parcelAmount' => 'required|numeric',
                        );

                      $validator = Validator::make($request->all(), $rules);
                      if($validator->fails()) {
                                $response = [
                                    'requestId' => strtolower(Str::random(30)),
                                    'message'    => 'Validation Error.',
                                    'error'    => $validator->errors(),
                                    'statusCode' => 422,
                                   ];
                                return response()->json($response);
                            } 

                    $api_statistics = Userapistatistics::where('user_id', $user->id)->where('resource_path', $request->path())->first();
                        if (is_null($api_statistics)) {  
                           $insId = Userapistatistics::insertGetId([
                            'user_id' => $user->id,
                            'resource_path' => $request->path(),
                            'count_requests' => 1,
                            'created' => Carbon::now()
                          ]);
                        } else {
                            Userapistatistics::where('id', $api_statistics->id)->update([
                              'count_requests' => $api_statistics->count_requests + 1,
                              'updated' => Carbon::now()
                             ]);
                        }         

                    $apirequestExists = Apirequests::where('user_id', '=', $user->id)->first();
                    if(!$apirequestExists) {
                      $text = trim($request->textMessage);
                      $siteAddress = trim($request->siteAddress);
                      $planTime = trim($request->planTime);
                      $parcelAmount = trim($request->parcelAmount);
                      $Apirequest = Apirequests::insert([
                                  'user_id' => $user->id,
                                  'active' => 0,
                                  'created' => Carbon::now(),
                                  'text' => $text,
                                  'site_address' => $siteAddress,
                                  'plan_time' => $planTime,
                                  'parcels_amount' => $parcelAmount
                                ]);

                      $local_token = Str::random(64);
                      $userApi = Userapi::insert([
                              'local_token' => $local_token,
                              'user_id' => $user->id,
                              'active' => 0,
                              'created' => Carbon::now()
                         ]);

                        $response = [
                          'requestId' => strtolower(Str::random(30)),
                          'message'   => "Your request for API authToken is created successfully",
                          'statusCode' => 200,
                        ];
                        return response()->json($response);

                    } else {
                      
                        $response = [
                          'requestId' => strtolower(Str::random(30)),
                          'message'   => "Your request for Api authToken is already received.",
                          'statusCode' => 200,
                        ];
                        return response()->json($response);
                      }
                          
                  }

                } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

                        return response()->json(['token_expired'], $e->getStatusCode());

                } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

                        return response()->json(['token_invalid'], $e->getStatusCode());

                } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {

                        return response()->json(['token_absent'], $e->getStatusCode());

                }

    }

    public function get_apitoken(Request $request)
    {
                try {

                  if (! $user = JWTAuth::parseToken()->authenticate()) {
                          return response()->json(['user_not_found'], 404);
                  } else {
                    $api_statistics = Userapistatistics::where('user_id', $user->id)->where('resource_path', $request->path())->first();
                        if (is_null($api_statistics)) {  
                           $insId = Userapistatistics::insertGetId([
                            'user_id' => $user->id,
                            'resource_path' => $request->path(),
                            'count_requests' => 1,
                            'created' => Carbon::now()
                          ]);
                        } else {
                            Userapistatistics::where('id', $api_statistics->id)->update([
                              'count_requests' => $api_statistics->count_requests + 1,
                              'updated' => Carbon::now()
                             ]);
                        }

                    $apirequestExists = Apirequests::where('user_id', '=', $user->id)->first();
                    if(!$apirequestExists) {
                        $response = [
                          'requestId' => strtolower(Str::random(30)),
                          'message'   => "API authToken request does not found",
                          'statusCode' => 203,
                        ];
                        return response()->json($response);
                    } else {
                        if($apirequestExists->active == 2) {
                          $response = [
                            'requestId' => strtolower(Str::random(30)),
                            'message'   => "Your request has been rejected.",
                            'statusCode' => 203,
                          ];
                        return response()->json($response);
                        } elseif($apirequestExists->active == 1) {
                          $user_api = Userapi::where('user_id', '=', $user->id)->first();
                          $response = [
                            'requestId' => strtolower(Str::random(30)),
                            'Approved'   => (bool) $user_api->active,
                            'Token' => $user_api->auth_token,
                            'statusCode' => 200,
                          ];
                        return response()->json($response);
                        } else {
                          $response = [
                            'requestId' => strtolower(Str::random(30)),
                            'message'   => "Your request is currently under review.",
                            'statusCode' => 203,
                          ];
                        return response()->json($response);
                        }
                    }

                  }

                } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

                        return response()->json(['token_expired'], $e->getStatusCode());

                } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

                        return response()->json(['token_invalid'], $e->getStatusCode());

                } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {

                        return response()->json(['token_absent'], $e->getStatusCode());
                }
                    
        }

}
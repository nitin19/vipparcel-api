<?php

namespace App\Http\Controllers\Api\Shipping;

use App\Models\User;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Api\APIBaseController as APIBaseController;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Str;
use App\Models\Label;
use Auth;
use DB;
use Exception;
use Log;
use Session;
use Hash;
use Image;
use EasyPost\Address;
use EasyPost\EasyPost;
use EasyPost\Parcel;
use EasyPost\Shipment;
use EasyPost\Rate;


class LabelController extends APIBaseController
{

    public $successStatus = 200;
    protected $_values;
    protected $commission = '3';

    public function __construct()
    {
       //
    }

    public function get_mailClasses(Request $request)
    {   
        try {
            $token = trim($request->authToken);
            $uri = $request->path();
            if($token) {
                if($uri == 'api/v1/shipping/label/mailClasses') {
                    $auth_token = DB::table('user_api')->where('auth_token', $token)->first();
                    if (is_null($auth_token)) { 
                        $response = [
                        'requestId' => strtolower(Str::random(30)),
                        'message'    => 'Token does not exist.',
                        'statusCode' => 401,
                        ];
                    } else {
                        if($auth_token->active == 1) {
                        $user = DB::table('users')->where('id', $auth_token->user_id)->where('in_archive', 0)->first();
                        if ($user) {    
                            $records =  array(
                            'domestic' => array(
                                'First' => 'First-Class Package',
                                // 'FirstLetter'        => 'First-Class Letter',
                                'FirstLetterCertifiedMail' => 'First-Class Letter (CertifiedMail)',
                                'Priority' => 'Priority',
                                'PriorityExpress' => 'Priority Express',

                                'SmallFlatRateBox' => 'Priority Small Flat Rate Box',
                                'MediumFlatRateBox' => 'Priority Medium Flat Rate Box',
                                'LargeFlatRateBox' => 'Priority Large Flat Rate Box',

                                'FlatRatePaddedEnvelope' => 'Priority Padded Flat Rate Envelope',/**/
                                'FlatRateEnvelope' => 'Priority Flat Rate Envelope',
                                'FlatRateLegalEnvelope' => 'Priority Flat Rate Legal Envelope',
                                'FlatRateGiftCardEnvelope' => 'Priority Flat Rate Gift Card Envelope',
                                'FlatRateWindowEnvelope' => 'Priority Flat Rate Window Envelope',
                                'FlatRateCardboardEnvelope' => 'Priority Rate Cardboard Envelope',
                                'SmallFlatRateEnvelope' => 'Priority Small Flat Rate Envelope',

                                // @TODO fix it (see method get_mail_class_title and $data['MailpieceShapeExpress'] = '1'; on filter)
                                'ExpressFlatRatePaddedEnvelope' => 'Priority Express Padded Envelopes',
                                'ExpressFlatRateEnvelope' => 'Priority Express Flat Rate Envelope',
                                'ExpressFlatRateLegalEnvelope' => 'Priority Express Flat Rate Legal Envelope',
                                //'ExpressFlatRateGiftCardEnvelope'  => 'Priority Express Flat Rate Gift Card Envelope',
                                //'ExpressFlatRateWindowEnvelope'    => 'Priority Express Flat Rate Window Envelope',
                                //'ExpressFlatRateCardboardEnvelope' => 'Priority Express Rate Cardboard Envelope',
                                //'ExpressSmallFlatRateEnvelope'     => 'Priority Express Small Flat Rate Envelope',

                                'RegionalRateBoxA' => 'Priority Regional Rate Box (A)',
                                'RegionalRateBoxB' => 'Priority Regional Rate Box (B)',
                               // REMOVED USPS; 'RegionalRateBoxC' => 'Priority Regional Rate Box (C)',
                                'MediaMail' => 'Media Mail',
                                'LibraryMail' => 'Library Mail',
                                'ParcelSelect' => 'Parcel Select Ground', // with SortType = Nonpresorted
                            ),
                            'international' => array(
                                'PriorityMailExpressInternational' => 'Priority Express International',
                                'PriorityMailInternational' => 'Priority International',
                                'FirstClassPackageInternationalService' => 'First-Class Package',
                                'FirstClassMailInternationalFlatRate' => 'First-Class Mail Flat Rate',
                                'FirstClassMailInternationalLetterRate' => 'First-Class Mail Letter Rate',
                                'PriorityMailInternationalSmallFlatRateBox' => 'Priority Small Flat Rate Box',
                                'PriorityMailInternationalMediumFlatRateBox' => 'Priority Medium Flat Rate Box',
                                'PriorityMailInternationalLargeFlatRateBox' => 'Priority Large Flat Rate Box',
                                'PriorityMailInternationalFlatRatePaddedEnvelope' => 'Priority Flat Rate Padded Envelope',/**/
                                'PriorityMailInternationalFlatRateGiftCardEnvelope' => 'Priority Flat Rate Gift Card Envelope',
                                'PriorityMailInternationalFlatRateWindowEnvelope' => 'Priority Flat Rate Window Card Envelope',
                                'PriorityMailInternationalFlatRateCardboardEnvelope' => 'Priority Flat Rate Cardboard Card Envelope',
                                'PriorityMailInternationalSmallFlatRateEnvelope' => 'Priority Small Flat Rate Envelope',
                                'PriorityMailExpressInternationalFlatRateEnvelope' => 'Priority Express Flat Rate Envelope',
                                'PriorityMailExpressInternationalFlatRateLegalEnvelope' => 'Priority Express Flat Rate Legal Envelope',
                                //'GXG'                                                   => 'Global Express Guaranteed',
                            ),
                            'envelope' => [
                                'FirstClassMail' => 'First-Class Mail',
                                'PriorityMail' => 'Priority Mail'
                            ]
                        );
                            if (count($records) > 0) {
                                $response = [
                                'requestId' => strtolower(Str::random(30)),
                                'records'    => $records,
                                'statusCode' => $this->successStatus,
                               ];
                              } else {
                                $response = [
                                'requestId' => strtolower(Str::random(30)),
                                'message'    => 'Record does not exist.',
                                'statusCode' => 401,
                               ];
                            }

                            $api_statistics = DB::table('user_api_statistics')->where('user_id', $auth_token->user_id)->where('resource_path', $request->path())->first();

                            if (is_null($api_statistics)) { 
                               $insId = DB::table('user_api_statistics')->insertGetId(
                                 ['user_id' => $auth_token->user_id, 'resource_path' => $request->path(), 'count_requests' => 1, 'created' => date('Y-m-d H:i:s')]
                                );
                            } else {
                                DB::table('user_api_statistics')->where('id', $api_statistics->id)->update(['count_requests' => $api_statistics->count_requests + 1, 'updated' => date('Y-m-d H:i:s')]);
                            }
                          } else {
                                $response = [
                                'requestId' => strtolower(Str::random(30)),
                                'statusCode' => 403,
                                'message'    => "You don't have permission to access.",
                               ];
                            }   
                        } else {
                            $response = [
                            'requestId' => strtolower(Str::random(30)),
                            'message' => 'Token is locked.',
                            'statusCode'    => 403,
                            ];
                        }
                    }  
                } else {
                        $response = [
                        'requestId' => strtolower(Str::random(30)),
                        'statusCode'    => 405,
                        'error' => 'The requested URL '. $uri .'was not found on this server.',
                        ];
                }
            } else {
                    if($uri == 'api/v1/shipping/label/mailClasses') {
                            $records =  array(
                            'domestic' => array(
                                'First' => 'First-Class Package',
                                // 'FirstLetter'        => 'First-Class Letter',
                                'FirstLetterCertifiedMail' => 'First-Class Letter (CertifiedMail)',
                                'Priority' => 'Priority',
                                'PriorityExpress' => 'Priority Express',

                                'SmallFlatRateBox' => 'Priority Small Flat Rate Box',
                                'MediumFlatRateBox' => 'Priority Medium Flat Rate Box',
                                'LargeFlatRateBox' => 'Priority Large Flat Rate Box',

                                'FlatRatePaddedEnvelope' => 'Priority Padded Flat Rate Envelope',/**/
                                'FlatRateEnvelope' => 'Priority Flat Rate Envelope',
                                'FlatRateLegalEnvelope' => 'Priority Flat Rate Legal Envelope',
                                'FlatRateGiftCardEnvelope' => 'Priority Flat Rate Gift Card Envelope',
                                'FlatRateWindowEnvelope' => 'Priority Flat Rate Window Envelope',
                                'FlatRateCardboardEnvelope' => 'Priority Rate Cardboard Envelope',
                                'SmallFlatRateEnvelope' => 'Priority Small Flat Rate Envelope',

                                // @TODO fix it (see method get_mail_class_title and $data['MailpieceShapeExpress'] = '1'; on filter)
                                'ExpressFlatRatePaddedEnvelope' => 'Priority Express Padded Envelopes',
                                'ExpressFlatRateEnvelope' => 'Priority Express Flat Rate Envelope',
                                'ExpressFlatRateLegalEnvelope' => 'Priority Express Flat Rate Legal Envelope',
                                //'ExpressFlatRateGiftCardEnvelope'  => 'Priority Express Flat Rate Gift Card Envelope',
                                //'ExpressFlatRateWindowEnvelope'    => 'Priority Express Flat Rate Window Envelope',
                                //'ExpressFlatRateCardboardEnvelope' => 'Priority Express Rate Cardboard Envelope',
                                //'ExpressSmallFlatRateEnvelope'     => 'Priority Express Small Flat Rate Envelope',

                                'RegionalRateBoxA' => 'Priority Regional Rate Box (A)',
                                'RegionalRateBoxB' => 'Priority Regional Rate Box (B)',
                               // REMOVED USPS; 'RegionalRateBoxC' => 'Priority Regional Rate Box (C)',
                                'MediaMail' => 'Media Mail',
                                'LibraryMail' => 'Library Mail',
                                'ParcelSelect' => 'Parcel Select Ground', // with SortType = Nonpresorted
                            ),
                            'international' => array(
                                'PriorityMailExpressInternational' => 'Priority Express International',
                                'PriorityMailInternational' => 'Priority International',
                                'FirstClassPackageInternationalService' => 'First-Class Package',
                                'FirstClassMailInternationalFlatRate' => 'First-Class Mail Flat Rate',
                                'FirstClassMailInternationalLetterRate' => 'First-Class Mail Letter Rate',
                                'PriorityMailInternationalSmallFlatRateBox' => 'Priority Small Flat Rate Box',
                                'PriorityMailInternationalMediumFlatRateBox' => 'Priority Medium Flat Rate Box',
                                'PriorityMailInternationalLargeFlatRateBox' => 'Priority Large Flat Rate Box',
                                'PriorityMailInternationalFlatRatePaddedEnvelope' => 'Priority Flat Rate Padded Envelope',/**/
                                'PriorityMailInternationalFlatRateGiftCardEnvelope' => 'Priority Flat Rate Gift Card Envelope',
                                'PriorityMailInternationalFlatRateWindowEnvelope' => 'Priority Flat Rate Window Card Envelope',
                                'PriorityMailInternationalFlatRateCardboardEnvelope' => 'Priority Flat Rate Cardboard Card Envelope',
                                'PriorityMailInternationalSmallFlatRateEnvelope' => 'Priority Small Flat Rate Envelope',
                                'PriorityMailExpressInternationalFlatRateEnvelope' => 'Priority Express Flat Rate Envelope',
                                'PriorityMailExpressInternationalFlatRateLegalEnvelope' => 'Priority Express Flat Rate Legal Envelope',
                                //'GXG'                                                   => 'Global Express Guaranteed',
                            ),
                            'envelope' => [
                                'FirstClassMail' => 'First-Class Mail',
                                'PriorityMail' => 'Priority Mail'
                            ]
                        );
                            if (count($records) > 0) {
                                $response = [
                                'requestId' => strtolower(Str::random(30)),
                                'records'    => $records,
                                'statusCode' => $this->successStatus,
                               ];
                              } else {
                                $response = [
                                'requestId' => strtolower(Str::random(30)),
                                'message'    => 'Record does not exist.',
                                'statusCode' => 401,
                               ];
                            }
                    } else {
                        $response = [
                        'requestId' => strtolower(Str::random(30)),
                        'statusCode'    => 405,
                        'error' => 'The requested URL '. $uri .'was not found on this server.',
                        ];
                    }
            }
            return response()->json($response);
        } catch (\Exception $e) {
            return $e->getMessage();
        }    
    }
    public function get_getList(Request $request)
    {   
        try {
            $token = trim($request->authToken);
            $uri = $request->path();
            $fullUrl = $request->fullUrl(); 
            $records = array();
            $finlarr = array();

            if($token) {
                if($uri == 'api/v1/shipping/label/getList') {
                    $auth_token = DB::table('user_api')->where('auth_token', $token)->first();
                    if (is_null($auth_token)) { 
                        $response = [
                        'requestId' => strtolower(Str::random(30)),
                        'message'    => 'Token does not exist.',
                        'statusCode' => 401,
                        ];
                        return response()->json($response);
                    } else {
                        if($auth_token->active == 1) {
                            $user = DB::table('users')->where('id', $auth_token->user_id)->where('in_archive', 0)->first();
                            if ($user) {
                             $limit = trim($request->limit);
                                if($limit) {
                                    if($limit <= 150) {
                                        $limit = trim($request->limit);
                                    } else {
                                        $response = [
                                            'requestId' => strtolower(Str::random(30)),
                                            'statusCode' => 400,
                                            'error'    => "Invalid parameter: limit",
                                           ];
                                        return response()->json($response);   
                                    }
                                } else {
                                    $limit = 30;
                                }
                                $offset = trim($request->offset);
                                if($offset) {
                                   $offset = trim($request->offset);
                                } else {
                                   $offset = 0; 
                                }
                                $orderByArr = array('id', 'trackNumber', 'labelType', 'created', 'updated');
                                    if (is_array($request->orderBy)) {
                                   foreach($request->orderBy as $Key=>$Value) {
                                     break;
                                   }

                                  if($Key) {
                                    if(in_array(trim($Key), $orderByArr)) {
                                    if(($Value!='') && ( $Value == 'asc' || $Value == 'ASC' || $Value == 'desc' || $Value == 'DESC' )){

                                        switch ($Key) {
                                        case 'id':
                                            $sortKey = 'id';
                                            $sortValue = $Value;
                                            break;
                                        case 'trackNumber':
                                            $sortKey = 'tracking_number';
                                            $sortValue = $Value;
                                            break;
                                        case 'labelType':
                                            $sortKey = 'is_international';
                                            $sortValue = $Value;
                                            break;
                                        case 'created':
                                            $sortKey = 'created';
                                            $sortValue = $Value;
                                            break;
                                        case 'updated':
                                            $sortKey = 'updated';
                                            $sortValue = $Value;
                                            break;
                                        } 
                                       } else {
                                        $response = [
                                            'requestId' => strtolower(Str::random(30)),
                                            'statusCode' => 400,
                                            'error'    => "Invalid parameter: orderBy",
                                           ];
                                        return response()->json($response);
                                       }
                                    } else {
                                    $response = [
                                            'requestId' => strtolower(Str::random(30)),
                                            'statusCode' => 400,
                                            'error'    => "Field \"$Key\" is not sortable",
                                           ];
                                        return response()->json($response);
                                   }
                                 } else {
                                   $response = [
                                            'requestId' => strtolower(Str::random(30)),
                                            'statusCode' => 400,
                                            'error'    => "Field \"0\" is not sortable",
                                           ];
                                        return response()->json($response);
                                  } 

                                } else {

                                $orderBy = trim($request->orderBy);
                                if($orderBy) {
                                   if(in_array(trim($request->orderBy), $orderByArr)) {
                                    switch ($orderBy) {
                                        case 'id':
                                            $sortKey = 'id';
                                            $sortValue = 'asc';
                                            break;
                                        case 'trackNumber':
                                            $sortKey = 'tracking_number';
                                            $sortValue = 'asc';
                                            break;
                                        case 'labelType':
                                            $sortKey = 'is_international';
                                            $sortValue = 'asc';
                                            break;
                                        case 'created':
                                            $sortKey = 'created';
                                            $sortValue = 'asc';
                                            break;
                                        case 'updated':
                                            $sortKey = 'updated';
                                            $sortValue = 'asc';
                                            break;
                                        }
                                     
                                   } else {
                                    $response = [
                                            'requestId' => strtolower(Str::random(30)),
                                            'statusCode' => 400,
                                            'error'    => "Field \"$orderBy\" is not sortable",
                                           ];
                                   return response()->json($response);
                                   }
                                } else {
                                 if (strpos($fullUrl,'orderBy') !== false) {
                                      $response = [
                                            'requestId' => strtolower(Str::random(30)),
                                            'statusCode' => 400,
                                            'error'    => "Invalid parameter: orderBy",
                                           ];
                                        return response()->json($response);
                                     } else {
                                      $sortKey = 'id';
                                      $sortValue = 'asc';
                                    }
                                  }     
                            } 
                            $labels_data = DB::table('labels AS label')
                                    ->select('label.*','tracking.id AS tracking:id', 'tracking.label_id AS tracking:label_id', 'tracking.created AS tracking:created', 'tracking.updated AS tracking:updated')
                                    ->leftJoin('label_trackings AS tracking', 'label.id', '=', 'tracking.label_id')
                                    ->where('label.user_id', '=', $auth_token->user_id)
                                    ->where('label.is_test_label', '=', 0)
                                    ->get();

                            $labels_data = DB::table('labels AS label')
                                    ->select("label.*","trackings.id AS tracking_id","trackings.label_id AS tracking_label_id","trackings.created AS tracking_created","trackings.updated AS tracking_updated")
                                    ->leftJoin('label_trackings AS trackings', 'label.id', '=', 'trackings.label_id')
                                    ->where('label.user_id', '=',$auth_token->user_id)
                                    ->where('label.is_test_label', '=', 0)
                                    ->orderBy($sortKey, $sortValue)
                                    ->offset($offset)
                                    ->limit($limit)
                                    ->get();

                            $totalRecords = count($labels_data);
                            if($totalRecords > 0) {
                                $records = $address = array();
                                foreach($labels_data as $getlabelsdata) {
                                        $getlist_sender_recipient_datas = $getlabelsdata->post_data;
                                       
                                        $gg = json_decode($getlist_sender_recipient_datas);
                                        $array = json_decode(json_encode($gg), True);
                                        $is_international = $getlabelsdata->is_international;
                                        if($is_international == '1'){
                                            $label_type = 'international';
                                        } else {
                                            $label_type = 'domestic';
                                        }
                                        $records['id'] = (int)$getlabelsdata->id;
                                        $records['trackNumber'] = $getlabelsdata->tracking_number;
                                        $records['labelType'] = $label_type;
                                        $records['weight0z'] = $array['WeightOz'];
                                        $records['mailClass'] = $getlabelsdata->mail_class_key_full;
                                        $records['description'] = $array['Description'];
                                        $records['insuranceValue'] = $array['InsuredValue'];
                                        if(! $getlabelsdata->is_test_label){
                                            $payment_id = $getlabelsdata->payment_id;
                                            $payment_data = DB::table('payments')
                                                                ->where('id','=',$payment_id)
                                                                ->first();
                                            if($payment_data !=''){
                                                $records['value'] = $payment_data->amount;
                                            } else {
                                                $records['value'] ='';
                                            }
                                        } else {
                                            $records['value'] = $getlabelsdata->total_value;
                                        }
                                        
                                        $records['created'] = $getlabelsdata->created;
                                        $records['updated'] = $getlabelsdata->updated;

                                        $optionalFields = $request->optionalFields; 
                                        if($optionalFields!=''){
                                        if(in_array('recipient',$optionalFields)){
                                            $records['recipient'] = array(
                                                'firstName' => $getlabelsdata->to_first_name,
                                                'lastName' => $getlabelsdata->to_last_name,
                                                'city' => $getlabelsdata->to_city,
                                                'address' => $array['ToAddress1'],
                                                'postalCode' => $getlabelsdata->to_postal_code,
                                                'phone' => $array['ToPhone'],
                                                'email' => $array['ToEMail'],
                                            );
                                                if(! $is_international){
                                                    $statecode = $array['ToState'];
                                                    $military_state = $array['military_state'];
                                                    if($statecode!=''){
                                                    $state_data = DB::table('states')
                                                                ->where('abbr','=',$statecode)
                                                                ->first();
                                                    } else {
                                                    $state_data = DB::table('states')
                                                                ->where('name','=',$military_state)
                                                                ->first();
                                                    }
                                                    $state_data_result = json_decode(json_encode($state_data), True);
                                                    $records['recipient']['company'] = $array['ToCompany'];
                                                    $records['recipient']['stateId'] = $state_data_result['id'];
                                                    $records['recipient']['stateCode'] = $state_data_result['abbr'];
                                                    $records['recipient']['stateName'] = $state_data_result['name'];
                                                } else {
                                                    $countryid = $getlabelsdata->to_country;
                                                    if($countryid!=''){
                                                        $country_data = DB::table('countries')
                                                                ->where('idCountry',$countryid)
                                                                ->first();
                                                        $records['recipient']['province'] = '';
                                                        $records['recipient']['countryName'] = $country_data->countryName;
                                                        $records['recipient']['countryCode'] = $country_data->countryCode;
                                                        $records['recipient']['countryId'] = $country_data->idCountry;
                                                    } else {
                                                        $records['recipient']['province'] = '';
                                                        $records['recipient']['countryName'] = '';
                                                        $records['recipient']['countryCode'] = '';
                                                        $records['recipient']['countryId'] = '';
                                                    }
                                                }
                                        } 
                                        if(in_array('sender',$optionalFields)){
                                            $statecode = $array['FromState'];
                                            $state_data = DB::table('states')
                                                                ->where('abbr','=',$statecode)
                                                                ->first();
                                            $state_data_result = json_decode(json_encode($state_data), True);
                                            $records['sender'] = array(
                                                'firstName' => $array['from_first_name'],
                                                'lastName' => $array['from_last_name'],
                                                'company' => $array['FromCompany'],
                                                'city' => $array['FromCity'],
                                                'address' => $array['ReturnAddress1'],
                                                'postalCode' => $array['FromPostalCode'],
                                                'zip4' => $array['FromZIP4'],
                                                'phone' => $array['FromPhone'],
                                                'email' => $array['FromEMail'],
                                                'stateId' => $state_data_result['id'],
                                                'stateCode' => $state_data_result['abbr'],
                                                'stateName' => $state_data_result['name'],
                                            );
                                        } 
                                        
                                } else {
                                    $optionalFields[]='';
                                }
                                array_push($finlarr,$records);
                              }
                              //exit();
                            }
                            if (count($records) >0) {
                                    $response = [
                                    'requestId' => strtolower(Str::random(30)),
                                    'totalRecords' => $totalRecords,
                                    'records'    => $finlarr,
                                    'statusCode' => $this->successStatus,
                                   ];
                                 return response()->json($response);
                                  } else {
                                    $response = [
                                    'requestId' => strtolower(Str::random(30)),
                                    'totalRecords' => $totalRecords,
                                    'records'    => $finlarr,
                                    'statusCode' => $this->successStatus,
                                   ];
                                 return response()->json($response);
                                }
                              } else {
                                $response = [
                                'requestId' => strtolower(Str::random(30)),
                                'statusCode' => 403,
                                'message'    => "You don't have permission to access.",
                               ];
                               return response()->json($response);
                            }
                        } else {
                            $response = [
                            'requestId' => strtolower(Str::random(30)),
                            'message' => 'Token is locked.',
                            'statusCode'    => 403,
                            ];
                            return response()->json($response);
                        }
                    }  
                } else {
                        $response = [
                        'requestId' => strtolower(Str::random(30)),
                        'statusCode'    => 405,
                        'error' => 'The requested URL '. $uri .'was not found on this server.',
                        ];
                        return response()->json($response);
                }
            } else {
                if($request->orderBy){
                $response = [
                    'requestId' => strtolower(Str::random(30)),
                    'statusCode' => 400,
                    'error'    => 'Required parameter not passed: authToken',
                ];
                } else {
                $response = [
                    'requestId' => strtolower(Str::random(30)),
                    'statusCode' => 400,
                    'error'    => 'Required parameter not passed: authToken',
                ];
                }
                return response()->json($response);

        }
    }
        catch (\Exception $e) {
            return $e->getMessage();
        } 
    }

    public function get_getImages(Request $request, $id)
    {   
        try {
            $token = trim($request->authToken);
            $uri = $request->path();
            $fullUrl = $request->fullUrl(); 
            $is_test_request = '1';
            $finalrecords = array();

            if($token) {
                if($uri == 'api/v1/shipping/label/getImages/'.$id) {
                    $auth_token = DB::table('user_api')->where('auth_token', $token)->first();
                    if (is_null($auth_token)) { 
                        $response = [
                        'requestId' => strtolower(Str::random(30)),
                        'message'    => 'Token does not exist.',
                        'statusCode' => 401,
                        ];
                        return response()->json($response);
                    } else {
                        if($auth_token->active == 1) {
                        $user = DB::table('users')->where('id', $auth_token->user_id)->where('in_archive', 0)->first();
                            if ($user) {
                                if(! $id){
                                   $response = [
                                        'requestId' => strtolower(Str::random(30)),
                                        'statusCode' => 400,
                                        'message'    => 'Invalid query: required param "id"',
                                    ]; 
                                    return response()->json($response);
                                } 

                                /**is_test_request condition use here***/

                                $labels_data = DB::table('labels AS label')
                                    ->select('label.*','tracking.id AS tracking:id', 'tracking.label_id AS tracking:label_id', 'tracking.created AS tracking:created', 'tracking.updated AS tracking:updated')
                                    ->leftJoin('label_trackings AS tracking', 'label.id', '=', 'tracking.label_id')
                                    ->where('label.user_id', '=', 46)
                                    ->where('label.id', '=', $id)
                                    ->first();

                                if(! $labels_data){
                                    $labels_sub_data = DB::table('labels')
                                                        ->where('tracking_number', '=',$id)
                                                        ->first();
                                    if(! $labels_sub_data){
                                        $response = [
                                            'requestId' => strtolower(Str::random(30)),
                                            'statusCode' => 404,
                                            'message'    => 'Not Found',
                                        ];
                                        return response()->json($response);
                                    }
                                }
                                $labels_sub_data = DB::table('label_images')
                                                        ->where('label_id', '=',$id)
                                                        ->get();
                               $allimagespath='';
                                foreach ($labels_sub_data as $labels_sub_datas) {
                                    $label_images_path = $labels_sub_datas->file_path;

                                    if(strrpos($label_images_path, '/upload') === FALSE){
                                        $imagepath['images'] = $label_images_path;
                                    }
                                    else {
                                        $site_url = url('/public');
                                        //if(! empty($margin)){
                                        $imagepath['images'] = $site_url.$label_images_path;
                                    }
                                    $allimagespath.= $imagepath['images'].',';
                                    
                                    
                                }
                                    $allimagespaths = rtrim($allimagespath,',');
                                    $all_images_path = explode(',', $allimagespaths);
                                if($all_images_path){
                                    $response = [
                                            'requestId' => strtolower(Str::random(30)),
                                            'images' => $all_images_path,
                                            'statusCode' => $this->successStatus,
                                        ];
                                    return response()->json($response);
                                }else {
                                    $response = [
                                            'requestId' => strtolower(Str::random(30)),
                                            'images' => $finalrecords,
                                            'statusCode' => $this->successStatus,
                                        ];
                                    return response()->json($response);
                                }

                              } else {
                                $response = [
                                'requestId' => strtolower(Str::random(30)),
                                'statusCode' => 403,
                                'message'    => "You don't have permission to access.",
                               ];
                               return response()->json($response);
                            }
                         } else {
                            $response = [
                            'requestId' => strtolower(Str::random(30)),
                            'message' => 'Token is locked.',
                            'statusCode'    => 403,
                            ];
                            return response()->json($response);
                        }
                    }  
                } else {
                        $response = [
                        'requestId' => strtolower(Str::random(30)),
                        'statusCode'    => 405,
                        'error' => 'The requested URL '. $uri .'was not found on this server.',
                        ];
                        return response()->json($response);
                }
            } else {
                $response = [
                    'requestId' => strtolower(Str::random(30)),
                    'statusCode' => 400,
                    'error'    => 'Required parameter not passed: authToken',
                ];
                return response()->json($response);
            }
           // return response()->json($response);
        } catch (\Exception $e) {
            return $e->getMessage();
        }    
    }

    public function get_getInfo(Request $request, $id)
    {   
        try {
            $token = trim($request->authToken);
            $uri = $request->path();
            $fullUrl = $request->fullUrl(); 
            $is_test_request = '1';
            $records = array();
            $finlarr = array();
            if($token) {
                if($uri == 'api/v1/shipping/label/getInfo/'.$id) {
                    $auth_token = DB::table('user_api')->where('auth_token', $token)->first();
                    if (is_null($auth_token)) { 
                        $response = [
                        'requestId' => strtolower(Str::random(30)),
                        'message'    => 'Token does not exist.',
                        'statusCode' => 401,
                        ];
                        return response()->json($response);
                    } else {
                        if($auth_token->active == 1) {
                            
                        $user = DB::table('users')->where('id', $auth_token->user_id)->where('in_archive', 0)->first();
                            if ($user) {
                                if(! $id){
                                   $response = [
                                        'requestId' => strtolower(Str::random(30)),
                                        'statusCode' => 400,
                                        'message'    => 'Invalid query: required param "id"',
                                    ]; 
                                    return response()->json($response);
                                } 
                                $labels_data = DB::table('labels AS label')
                                    ->select('label.*','tracking.id AS tracking:id', 'tracking.label_id AS tracking:label_id', 'tracking.created AS tracking:created', 'tracking.updated AS tracking:updated')
                                    ->leftJoin('label_trackings AS tracking', 'label.id', '=', 'tracking.label_id')
                                    ->where('label.user_id', '=',$auth_token->user_id)
                                    ->where('label.id', '=', $id)
                                    ->first();
                                if(! $labels_data){
                                    $labels_sub_data = DB::table('labels')
                                                        ->where('tracking_number', '=',$id)
                                                        ->first();
                                    if(! $labels_sub_data){
                                        $response = [
                                            'requestId' => strtolower(Str::random(30)),
                                            'statusCode' => 404,
                                            'message'    => 'Not Found',
                                        ];
                                        return response()->json($response);
                                    }
                                }
  
                                        $getinfo_sender_recipient_datas = $labels_data->post_data;
                                        $get_info_data = json_decode($getinfo_sender_recipient_datas);
                                        $results = json_decode(json_encode($get_info_data), True);
                                        $is_international = $labels_data->is_international;
                                        if($is_international == '1'){
                                            $label_type = 'international';
                                        } else {
                                            $label_type = 'domestic';
                                        }

                                        $records = array(
                                            'requestId'      => strtolower(Str::random(30)),
                                            'id'             => (int)$labels_data->id,
                                            'trackNumber'    => $labels_data->tracking_number,
                                            'labelType'      => $label_type,
                                            'weight0z'       => $results['WeightOz'],
                                            'mailClass'      => $labels_data->mail_class_key_full,
                                            'description'    => $results['Description'],
                                            'insuranceValue' => $results['InsuredValue'],
                                        );
                                            if(! $labels_data->is_test_label){
                                            $payment_id = $labels_data->payment_id;
                                            $payment_data = DB::table('payments')
                                                                ->where('id','=',$payment_id)
                                                                ->first();
                                            if($payment_data !=''){
                                                $records['value'] = $payment_data->amount;
                                            } else {
                                                $records['value'] ='';
                                            }
                                            } else {
                                                $records['value'] = $labels_data->total_value;
                                            }
                                            $records['created'] = $labels_data->created;
                                            $records['updated'] = $labels_data->updated;
                                        
                                        $records['recipient'] = array(
                                                'firstName' => $labels_data->to_first_name,
                                                'lastName' => $labels_data->to_last_name,
                                                'city' => $labels_data->to_city,
                                                'address' => $results['ToAddress1'],
                                                'postalCode' => $labels_data->to_postal_code,
                                                'phone' => $results['ToPhone'],
                                                'email' => $results['ToEMail'],
                                            );
                                        if(! $is_international){
                                                $statecode = $results['ToState'];
                                                $military_state = $results['military_state'];
                                                if($statecode!=''){
                                                    $state_data = DB::table('states')
                                                                ->where('abbr','=',$statecode)
                                                                ->first();

                                                    $records['recipient']['company'] = $results['ToCompany'];
                                                    $records['recipient']['stateId'] = $state_data->id;
                                                    $records['recipient']['stateCode'] = $state_data->abbr;
                                                    $records['recipient']['stateName'] = $state_data->name;
                                                    $records['recipient']['zip4'] = $results['ToZIP4'];
                                                } else {
                                                    $state_data = DB::table('states')
                                                                ->where('name','=',$military_state)
                                                                ->first();
                                                    $records['recipient']['company'] = $results['ToCompany'];
                                                    $records['recipient']['stateId'] = $state_data->id;
                                                    $records['recipient']['stateCode'] = $state_data->abbr;
                                                    $records['recipient']['stateName'] = $state_data->name;
                                                    $records['recipient']['zip4'] = $results['ToZIP4'];
                                                }
                                        } else {
                                                $countryid = $labels_data->to_country;
                                                if($countryid!=''){
                                                    $country_data = DB::table('countries')
                                                            ->where('idCountry',$countryid)
                                                            ->first();
                                                    $records['recipient']['province'] = '';
                                                    $records['recipient']['countryName'] = $country_data->countryName;
                                                    $records['recipient']['countryCode'] = $country_data->countryCode;
                                                    $records['recipient']['countryId'] = $country_data->idCountry;
                                                } else {
                                                    $records['recipient']['province'] = '';
                                                    $records['recipient']['countryName'] = '';
                                                    $records['recipient']['countryCode'] = '';
                                                    $records['recipient']['countryId'] = '';
                                                }
                                        }
                                        $statecode = $results['FromState'];
                                        $state_data = DB::table('states')
                                                                ->where('abbr','=',$statecode)
                                                                ->first();
                                        $records['sender'] = array(
                                                'firstName' => $results['from_first_name'],
                                                'lastName' => $results['from_last_name'],
                                                'company' => $results['FromCompany'],
                                                'city' => $results['FromCity'],
                                                'address' => $results['ReturnAddress1'],
                                                'postalCode' => $results['FromPostalCode'],
                                                'zip4' => $results['FromZIP4'],
                                                'phone' => $results['FromPhone'],
                                                'email' => $results['FromEMail'],
                                                'stateId' => $state_data->id,
                                                'stateCode' => $state_data->abbr,
                                                'stateName' => $state_data->name,
                                        );
                                        if(! $is_international){
                                        
                                        } else {
                                         
                                        $records['customs'] = array(
                                                'eelpfc' => '',
                                                'explanation' => '',
                                                'category' => '',
                                        );
                                        }
                                        $labels_sub_data = DB::table('label_images')
                                                        ->where('label_id', '=',$id)
                                                        ->get();
                                        $allimagespath='';
                                        if(count($labels_sub_data) > 0){
                                        foreach ($labels_sub_data as $labels_sub_datas) {
                                            $label_images_path = $labels_sub_datas->file_path;

                                            if(strrpos($label_images_path, '/upload') === FALSE){
                                                $records['images'] = $label_images_path;
                                            } else {
                                                $site_url = url('/public');
                                                //if(! empty($margin)){
                                                $records['images'] = $site_url.$label_images_path;
                                            }
                                                $allimagespath.= $records['images'].',';
                                        }
                                            $allimagespaths = rtrim($allimagespath,',');
                                            $all_images_paths = explode(',', $allimagespaths);
                                            $records['images'] = $all_images_paths;
                                        } 
                                        $records['statusCode'] = $this->successStatus;
                                    //exit();
                                    if (count($records) >0) {
                                        $response = $records;
                                        return response()->json($response);
                                    } else {
                                       $response = [
                                        'requestId' => strtolower(Str::random(30)),
                                        'message'    => 'Record does not exist.',
                                        'statusCode' => 401,
                                       ];
                                       return response()->json($response);
                                    }
                               
                              } else {
                                $response = [
                                'requestId' => strtolower(Str::random(30)),
                                'statusCode' => 403,
                                'message'    => "You don't have permission to access.",
                               ];
                               return response()->json($response);
                            }
                         } else {
                            $response = [
                            'requestId' => strtolower(Str::random(30)),
                            'message' => 'Token is locked.',
                            'statusCode'    => 403,
                            ];
                            return response()->json($response);
                        }
                    }  
                } else {
                        $response = [
                        'requestId' => strtolower(Str::random(30)),
                        'statusCode'    => 405,
                        'error' => 'The requested URL '. $uri .'was not found on this server.',
                        ];
                        return response()->json($response);
                }
            } else {
                $response = [
                    'requestId' => strtolower(Str::random(30)),
                    'statusCode' => 400,
                    'error'    => 'Required parameter not passed: authToken',
                ];
                return response()->json($response);
            }
           // return response()->json($response);
        } catch (\Exception $e) {
            return $e->getMessage();
        }    
    }

    public function action_post_calculate(Request $request)
    {  
        $settingDetails = DB::table('settings')->where('id', '1')->first();
        $provider_accounts = $settingDetails->provider_accounts;
        $provideraccounts = json_decode($provider_accounts);
        $EasypostDetails = $provideraccounts->Easypost_Default;
        EasyPost::setApiKey($EasypostDetails->api_key);
        try {
                $token = trim($request->authToken);
                $uri = $request->path();
                $fullUrl = $request->fullUrl(); 
                $is_test_request = '1';
                $finalrecords = array();

                if($token) {
                    if($uri == 'api/v1/shipping/label/calculate') {
                        $auth_token = DB::table('user_api')->where('auth_token', $token)->first();
                            if (is_null($auth_token)) { 
                                $response = [
                                'requestId' => strtolower(Str::random(30)),
                                'message'    => 'Token does not exist.',
                                'statusCode' => 401,
                                ];
                                return response()->json($response);
                            } else {
                                if($auth_token->active == 1) {
                                    $user = DB::table('users')->where('id', $auth_token->user_id)->where('in_archive', 0)->first();
                                    if ($user) {

                                        if (!$request->mailClass)
                                        {
                                            $response = [
                                                'requestId' => strtolower(Str::random(30)),
                                                'statusCode' => 400,
                                                'message' => 'Please Select Mail Class',
                                            ];
                                            return response()->json($response);
                                        }

                                        $LabelMailClassData = DB::table('label_mail_class_details')->where('mail_class', $request->mailClass)->where('type', $request->labelType)->first();
                                        if($request->weightOz > $LabelMailClassData->max_weight_oz){
                                            $response = [
                                                'requestId' => strtolower(Str::random(30)),
                                                'statusCode' => 400,
                                                'message' => 'Mailpiece is over maximum weight of '.$LabelMailClassData->max_weight_oz.' oz.',
                                            ];
                                            return response()->json($response);
                                        }
                                            /*$convert_fields = array(
                                                'labelType'             => 'LabelType',
                                                'mailClass'             => 'MailClass',
                                                'weightOz'              => 'WeightOz',
                                                'insuredService'        => 'InsuredService',
                                                'service'               => 'services',
                                                'insuredValue'          => 'InsuredValue',
                                                'senderPostalCode'      => 'FromPostalCode',
                                                'recipientPostalCode'   => 'ToPostalCode',
                                                'countryId'             => 'to_country',
                                                'originZipCode'         => 'OriginZipCode',
                                                'deliveryConfirmation'  => 'DeliveryConfirmation',
                                            ); */
                                            $is_international = (strtolower($request->labelType) == strtolower('international'));
                                            /*if ($is_international = (strtolower($request->labelType) == strtolower('international')))
                                            {
                                                $request->mailClass = 'MailClassInternational';
                                            }
                                            else
                                            {
                                                $request->mailClass = 'MailClass';
                                            }*/
                                            if ( ! $is_international AND $request->countryId)
                                            {
                                                $response = [
                                                        'requestId' => strtolower(Str::random(30)),
                                                        'statusCode' => 400,
                                                        'message' => 'Invalid parameter: countryId',
                                                    ];
                                                return response()->json($response);
                                            }

                                            $post = Array(
                                                    'labelType' => 'LabelType',
                                                    'mailClass' => 'MailClass',
                                                    'weightOz' => 'WeightOz',
                                                    'service' => 'services',
                                                    'insuredValue' => 'InsuredValue',
                                                    'description' => 'Description',
                                                    'imageFormat' => 'image_format',
                                                    'imageResolution' => 'ImageResolution',
                                                    'rubberStamp1' => 'RubberStamp1',
                                                    'rubberStamp2' => 'RubberStamp2',
                                                    'rubberStamp3' => 'RubberStamp3',
                                                    'insuredService' => 'InsuredService',
                                                    'deliveryConfirmation' => 'DeliveryConfirmation',
                                                    'validationAddress' => 'ValidationAddress',
                                                    'reference' => 'reference',
                                            );

                                            if ($dimensional_weight = $request->dimensionalWeight)
                                            {

                                                if (array_key_exists('length', $dimensional_weight)) {
                                                    $request->MailpieceDimensions = array(
                                                    'Length' => $dimensional_weight['length'],
                                                    );
                                                }
                                                 else {
                                                    $response = [
                                                        'requestId' => strtolower(Str::random(30)),
                                                        'statusCode' => 400,
                                                        'message' => 'Validation Failed: Length is required when dimensions are supplied with parcel',
                                                    ];
                                                    return response()->json($response);
                                                }

                                                if (array_key_exists('height', $dimensional_weight)) {
                                                    $request->MailpieceDimensions = array(
                                                    'height' => $dimensional_weight['height'],
                                                    );
                                                                                                    
                                                }
                                                 else {
                                                    $response = [
                                                        'requestId' => strtolower(Str::random(30)),
                                                        'statusCode' => 400,
                                                        'message' => 'Validation Failed: Height is required when dimensions are supplied with parcel',
                                                    ];
                                                    return response()->json($response);
                                                }

                                                if (array_key_exists('width', $dimensional_weight)) {
                                                    $request->MailpieceDimensions = array(
                                                    'width' => $dimensional_weight['width'],
                                                    );                                                  
                                                } 
                                                else {
                                                    $response = [
                                                        'requestId' => strtolower(Str::random(30)),
                                                        'statusCode' => 400,
                                                        'message' => 'Validation Failed: Width is required when dimensions are supplied with parcel',
                                                    ];
                                                    return response()->json($response);
                                                }

                                                if ($request->mailClass != 'Priority')
                                                {
                                                    $response = [
                                                        'requestId' => strtolower(Str::random(30)),
                                                        'statusCode' => 400,
                                                        'message' => 'Invalid parameter: dimensionalWeight. Only for label type: Domestic, mail class: Priority',
                                                    ];
                                                    return response()->json($response);
                                                }
                                                $request->dimensional_weight = 1;
                                                //$request->MailpieceDimensions = array(
                                                   // 'Length' => $dimensional_weight['length'],
                                                   // 'Width'  => $dimensional_weight['width'],
                                                    //'Height' => $dimensional_weight['height'],
                                                //);
                                            }
                   
                                            if ($request->mailClass == 'Priority' && $user->id && $user->is_required_dim_weight && (! $dimensional_weight))
                                            {
                                                $response = [
                                                    'requestId' => strtolower(Str::random(30)),
                                                    'statusCode' => 400,
                                                    'message' => 'Invalid parameter: dimensionalWeight. Field must not be empty for Priority mail class.',
                                                ];
                                                return response()->json($response);
                                            }
                                            if (isset($request->labelType))
                                            {
                                                $request->labelType = ucfirst($request->labelType);
                                            }
                        
                                            if (isset($request->weightOz) && ($request->weightOz > 0))
                                            {
                                                $weight_lbs = $request->weightOz / 16;
                                                if ($weight_lbs < 1)
                                                {
                                                   $convert_weight = array(0, $request->weightOz);
                                                }
                                                $fweight_oz = ($weight_lbs - floor($weight_lbs));
                                                $weight_lbs = $weight_lbs - $fweight_oz;
                                                $convert_weight = array($weight_lbs, $fweight_oz * 16);
                                                $post['WeightLbs'] = $convert_weight[0];
                                                $post['WeightOz'] = $convert_weight[1];
                                            }
                                            else
                                            {
                                                $response = [
                                                    'requestId' => strtolower(Str::random(30)),
                                                    'statusCode' => 400,
                                                    'message' => 'Invalid parameter: weightOz',
                                                ];
                                                return response()->json($response);
                                            }

                                            if (isset($request->services))
                                            {
                                                $response = [
                                                    'requestId' => strtolower(Str::random(30)),
                                                    'statusCode' => 400,
                                                    'message' => 'Invalid parameter: service',
                                                ];
                                                return response()->json($response);
                                            }

                                            $label_type = $request->labelType;
                                            $international = 'International';
                                            $domestic = 'Domestic';

                                            if ( (! $label_type)||(! in_array($label_type, array($international, $domestic))))
                                            {
                                                $response = [
                                                    'requestId' => strtolower(Str::random(30)),
                                                    'statusCode' => 400,
                                                    'message' => 'Invalid parameter: labelType',
                                                ];
                                                return response()->json($response);
                                            }
                                            if ($is_international && (! isset($request->countryId)))
                                            {
                                                $response = [
                                                    'requestId' => strtolower(Str::random(30)),
                                                    'statusCode' => 400,
                                                    'message' => 'Invalid parameter: countryId',
                                                ];
                                                return response()->json($response);
                                            }
                                            if ( ! $request->senderPostalCode)
                                            {
                                                $response = [
                                                        'requestId' => strtolower(Str::random(30)),
                                                        'statusCode' => 400,
                                                        'message' => 'Sender Postal Code Not Correct ',
                                                    ];
                                                return response()->json($response);
                                            }
                                            if ( ! $request->recipientPostalCode)
                                            {
                                                $response = [
                                                        'requestId' => strtolower(Str::random(30)),
                                                        'statusCode' => 400,
                                                        'message' => 'Recipient Postal Code Not Correct',
                                                    ];
                                                return response()->json($response);
                                            }
                                            
                                            $labels_data = DB::table('labels')
                                                        ->where('user_id', '=',$auth_token->user_id)
                                                        ->get();

                                            foreach ($labels_data as $labels_datas) {
                                               $provider_account = $labels_datas->provider_account;
                                               $get_info_data = json_decode($provider_account);
                                               $results = json_decode(json_encode($get_info_data), True);
                                            }                            
                                            $to_address_params = array("zip" => $request->senderPostalCode);
                                            $to_address = Address::create($to_address_params);
                                            $from_address_params = array("zip" => $request->recipientPostalCode);
                                            $from_address = Address::create($from_address_params);
                                            $parcel_params =  $request->MailpieceDimensions;
                                            $parcel_params = array(
                                                                   "predefined_package" => null,
                                                                   "weight" => $request->weightOz,
                                            );
                                            $parcel = Parcel::create($parcel_params);
                                            $shipment_params = array("from_address" => $from_address,
                                                    "to_address"   => $to_address,
                                                    "parcel"       => $parcel
                                            );
                                            $shipment = Shipment::create($shipment_params);
                                            $rates = $shipment->rates;
                                            $adddata  = array();
                                            foreach ($rates as $ratess) {
                                                $list_rate = $ratess->list_rate;
                                                $service = $ratess->service;
                                                $retail_rate = $ratess->retail_rate;
                                                $delivery_days = $ratess->delivery_days;
                                                $arraydata = array_push($adddata,$service,$list_rate,$retail_rate,$delivery_days);
                                            }
                                            $usps_zone = $shipment->usps_zone;
                                            $weight_lbs = $request->weightOz / 16;
                                            $max_weight_oz_data = DB::table('domestic_rates')
                                                        ->where('weight_lb','=', $weight_lbs)
                                                        ->where('zone','=', $usps_zone)
                                                        ->first();


                                            $mailclass = $request->mailClass;
                                            /*if($mailClass == ''){
                                                $list_rate = 7.75;

                                            }else{*/
                                            $servicedata = array_search($mailclass, $adddata);
                                            //print_r($servicedata);
                                            //exit();
                                            $list_rate = $adddata[$servicedata+1];
                                            $retail_rate = $adddata[$servicedata+2];
                                            $delivery_days = $adddata[$servicedata+3];
                                            $insuredValue = $request->insuredValue;
                                         /*}*/

                                            if($insuredValue!=''){
                                                $value= $insuredValue;
                                                $number = $value/100; 
                                                $floating = floor($number); 
                                                $fraction = $number-$floating; 
                                                if($fraction>0){
                                                    $price = ($floating + 1) * 1.9;
                                                }
                                                if($fraction == ''){
                                                    $price = ($floating) * 1.9;
                                                }
                                            }

                                            $deliveryConfirmation = $request->deliveryConfirmation;
                                            if($deliveryConfirmation == 'ADULT_SIGNATURE'){
                                                $adult_signatures = $list_rate + 6.8;
                                            }
                                            elseif($deliveryConfirmation == 'SIGNATURE'){
                                                $signature = $list_rate + 2.96;
                                            }
                                            else{
                                                $no_signature = $list_rate;
                                            }

                                            if ($request->mailClass == 'FirstClassMailInternational')
                                            {
                                                if ($request->MailpieceShape == 'Letter'){
                                                   echo '3.5';
                                                }
                                                elseif ($request->MailpieceShape == 'Flat'){
                                                    echo '64';
                                                }
                                            }
                                            if ($request->mailClass == 'First' && $request->MailpieceShape == 'Letter'){
                                                echo '13';
                                            }   

                                            $labelType = $request->labelType; 
                                            if(!$request->mailClass) {
                                                $max_weight_oz_data = DB::table('label_mail_class_details')
                                                        ->where('type','=', $labelType)
                                                        ->first();
                                            } else {
                                                $max_weight_oz_data = DB::table('label_mail_class_details')
                                                        ->where('mail_class', '=', $request->mailClass)
                                                        ->where('type','=', $labelType)
                                                        ->first();
                                            }
 
                                            if ($labelType == 'international') {
                                            $max_weight_oz_data = DB::table('label_mail_class_details')
                                                        ->where('country_id', '=', $request->countryId)
                                                        ->first();
                                                    
                                            }
                                            $records = array();
                                            $records['requestId'] = strtolower(Str::random(30));
                                            $deliveryConfirmation = $request->deliveryConfirmation;
                                            if($deliveryConfirmation!=''){
                                            if($deliveryConfirmation == 'ADULT_SIGNATURE'){
                                                if($request->insuredValue!=''){
                                                    $records['value'] = (string)($adult_signatures + $price);
                                                } else {
                                                    $records['value'] = (string)$adult_signatures;
                                                } 
                                            }
                                            elseif($deliveryConfirmation == 'SIGNATURE'){
                                                if($request->insuredValue!=''){
                                                    $records['value'] = (string)($signature + $price);
                                                } else {
                                                    $records['value'] = (string)$signature;
                                                } 
                                            }
                                            elseif($deliveryConfirmation == 'NO_SIGNATURE'){
                                                if($request->insuredValue!=''){
                                                    $records['value'] = (string)($no_signature + $price); 
                                                } else {
                                                    $records['value'] = (string)$no_signature;
                                                }
                                            }
                                            }   
                                            elseif($request->insuredValue!=''){
                                                $records['value'] = (string)($price + $list_rate);
                                            }
                                            else {
                                                $records['value'] = (string)$list_rate;
                                            }
                                            $records['valueUsps'] = NULL;
                                            if ($is_international) {
                                                $usps_price = $list_rate;
                                                if ($usps_price && $usps_price > $list_rate) {
                                                    $records['valueUsps'] = $usps_price;
                                                }
                                            } else {
                                                if($list_rate < $retail_rate){ 
                                                    $records['valueUsps'] = (string) $retail_rate;
                                                }
                                            }
                                            $records['deliverDays'] = $delivery_days;
                                            $records['maxWeightOz'] = $max_weight_oz_data->max_weight_oz;
                                            $records['statusCode'] = $this->successStatus;
                                            return response()->json($records);
                                    } else {
                                        $response = [
                                        'requestId' => strtolower(Str::random(30)),
                                        'statusCode' => 403,
                                        'message'    => "You don't have permission to access.",
                                       ];
                                       return response()->json($response);
                                    }
                                } else {
                                    $response = [
                                    'requestId' => strtolower(Str::random(30)),
                                    'message' => 'Token is locked.',
                                    'statusCode'    => 403,
                                    ];
                                    return response()->json($response);
                                }
                            }  
                    } else {
                        $response = [
                        'requestId' => strtolower(Str::random(30)),
                        'statusCode'    => 405,
                        'message' => 'The requested URL '. $uri .'was not found on this server.',
                        ];
                        return response()->json($response);
                    }
                } else {
                    $response = [
                        'requestId' => strtolower(Str::random(30)),
                        'statusCode' => 400,
                        'message'    => 'Required parameter not passed: authToken',
                    ];
                    return response()->json($response);
                }
           // return response()->json($response);
        } catch (\Exception $e) {
            return $e->getMessage();
        }    
    }
    
    public function action_post_print(Request $request){
     
        $settingDetails = DB::table('settings')->where('id', '=', '1')->first();
        $provider_accounts = $settingDetails->provider_accounts;
        $provideraccounts = json_decode($provider_accounts);
        $EasypostDetails = $provideraccounts->Easypost_Default;
        EasyPost::setApiKey($EasypostDetails->api_key);

        $rules = [];
        $rules['labelType'] = 'required';
        $rules['mailClass'] = 'required';
        $rules['weightOz'] = 'required|regex:/^[0-9]+$/';
        $senderoptional = array('company', 'zip4', 'originZipCode', 'phone', 'email');
        $recipientoptional = array('company', 'zip4', 'originZipCode', 'phone', 'email');

        if(is_array($request->sender)){ $sender = $request->sender; } else { $sender = array(); }
        
        foreach($sender as $skey => $sval){
            if(!in_array($skey, $senderoptional) && $sval==''){
                $rules['sender.'.$skey] = 'required';
            }
        }

        if(is_array($request->recipient)){ $recipient = $request->recipient; } else { $recipient = array(); }
        
        foreach($recipient as $rkey => $rval){
            if(!in_array($rkey, $recipientoptional) && $rval==''){
                $rules['recipient.'.$rkey] = 'required';
            }
        }

        if($request->mailClass=='Priority'){
            if(is_array($request->dimensionalWeight)){ $dimensionalWeight = $request->dimensionalWeight; } else { $dimensionalWeight = array(); }
            foreach($dimensionalWeight as $dimensionalkey => $dimensionalval){
                if($dimensionalval==''){
                    $rules['dimensionalWeight.'.$dimensionalkey] = 'required';
                }
            }
        }

        if($request->labelType!='domestic'){
            $rules['countryId'] = 'required|regex:/^[0-9]+$/|max:3|min:1';
        }

        if($request->is_apo_fpo){
            if($request->is_apo_fpo=='1' || $request->is_apo_fpo=='true' || $request->is_apo_fpo=='True'){
                $is_apo_fpo = '1';
            } else {
                $is_apo_fpo = '0';
            }
        } else {
            $is_apo_fpo = '0';
        }
        
        $token = trim($request->authToken);
        $uri = $request->path();
        $fullUrl = $request->fullUrl();
        if($token) {
            if($uri == 'api/v1/shipping/label/print') {
                $auth_token = DB::table('user_api')->where('auth_token', $token)->first();
                if(is_null($auth_token)) { 
                    $response = [
                    'requestId' => strtolower(Str::random(30)),
                    'message'    => 'Token does not exist.',
                    'statusCode' => 401,
                    ];
                    return response()->json($response);
                } else {
                    if($auth_token->active == 1) {
                        $user = DB::table('users')->where('id', $auth_token->user_id)->where('in_archive', 0)->first();
                        if($user){
                            $validator = Validator::make($request->all(), $rules);
                            if($validator->fails()) {
                                $response = [
                                'requestId' => strtolower(Str::random(30)),
                                'message'    => 'Validation Error.',
                                'error'    => $validator->errors(),
                                'statusCode' => 422,
                               ];
                               return response()->json($response);
                            } else {
                                $labelType = $request->labelType;
                                $weightOz = $request->weightOz;
                                $weightLb = intval(ceil($weightOz / 16));
                                $length = $request->dimensionalWeight['length'];
                                $height = $request->dimensionalWeight['height'];
                                $width = $request->dimensionalWeight['width'];
                                $countryId = $request->countryId;
                                $mailClass = $request->mailClass;
                                $insuredValue = $request->insuredValue;
                                $recipientfirstname = $request->recipient['firstName'];
                                $recipientlastname = $request->recipient['lastName'];
                                $recipientstate = $request->recipient['state'];
                                $recipientcity = $request->recipient['city'];
                                $recipientstreetAddress = $request->recipient['streetAddress'];
                                $recipientpostalCode = $request->recipient['postalCode'];
                                $recipientphone = $request->recipient['phone'];
                                $recipientcompany = $request->recipient['company'];
                                $recipientemail = $request->recipient['email']; 
                                $recipientzip4 = $request->recipient['zip4'];
                                $senderfirstname = $request->sender['firstName'];
                                $senderlastname = $request->sender['lastName'];
                                $senderstate = $request->sender['state'];
                                $sendercity = $request->sender['city'];
                                $senderstreetAddress = $request->sender['streetAddress'];
                                $senderpostalCode = $request->sender['postalCode'];
                                $senderphone = $request->sender['phone'];
                                $sendercompany = $request->sender['company'];
                                $senderemail = $request->sender['email']; 
                                $senderzip4 = $request->sender['zip4'];
                                $senderoriginZipCode = $request->sender['originZipCode'];
                                $created = date('Y-m-d h:i:s');
                                if($labelType=='domestic'){
                                    $LabelMailClassDetails = DB::table('label_mail_class_details')->where('mail_class', $mailClass)->first();
                                    /*Domestic Label Data Code Start*/
                                    if($LabelMailClassDetails && $weightOz > $LabelMailClassDetails->max_weight_oz){
                                        $max_weight_oz = $LabelMailClassDetails->max_weight_oz;
                                        $response = [
                                        'requestId' => strtolower(Str::random(30)),
                                        'Status' => 'Maximum Weight Allowed '.$max_weight_oz,
                                        'statusCode'    => 204,
                                        ];
                                        return response()->json($response);
                                    } else {
                                        $zoneData = json_decode(file_get_contents('https://www.easypost.com/api/v2/usps_zones?from='.$senderpostalCode.'&to='.$recipientpostalCode));
                                        $DomesticRateData = DB::table('domestic_rates')->where('mail_class', $mailClass)->where('weight_lb', $weightLb)->where('zone', $zoneData->usps_zone)->where('is_apo_fpo', $is_apo_fpo)->first();
                                        if($DomesticRateData==''){
                                         $DomesticRateData = DB::table('domestic_rates')->where('mail_class', $mailClass)->where('weight_lb', $weightLb)->where('zone', $zoneData->usps_zone)->where('is_apo_fpo', '0')->first();   
                                        }

                                        if($DomesticRateData){
                                            $CommissionPrice = ($DomesticRateData->price * $this->commission) / 100;
                                            $CommissionPrice = number_format((float)$CommissionPrice, 2, '.', '');
                                            $ActualPrice = $DomesticRateData->price + $CommissionPrice;
                                            if($insuredValue!=''){
                                            $ActualPrice = $ActualPrice + '1.90';
                                            }

                                            $addressVerify = Address::create(array(
                                                "name"    => $recipientfirstname.' '.$recipientlastname,
                                                "street1" => $recipientstreetAddress,
                                                "street2" => "",
                                                "city"    => $recipientcity,
                                                "state"   => $recipientstate,
                                                "zip"     => $recipientpostalCode,
                                                "country" => "US",
                                                "verify"  =>  array("delivery"),
                                                )
                                            );
                                            $verifications = $addressVerify->verifications;
                                            $delivery = $verifications->delivery;
                                            $errorMessage = '';
                                            if($delivery->success!='1'){
                                                foreach($delivery->errors as $deliveryError ){
                                                    $errorMessage .= $deliveryError->message.'. ';
                                                }
                                                $response = [
                                                'requestId' => strtolower(Str::random(30)),
                                                'statusCode' => 201,
                                                'message'    => $errorMessage,
                                                ];
                                                return response()->json($response); 
                                            } else {
                                                
                                                $shipment = Shipment::create(array(
                                                'to_address' => array(
                                                "name"    => $recipientfirstname.' '.$recipientlastname,
                                                "street1" => $recipientstreetAddress,
                                                "street2" => "",
                                                "city"    => $recipientcity,
                                                "state"   => $recipientstate,
                                                "zip"     => $recipientpostalCode,
                                                "country" => "US",
                                                "company" => $recipientcompany,
                                                "phone"   => $recipientphone
                                                ),
                                                'from_address' => array(
                                                "name"    => $senderfirstname.' '.$senderlastname,
                                                "street1" => $senderstreetAddress,
                                                "street2" => "",
                                                "city"    => $sendercity,
                                                "state"   => $senderstate,
                                                "zip"     => $senderpostalCode,
                                                "country" => "US",
                                                "company" => $sendercompany,
                                                "phone"   => $senderphone
                                                ),
                                                'parcel' => array(
                                                'length' => $length,
                                                'width' => $width,
                                                'height' => $height,
                                                'weight' => $weightOz
                                                )
                                                ));

                                                if(!file_exists('public/upload/labels/'.date('Y-m-d'))){
                                                   $result = mkdir(('public/upload/labels/'.date('Y-m-d')), 0777, true);
                                                }

                                                $Shipmentdata = Shipment::retrieve($shipment->id);
                                                $ShippingRates = Rate::retrieve($shipment->lowest_rate("USPS"));
                                                $shipment->buy($shipment->lowest_rate("USPS"));
                                                $Labeldata = $shipment->insure(array('amount' => $ActualPrice));
                                                $labelRates =  $Labeldata->rates;
                                                $postageLabel =  $Labeldata->postage_label;
                                                $filename = 'public/upload/labels/'.date('Y-m-d');
                                                $labelurlArr = explode('/', $postageLabel->label_url);
                                                $ch = curl_init($postageLabel->label_url);
                                                $fp = fopen($filename.'/'.end($labelurlArr), 'wb');
                                                curl_setopt($ch, CURLOPT_FILE, $fp);
                                                curl_setopt($ch, CURLOPT_HEADER, 0);
                                                curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                                                curl_exec($ch);
                                                curl_close($ch);
                                                fclose($fp);
                                                $filename2 = '/upload/labels/'.date('Y-m-d');
                                                $fileurl = $filename2.'/'.end($labelurlArr);

                                                $newlabel = DB::table('labels')
                                                ->insertGetId(['user_id' => $auth_token->user_id, 'created' => $created, 'request_label' => '', 'response_label' => '', 'request_calculate' => '', 'response_calculate' => '', 'is_international' => '0', 'is_apo_fpo' => $is_apo_fpo, 'tracking_number' => $Labeldata->tracking_code, 'limit_print' => '0', 'is_test_label' => '1', 'to_postal_code' => $recipientpostalCode, 'to_first_name' => $recipientfirstname, 'to_last_name' => $recipientlastname, 'to_city' => $recipientcity, 'to_country' => '233', 'mail_class_key' => $mailClass, 'mail_class_key_full' => $mailClass, 'api_request' => '', 'service_value' => $labelRates[0]->rate, 'provider' => 'easypost', 'provider_transaction_id' => $Labeldata->id, 'transaction_id' => $labelRates[0]->id, 'total_value' => $Labeldata->insurance, 'status' => '0', 'is_dropship' => '0', 'post_data' => '', 'reject_message' => '', 'insured_value' => $insuredValue, 'is_metro' => '0', 'is_dimensional_weight' => '0', 'provider_account' => '', 'with_instructions' => '0']);


                                                if($newlabel){
                                                    $newlabelimg = DB::table('label_images')
                                                    ->insertGetId(['file_path' => $fileurl, 'label_id' => $newlabel]);
                                                    if($newlabelimg){
                                                        $response = [
                                                        'id' => $newlabel,
                                                        'value'=> $Labeldata->insurance,
                                                        'trackNumber'=> $Labeldata->tracking_code,
                                                        'images'=> array(url('/public/').$fileurl),
                                                        'Status' => 'Created',
                                                        'statusCode'    => 201,
                                                        'requestId' => strtolower(Str::random(30)),
                                                        ];
                                                        return response()->json($response);
                                                    } else {
                                                        $response = [
                                                        'id' => $newlabel,
                                                        'value'=> $Labeldata->insurance,
                                                        'trackNumber'=> $Labeldata->tracking_code,
                                                        'images'=> array(),
                                                        'Status' => 'Created',
                                                        'statusCode'    => 201,
                                                        'requestId' => strtolower(Str::random(30)),
                                                        ];
                                                        return response()->json($response);
                                                    }
                                                    
                                                } else {
                                                    $response = [
                                                    'requestId' => strtolower(Str::random(30)),
                                                    'Status' => 'Error',
                                                    'statusCode'    => 403,
                                                    ];
                                                    return response()->json($response);
                                                }
                                            }
                                        } else {
                                            $response = [
                                            'requestId' => strtolower(Str::random(30)),
                                            'Status' => 'Maximum Weight Allowed '.$LabelMailClassDetails->max_weight_oz,
                                            'statusCode'    => 204,
                                            ];
                                            return response()->json($response); 
                                        }
                                    }
                                    /*Domestic Label Data Code End*/
                                } else {
                                    $LabelMailClassDetails = DB::table('label_mail_class_details')->where('mail_class', $mailClass)->where('country_id', $countryId)->first();
                                    /*International Label Data Code Start*/
                                    if($LabelMailClassDetails && $weightOz > $LabelMailClassDetails->max_weight_oz){
                                        $max_weight_oz = $LabelMailClassDetails->max_weight_oz;
                                        $response = [
                                        'requestId' => strtolower(Str::random(30)),
                                        'Status' => 'Maximum Weight Allowed '.$max_weight_oz,
                                        'statusCode'    => 204,
                                        ];
                                        return response()->json($response);
                                    } else {
                                        $settingDetails = DB::table('settings')->where('id', '1')->first();
                                        $countryDetails = DB::table('countries')->where('idCountry', $countryId)->first();
                                        $provider_accounts = $settingDetails->provider_accounts;
                                        $provideraccounts = json_decode($provider_accounts);
                                        $EasypostDetails = $provideraccounts->Easypost_Default;
                                        $EasypostDetails->api_key;
                                        $request_params = array(
                                        "address[street1]" => $recipientstreetAddress, 
                                        "address[street2]" => '',
                                        "address[city]" => $recipientcity,
                                        "address[state]" => $recipientstate,
                                        "address[zip]" => $recipientpostalCode,
                                        "address[country]" => $countryDetails->countryCode,
                                        "address[company]" => $recipientcompany,
                                        "address[phone]" => $recipientphone, 
                                        "verify[]" => 'delivery',
                                        );
                                        $nvp_string = '';
                                        foreach($request_params as $var=>$val){
                                            $nvp_string .= '&'.$var.'='.urlencode($val);    
                                        }
                                        $nvp_string = substr($nvp_string, 1);
                                        $ch = curl_init();
                                        curl_setopt($ch, CURLOPT_URL, "https://api.easypost.com/v2/addresses");
                                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                                        curl_setopt($ch, CURLOPT_POSTFIELDS, $nvp_string);
                                        curl_setopt($ch, CURLOPT_POST, 1);
                                        curl_setopt($ch, CURLOPT_USERPWD, $EasypostDetails->api_key . ":" . "");
                                        $headers = array();
                                        $headers[] = "Content-Type: application/x-www-form-urlencoded";
                                        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                                        $address_verify = curl_exec($ch);
                                        $addressVerify = json_decode($address_verify);
                                        curl_close ($ch);
                                        $verifications = $addressVerify->verifications;
                                        $delivery = $verifications->delivery;
                                        $errorMessage = '';
                                        if($delivery->success!='1'){
                                            foreach($delivery->errors as $deliveryError ){
                                                $errorMessage .= $deliveryError->message.'. ';
                                            }
                                            $response = [
                                            'requestId' => strtolower(Str::random(30)),
                                            'statusCode' => 201,
                                            'message'    => $errorMessage,
                                            ];
                                            return response()->json($response); 
                                        } else {
                                            echo '<pre>';
                                            print_r($delivery);
                                            echo '</pre>';  
                                        }
                                    }
                                    /*International Label Data Code End*/
                                }
                            }
                        } else {
                            $response = [
                            'requestId' => strtolower(Str::random(30)),
                            'statusCode' => 403,
                            'message'    => "You don't have permission to access.",
                            ];
                            return response()->json($response); 
                        }
                    } else {
                        $response = [
                        'requestId' => strtolower(Str::random(30)),
                        'message' => 'Token is locked.',
                        'statusCode'    => 403,
                        ];
                        return response()->json($response);
                    }
                }
            } else {
                $response = [
                'requestId' => strtolower(Str::random(30)),
                'statusCode'    => 405,
                'error' => 'The requested URL '.$uri.' was not found on this server.',
                ];
                return response()->json($response);
            }
        } else {
            $response = [
            'requestId' => strtolower(Str::random(30)),
            'statusCode' => 400,
            'error'    => 'Required parameter not passed: authToken',
            ];
            return response()->json($response);
        }
    }



}
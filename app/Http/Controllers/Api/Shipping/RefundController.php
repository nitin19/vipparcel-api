<?php

namespace App\Http\Controllers\Api\Shipping;

use App\User;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Api\APIBaseController as APIBaseController;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Str;
use App\Balance;
use Auth;
use DB;
use Exception;
use Log;
use Session;
use Hash;
use Image;
use Carbon;

class RefundController extends APIBaseController
{

    public $successStatus = 200;

    public function __construct()
    {
       //
    }

    public function get_getList(Request $request)
    {   
        try {
            $token = trim($request->authToken);
            $uri = $request->path();
            $fullUrl = $request->fullUrl(); 
            $records = array();
            if($token) {
                if($uri == 'api/v1/shipping/refund/getList') {
                    $auth_token = DB::table('user_api')->where('auth_token', $token)->first();
                    if (is_null($auth_token)) { 
                        $response = [
                        'requestId' => strtolower(Str::random(30)),
                        'message'    => 'Token does not exist.',
                        'statusCode' => 401,
                        ];
                        return response()->json($response);
                    } else {
                        if($auth_token->active == 1) {
                        $user = DB::table('users')->where('id', $auth_token->user_id)->where('in_archive', 0)->first();
                            if ($user) {
                                $limit = trim($request->limit);
                                if($limit) {
                                    if($limit <= 150) {
                                        $limit = trim($request->limit);
                                    } else {
                                        $response = [
                                            'requestId' => strtolower(Str::random(30)),
                                            'statusCode' => 400,
                                            'error'    => "Invalid parameter: limit",
                                           ];
                                        return response()->json($response);   
                                    }
                                } else {
                                    $limit = 30;
                                }

                                $offset = trim($request->offset);
                                if($offset) {
                                   $offset = trim($request->offset);
                                } else {
                                   $offset = 0; 
                                }
                                
                                $orderByArr = array('id', 'created', 'updated');
                                if (is_array($request->orderBy)) {
                                   foreach($request->orderBy as $Key=>$Value) {
                                     break;
                                   }

                                  if($Key) {
                                    if(in_array(trim($Key), $orderByArr)) {
                                    if(($Value!='') && ( $Value == 'asc' || $Value == 'ASC' || $Value == 'desc' || $Value == 'DESC' )){
                                           $sortKey = $Key;
                                           $sortValue = $Value;
                                       } else {
                                        $response = [
                                            'requestId' => strtolower(Str::random(30)),
                                            'statusCode' => 400,
                                            'error'    => "Invalid parameter: orderBy",
                                           ];
                                        return response()->json($response);
                                       }
                                    } else {
                                    $response = [
                                            'requestId' => strtolower(Str::random(30)),
                                            'statusCode' => 400,
                                            'error'    => "Field \"$Key\" is not sortable",
                                           ];
                                        return response()->json($response);
                                   }
                                 } else {
                                   $response = [
                                            'requestId' => strtolower(Str::random(30)),
                                            'statusCode' => 400,
                                            'error'    => "Field \"0\" is not sortable",
                                           ];
                                        return response()->json($response);
                                  } 

                                } else {

                                $orderBy = trim($request->orderBy);
                                if($orderBy) {
                                   if(in_array(trim($request->orderBy), $orderByArr)) {
                                     switch ($orderBy) {
                                        case 'id':
                                            $sortKey = 'id';
                                            $sortValue = 'asc';
                                            break;
                                        case 'created':
                                            $sortKey = 'created';
                                            $sortValue = 'asc';
                                            break;
                                        case 'updated':
                                            $sortKey = 'updated';
                                            $sortValue = 'asc';
                                            break;
                                        } 
                                   } else {
                                    $response = [
                                            'requestId' => strtolower(Str::random(30)),
                                            'statusCode' => 400,
                                            'error'    => "Field \"$orderBy\" is not sortable",
                                           ];
                                   return response()->json($response);
                                   }
                                } else {
                                 if (strpos($fullUrl,'orderBy') !== false) {
                                      $response = [
                                            'requestId' => strtolower(Str::random(30)),
                                            'statusCode' => 400,
                                            'error'    => "Invalid parameter: orderBy",
                                           ];
                                        return response()->json($response);
                                     } else {
                                      $sortKey = 'id';
                                      $sortValue = 'asc';
                                    }
                                  }     
                            }

                            $user_refunds_all = DB::table('refunds')->where('user_id', '=', $auth_token->user_id)->where('is_test', 1)->where('is_auto_refund', 0)->get();

                            $user_refunds = DB::table('refunds')->where('user_id', '=', $auth_token->user_id)->where('is_test', 1)->where('is_auto_refund', 0)->orderBy($sortKey, $sortValue)->offset($offset)->limit($limit)->get(); 
                           // $refunds = $user_refunds->toArray();
                               
                                if(count($user_refunds_all) > 0) {

                                    foreach($user_refunds as $key => $refund) {

                                        switch ($refund->status) {
                                        case '0':
                                            $refundStatus = 'Pending';
                                            break;
                                        case '1':
                                            $refundStatus = 'USPS Declined';
                                            break;
                                        case '2':
                                            $refundStatus = 'USPS Approved';
                                            break;
                                        } 
                         
                            $labels = DB::table('labels')->join('refund_labels', 'refund_labels.label_id', '=', 'labels.id')->where('refund_labels.refund_id',$refund->id)->orderBy('labels.id','DESC')->get();
                            $total = 0;
                                if( count($labels) > 0 ) {
                                foreach($labels as $label) {
                                    if (!$label->is_test_label) {
                                         $amount = DB::table('payments')->where('id', '=', $label->payment_id)->sum('amount');
                                         $total+=$amount;
                                         } else {
                                         $total+=$label->total_value;
                                        }
                                    }
                                } 

                                $records[$key] = array(
                                            'id'           => (int) $refund->id,
                                            'refundStatus' => $refundStatus,
                                            'countItems'   => (int) count($labels),
                                            'reason'       => $refund->reason,
                                            'comment'      => $refund->comment,
                                            'totalValue'   => round($total, 2),
                                            'created'      => $refund->created,
                                            'updated'      => $refund->updated,
                                        );

                                $records['labels'] = array();
                                    if( count($labels) > 0 ) {
                                        foreach($labels as $label) {   
                                        if (!$label->is_test_label) {
                                         $amount = DB::table('payments')->where('id', '=', $label->payment_id)->sum('amount');
                                         } else {
                                         $amount = $label->total_value;
                                        }
                                            $records[$key]['labels'][] = array(
                                                'id'          => (int) $label->id,
                                                'value'       => round($amount, 2),
                                                'trackNumber' => $label->tracking_number
                                            );
                                        }
                                    } else {
                                       $records[$key]['labels'] = array(); 
                                    }   
                                }

                                   $response = [
                                    'requestId' => strtolower(Str::random(30)),
                                    'totalRecords'    => (int) count($user_refunds_all),
                                    'records'    => $records,
                                    'statusCode' => $this->successStatus,
                                   ];
                                   return response()->json($response);

                                } else {
                                   $response = [
                                    'requestId' => strtolower(Str::random(30)),
                                    'totalRecords'    => (int) count($user_refunds_all),
                                   // 'message'    => 'Record does not exist.',
                                    'statusCode' => 200, //401,
                                   ];
                                   return response()->json($response);
                                }

                                $api_statistics = DB::table('user_api_statistics')->where('user_id', $auth_token->user_id)->where('resource_path', $request->path())->first();

                                if (is_null($api_statistics)) { 
                                   $insId = DB::table('user_api_statistics')->insertGetId(
                                     ['user_id' => $auth_token->user_id, 'resource_path' => $request->path(), 'count_requests' => 1, 'created' => date('Y-m-d H:i:s')]
                                    );
                                } else {
                                    DB::table('user_api_statistics')->where('id', $api_statistics->id)->update(['count_requests' => $api_statistics->count_requests + 1, 'updated' => date('Y-m-d H:i:s')]);
                                }
                               
                              } else {
                                $response = [
                                'requestId' => strtolower(Str::random(30)),
                                'statusCode' => 403,
                                'message'    => "You don't have permission to access.",
                               ];
                               return response()->json($response);
                            }
                         } else {
                            $response = [
                            'requestId' => strtolower(Str::random(30)),
                            'message' => 'Token is locked.',
                            'statusCode'    => 403,
                            ];
                            return response()->json($response);
                        }
                    }  
                } else {
                        $response = [
                        'requestId' => strtolower(Str::random(30)),
                        'statusCode'    => 405,
                        'error' => 'The requested URL '. $uri .'was not found on this server.',
                        ];
                        return response()->json($response);
                }
            } else {
                $response = [
                    'requestId' => strtolower(Str::random(30)),
                    'statusCode' => 400,
                    'error'    => 'Required parameter not passed: authToken',
                ];
                return response()->json($response);
            }
           // return response()->json($response);
        } catch (\Exception $e) {
            return $e->getMessage();
        }    
    }

    public function get_getInfo(Request $request, $id)
    {   
        try {
            $token = trim($request->authToken);
            $uri = $request->path(); 
            $records = array();
            if($token) {
                if($uri == 'api/v1/shipping/refund/getInfo/'.$id) {
                    $auth_token = DB::table('user_api')->where('auth_token', $token)->first();
                    if (is_null($auth_token)) { 
                        $response = [
                        'requestId' => strtolower(Str::random(30)),
                        'message'    => 'Token does not exist.',
                        'statusCode' => 401,
                        ];
                        return response()->json($response);
                    } else {
                        if($auth_token->active == 1) {
                        $user = DB::table('users')->where('id', $auth_token->user_id)->where('in_archive', 0)->first();
                            if ($user) {
                        $refund = DB::table('refunds')->where('id', $id)->where('user_id', '=', $auth_token->user_id)->where('is_test', 1)->where('is_auto_refund', 0)->first();
                            if ($refund) {

                              switch ($refund->status) {
                                        case '0':
                                            $refundStatus = 'Pending';
                                            break;
                                        case '1':
                                            $refundStatus = 'USPS Declined';
                                            break;
                                        case '2':
                                            $refundStatus = 'USPS Approved';
                                            break;
                                        } 

                              $labels = DB::table('labels')->join('refund_labels', 'refund_labels.label_id', '=', 'labels.id')->where('refund_labels.refund_id',$id)->orderBy('labels.id','DESC')->get();

                              $total = 0;

                              if( count($labels) > 0 ) {
                               foreach($labels as $label) {
                                if (!$label->is_test_label) {
                                         $amount = DB::table('payments')->where('id', '=', $label->payment_id)->sum('amount');
                                         $total+=$amount;
                                         } else {
                                         $total+=$label->total_value;
                                        }
                                    }
                                } 

                              $records = array(
                                            'requestId'    => strtolower(Str::random(30)),
                                            'id'           => (int) $refund->id,
                                            'refundStatus' => $refundStatus,
                                            'countItems'   => (int) count($labels),
                                            'reason'       => $refund->reason,
                                            'comment'      => $refund->comment,
                                            'totalValue'   => round($total, 2),
                                            'created'      => $refund->created,
                                            'updated'      => $refund->updated,
                                        );
                              
                              $records['labels'] = array();
                                    if( count($labels) > 0 ) {
                                        foreach($labels as $label) { 
                                        if (!$label->is_test_label) {
                                         $amount = DB::table('payments')->where('id', '=', $label->payment_id)->sum('amount');
                                         } else {
                                         $amount = $label->total_value;
                                        }  
                                            $records['labels'][] = array(
                                                'id'          => (int) $label->id,
                                                'value'       => round($amount, 2),
                                                'trackNumber' => $label->tracking_number
                                            );
                                        }
                                    } else {
                                       $records['labels'] = array(); 
                                    }        

                              $records['statusCode'] = $this->successStatus;
                              $response = $records;
                              return response()->json($response);
                                    
                                  } else {
                                    $response = [
                                    'requestId' => strtolower(Str::random(30)),
                                    'message'    => 'Record does not exist.',
                                    'statusCode' => 401,
                                   ];
                                   return response()->json($response);
                                }   

                               $api_statistics = DB::table('user_api_statistics')->where('user_id', $auth_token->user_id)->where('resource_path', $request->path())->first();

                                if (is_null($api_statistics)) { 
                                   $insId = DB::table('user_api_statistics')->insertGetId(
                                     ['user_id' => $auth_token->user_id, 'resource_path' => $request->path(), 'count_requests' => 1, 'created' => date('Y-m-d H:i:s')]
                                    );
                                } else {
                                    DB::table('user_api_statistics')->where('id', $api_statistics->id)->update(['count_requests' => $api_statistics->count_requests + 1, 'updated' => date('Y-m-d H:i:s')]);
                                }
                              } else {
                                $response = [
                                'requestId' => strtolower(Str::random(30)),
                                'statusCode' => 403,
                                'message'    => "You don't have permission to access.",
                               ];
                               return response()->json($response);
                            }
                        } else {
                            $response = [
                            'requestId' => strtolower(Str::random(30)),
                            'message' => 'Token is locked.',
                            'statusCode'    => 403,
                            ];
                            return response()->json($response);
                        }
                    }  
                } else {
                        $response = [
                        'requestId' => strtolower(Str::random(30)),
                        'statusCode'    => 405,
                        'error' => 'The requested URL '. $uri .'was not found on this server.',
                        ];
                        return response()->json($response);
                }
            } else {
                $response = [
                    'requestId' => strtolower(Str::random(30)),
                    'statusCode' => 400,
                    'error'    => 'Required parameter not passed: authToken',
                ];
                return response()->json($response);
            }
          //  return response()->json($response);
        } catch (\Exception $e) {
            return $e->getMessage();
        }    
    }


    public function get_getLabels(Request $request)
    {   
        try {
            $token = trim($request->authToken);
            $uri = $request->path();
            $fullUrl = $request->fullUrl(); 
            $records = array();
            if($token) {
                if($uri == 'api/v1/shipping/refund/getLabels') {
                    $auth_token = DB::table('user_api')->where('auth_token', $token)->first();
                    if (is_null($auth_token)) { 
                        $response = [
                        'requestId' => strtolower(Str::random(30)),
                        'message'    => 'Token does not exist.',
                        'statusCode' => 401,
                        ];
                        return response()->json($response);
                    } else {
                        if($auth_token->active == 1) {
                        $user = DB::table('users')->where('id', $auth_token->user_id)->where('in_archive', 0)->first();
                            if ($user) {
                                $limit = trim($request->limit);
                                if($limit) {
                                    if($limit <= 150) {
                                        $limit = trim($request->limit);
                                    } else {
                                        $response = [
                                            'requestId' => strtolower(Str::random(30)),
                                            'statusCode' => 400,
                                            'error'    => "Invalid parameter: limit",
                                           ];
                                        return response()->json($response);   
                                    }
                                } else {
                                    $limit = 30;
                                }

                                $offset = trim($request->offset);
                                if($offset) {
                                   $offset = trim($request->offset);
                                } else {
                                   $offset = 0; 
                                }
                                
                                $orderByArr = array('id', 'created', 'updated');
                                if (is_array($request->orderBy)) {
                                   foreach($request->orderBy as $Key=>$Value) {
                                     break;
                                   }

                                  if($Key) {
                                    if(in_array(trim($Key), $orderByArr)) {
                                    if(($Value!='') && ( $Value == 'asc' || $Value == 'ASC' || $Value == 'desc' || $Value == 'DESC' )){
                                           $sortKey = $Key;
                                           $sortValue = $Value;
                                       } else {
                                        $response = [
                                            'requestId' => strtolower(Str::random(30)),
                                            'statusCode' => 400,
                                            'error'    => "Invalid parameter: orderBy",
                                           ];
                                        return response()->json($response);
                                       }
                                    } else {
                                    $response = [
                                            'requestId' => strtolower(Str::random(30)),
                                            'statusCode' => 400,
                                            'error'    => "Field \"$Key\" is not sortable",
                                           ];
                                        return response()->json($response);
                                   }
                                 } else {
                                   $response = [
                                            'requestId' => strtolower(Str::random(30)),
                                            'statusCode' => 400,
                                            'error'    => "Field \"0\" is not sortable",
                                           ];
                                        return response()->json($response);
                                  } 

                                } else {

                                $orderBy = trim($request->orderBy);
                                if($orderBy) {
                                   if(in_array(trim($request->orderBy), $orderByArr)) {
                                     switch ($orderBy) {
                                        case 'id':
                                            $sortKey = 'id';
                                            $sortValue = 'asc';
                                            break;
                                        case 'created':
                                            $sortKey = 'created';
                                            $sortValue = 'asc';
                                            break;
                                        case 'updated':
                                            $sortKey = 'updated';
                                            $sortValue = 'asc';
                                            break;
                                        } 
                                   } else {
                                    $response = [
                                            'requestId' => strtolower(Str::random(30)),
                                            'statusCode' => 400,
                                            'error'    => "Field \"$orderBy\" is not sortable",
                                           ];
                                   return response()->json($response);
                                   }
                                } else {
                                 if (strpos($fullUrl,'orderBy') !== false) {
                                      $response = [
                                            'requestId' => strtolower(Str::random(30)),
                                            'statusCode' => 400,
                                            'error'    => "Invalid parameter: orderBy",
                                           ];
                                        return response()->json($response);
                                     } else {
                                      $sortKey = 'id';
                                      $sortValue = 'asc';
                                    }
                                  }     
                            }

                            $date = \Carbon\Carbon::today()->subDays(29);

                            $refund_labels_all = DB::table('labels')->where('user_id', '=', $auth_token->user_id)->where('is_test_label', 1)->where('created', '>=', $date)->whereNull('refund_id')->get();

                            $refund_labels = DB::table('labels')->where('user_id', '=', $auth_token->user_id)->where('is_test_label', 1)->where('created', '>=', $date)->whereNull('refund_id')->orderBy($sortKey, $sortValue)->offset($offset)->limit($limit)->get(); 
                           // $refunds = $refund_labels->toArray();
                               
                                if(count($refund_labels_all) > 0) {

                                foreach($refund_labels as $label) {
                                    if (!$label->is_test_label) {
                                         $amount = DB::table('payments')->where('id', '=', $label->payment_id)->sum('amount'); 
                                         } else {
                                         $amount = $label->total_value;
                                        }
                                    $records[] = array(
                                            'id'          => (int) $label->id,
                                            'value'       => round($amount, 2),
                                            'trackNumber' => $label->tracking_number,
                                            'created'     => $label->created,
                                            'updated'     => $label->updated
                                        );   
                                }

                                   $response = [
                                    'requestId' => strtolower(Str::random(30)),
                                    'countItems'    => (int) count($refund_labels_all),
                                    'records'    => $records,
                                    'statusCode' => $this->successStatus,
                                   ];
                                   return response()->json($response);

                                } else {
                                   $response = [
                                    'requestId' => strtolower(Str::random(30)),
                                    'totalRecords'    => (int) count($refund_labels_all),
                                   // 'message'    => 'Record does not exist.',
                                    'statusCode' => 200, //401,
                                   ];
                                   return response()->json($response);
                                }

                                $api_statistics = DB::table('user_api_statistics')->where('user_id', $auth_token->user_id)->where('resource_path', $request->path())->first();

                                if (is_null($api_statistics)) { 
                                   $insId = DB::table('user_api_statistics')->insertGetId(
                                     ['user_id' => $auth_token->user_id, 'resource_path' => $request->path(), 'count_requests' => 1, 'created' => date('Y-m-d H:i:s')]
                                    );
                                } else {
                                    DB::table('user_api_statistics')->where('id', $api_statistics->id)->update(['count_requests' => $api_statistics->count_requests + 1, 'updated' => date('Y-m-d H:i:s')]);
                                }
                               
                              } else {
                                $response = [
                                'requestId' => strtolower(Str::random(30)),
                                'statusCode' => 403,
                                'message'    => "You don't have permission to access.",
                               ];
                               return response()->json($response);
                            }
                         } else {
                            $response = [
                            'requestId' => strtolower(Str::random(30)),
                            'message' => 'Token is locked.',
                            'statusCode'    => 403,
                            ];
                            return response()->json($response);
                        }
                    }  
                } else {
                        $response = [
                        'requestId' => strtolower(Str::random(30)),
                        'statusCode'    => 405,
                        'error' => 'The requested URL '. $uri .'was not found on this server.',
                        ];
                        return response()->json($response);
                }
            } else {
                $response = [
                    'requestId' => strtolower(Str::random(30)),
                    'statusCode' => 400,
                    'error'    => 'Required parameter not passed: authToken',
                ];
                return response()->json($response);
            }
           // return response()->json($response);
        } catch (\Exception $e) {
            return $e->getMessage();
        }    
    }

    public function post_request(Request $request)
    {   
        try {
            $token = trim($request->authToken);
            $uri = $request->path();
            $fullUrl = $request->fullUrl(); 
            $records = array();
            if($token) {
                if($uri == 'api/v1/shipping/refund/request') {
                    $auth_token = DB::table('user_api')->where('auth_token', $token)->first();
                    if (is_null($auth_token)) { 
                        $response = [
                        'requestId' => strtolower(Str::random(30)),
                        'message'    => 'Token does not exist.',
                        'statusCode' => 401,
                        ];
                        return response()->json($response);
                    } else {
                        if($auth_token->active == 1) {
                        $user = DB::table('users')->where('id', $auth_token->user_id)->where('in_archive', 0)->first();
                            if ($user) {

                                $labels = $request->refundLabels;

                                $reason = trim($request->reason);

                                if (empty($labels) OR ! is_array($labels))
                                {
                                  $response = [
                                            'requestId' => strtolower(Str::random(30)),
                                            'statusCode' => 400,
                                            'error'    => "Invalid parameter: refundLabels",
                                           ];
                                        return response()->json($response);
                                }

                                if (empty($reason))
                                {
                                  $response = [
                                            'requestId' => strtolower(Str::random(30)),
                                            'statusCode' => 400,
                                            'error'    => "Invalid parameter: reason",
                                           ];
                                        return response()->json($response);
                                }

                                $labels = array_unique($labels);
                                $refund_ids = array();
                                $ins_refund_ids = array();
                                $refund_invalid_ids = array();

                                $date = \Carbon\Carbon::today()->subDays(29);

                                $user_refunds = DB::table('labels')->where('user_id', '=', $auth_token->user_id)->whereIn('id', $labels)->where('is_test_label', 1)->where('created', '>=', $date)->whereNull('refund_id')->get(); 
                                
                                if( count($user_refunds) > 0 ) {
                                  
                                  foreach($user_refunds as $user_refund) {
                                      $refund_ids[] = $user_refund->id;
                                  }

                                  $refund_invalid_ids = array_diff($labels,$refund_ids);

                                  if( count($refund_invalid_ids) > 0 ) {
                                      $response = [
                                            'requestId' => strtolower(Str::random(30)),
                                            'statusCode' => 400,
                                            'error'    => "Invalid labels list, please make sure that every label in list is not older than 30 days. Invalid ID's: ".implode(', ', $refund_invalid_ids),
                                           ];
                                        return response()->json($response);
                                    } else {

                                  foreach($user_refunds as $user_refund) {
                                   $refund_ids[] = $user_refund->id;
                                   $lastinsId = DB::table('refunds')->insertGetId(
                                            ['user_id' => $auth_token->user_id, 'status' => 0, 'created' => date('Y-m-d H:i:s'), 'updated' => date('Y-m-d H:i:s'), 'comment' => '', 'reason' => $reason, 'is_test' => 1, 'request_date' => date('Y-m-d H:i:s')]
                                          );

                                    $ins_refund_ids[] = $lastinsId;

                                    DB::table('refund_labels')->insert(
                                            ['label_id' => $user_refund->id, 'refund_id' => $lastinsId]
                                          );
                                    
                                    $lastinsrow = DB::table('refunds')->where('id', $lastinsId)->first();
                                    if(!$lastinsrow->is_test) {
                                      DB::table('labels')->where('id', $user_refund->id)->update(['updated' => date('Y-m-d H:i:s'), 'refund_id' => $lastinsId]);
                                    }
                                  }

                                  $response = [
                                    'requestId' => strtolower(Str::random(30)),
                                    'statusCode' => 201,
                                    'id'    => $ins_refund_ids
                                   ];
                                   return response()->json($response);

                                  }

                                } else {
                                      $response = [
                                            'requestId' => strtolower(Str::random(30)),
                                            'statusCode' => 400,
                                            'error'    => "Invalid labels list, please make sure that every label in list is not older than 30 days. Invalid ID's: ".implode(', ', $refund_invalid_ids),
                                           ];
                                        return response()->json($response);
                                  }
                               
                                $api_statistics = DB::table('user_api_statistics')->where('user_id', $auth_token->user_id)->where('resource_path', $request->path())->first();

                                if (is_null($api_statistics)) { 
                                   $insId = DB::table('user_api_statistics')->insertGetId(
                                     ['user_id' => $auth_token->user_id, 'resource_path' => $request->path(), 'count_requests' => 1, 'created' => date('Y-m-d H:i:s')]
                                    );
                                } else {
                                    DB::table('user_api_statistics')->where('id', $api_statistics->id)->update(['count_requests' => $api_statistics->count_requests + 1, 'updated' => date('Y-m-d H:i:s')]);
                                }
                               
                              } else {
                                $response = [
                                'requestId' => strtolower(Str::random(30)),
                                'statusCode' => 403,
                                'message'    => "You don't have permission to access.",
                               ];
                               return response()->json($response);
                            }
                         } else {
                            $response = [
                            'requestId' => strtolower(Str::random(30)),
                            'message' => 'Token is locked.',
                            'statusCode'    => 403,
                            ];
                            return response()->json($response);
                        }
                    }  
                } else {
                        $response = [
                        'requestId' => strtolower(Str::random(30)),
                        'statusCode'    => 405,
                        'error' => 'The requested URL '. $uri .'was not found on this server.',
                        ];
                        return response()->json($response);
                }
            } else {
                $response = [
                    'requestId' => strtolower(Str::random(30)),
                    'statusCode' => 400,
                    'error'    => 'Required parameter not passed: authToken',
                ];
                return response()->json($response);
            }
           // return response()->json($response);
        } catch (\Exception $e) {
            return $e->getMessage();
        }    
    }
   


 
}
<?php

namespace App\Http\Controllers\Api\Shipping;

use App\Models\User;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Api\APIBaseController as APIBaseController;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Str;
use App\Models\Balance;
use Auth;
use DB;
use Exception;
use Log;
use Session;
use Hash;
use Image;
use Carbon;

class ScanformController extends APIBaseController
{

    public $successStatus = 200;

    public function __construct()
    {
       //
    }

    public function get_getInfo(Request $request, $id)
    {   
        try {
            $token = trim($request->authToken);
            $uri = $request->path(); 
            $records = array();
            if($token) {
                if($uri == 'api/v1/shipping/scanForm/getInfo/'.$id) {
                    $auth_token = DB::table('user_api')->where('auth_token', $token)->first();
                    if (is_null($auth_token)) { 
                        $response = [
                        'requestId' => strtolower(Str::random(30)),
                        'message'    => 'Token does not exist.',
                        'statusCode' => 401,
                        ];
                        return response()->json($response);
                    } else {
                        if($auth_token->active == 1) {
                        $user = DB::table('users')->where('id', $auth_token->user_id)->where('in_archive', 0)->first();
                            if ($user) {
                        $scan = DB::table('scans')->where('id', $id)->where('is_test', 1)->where('user_id', $auth_token->user_id)->first();
                            if ($scan) {

                               $data = array();

                                $labels = DB::table('labels')->join('label_scans', 'label_scans.label_id', '=', 'labels.id')->where('label_scans.scan_id',$id)->orderBy('labels.id','DESC')->get();

                                     $zip4 = NULL;
                                     $postalCode =  $scan->zip;
                                     $postalCodeExp = explode('-', $postalCode);
                                    if (count($postalCodeExp) == 2)
                                    {
                                        $postalCode = $postalCodeExp[0];
                                        $zip4 = $postalCodeExp[1];
                                    } 

                                    $data['requestId'] = strtolower(Str::random(30));
                                    $data['id'] = (int) $id;
                                    $data['printImage'] = $scan->image;

                                    if(count($labels) > 0 ) {
                                        foreach($labels as $label) {
                                        $data['labels'][] = array(
                                            'id'  => (int) $label->id,
                                            'trackingNumber'=> $label->tracking_number,
                                            'created' => $label->created
                                        );
                                      }
                                    }

                                    $data['address'] = array(
                                        'firstName'  => $scan->first_name,
                                        'lastName'=> $scan->last_name,
                                        'city' => $scan->city,
                                        'state' => $scan->state,
                                        'postalCode'=> $postalCode,
                                        'address' => $scan->address,
                                        'zip4' => $zip4
                                    );

                                    

                                    $data['statusCode'] = $this->successStatus;
                                    $response = $data;
                                    return response()->json($response);
                                    
                                  } else {
                                    $response = [
                                    'requestId' => strtolower(Str::random(30)),
                                    'message'    => 'Record does not exist.',
                                    'statusCode' => 401,
                                   ];
                                   return response()->json($response);
                                }   

                               $api_statistics = DB::table('user_api_statistics')->where('user_id', $auth_token->user_id)->where('resource_path', $request->path())->first();

                                if (is_null($api_statistics)) { 
                                   $insId = DB::table('user_api_statistics')->insertGetId(
                                     ['user_id' => $auth_token->user_id, 'resource_path' => $request->path(), 'count_requests' => 1, 'created' => date('Y-m-d H:i:s')]
                                    );
                                } else {
                                    DB::table('user_api_statistics')->where('id', $api_statistics->id)->update(['count_requests' => $api_statistics->count_requests + 1, 'updated' => date('Y-m-d H:i:s')]);
                                }
                              } else {
                                $response = [
                                'requestId' => strtolower(Str::random(30)),
                                'statusCode' => 403,
                                'message'    => "You don't have permission to access.",
                               ];
                               return response()->json($response);
                            }
                        } else {
                            $response = [
                            'requestId' => strtolower(Str::random(30)),
                            'message' => 'Token is locked.',
                            'statusCode'    => 403,
                            ];
                            return response()->json($response);
                        }
                    }  
                } else {
                        $response = [
                        'requestId' => strtolower(Str::random(30)),
                        'statusCode'    => 405,
                        'error' => 'The requested URL '. $uri .'was not found on this server.',
                        ];
                        return response()->json($response);
                }
            } else {
                $response = [
                    'requestId' => strtolower(Str::random(30)),
                    'statusCode' => 400,
                    'error'    => 'Required parameter not passed: authToken',
                ];
                return response()->json($response);
            }
          //  return response()->json($response);
        } catch (\Exception $e) {
            return $e->getMessage();
        }    
    }


    public function post_create(Request $request)
    {   
        try {
            $token = trim($request->authToken);
            $uri = $request->path();
            $current_url = url()->current();
            $records = array();
            $is_test_request = '1';
            $final_scan_data = array();
            if($token) {
                if($uri == 'api/v1/shipping/scanForm/create') {
                    $auth_token = DB::table('user_api')->where('auth_token', $token)->first();
                    if (is_null($auth_token)) { 
                        $response = [
                        'requestId' => strtolower(Str::random(30)),
                        'message'    => 'Token does not exist.',
                        'statusCode' => 401,
                        ];
                        return response()->json($response);
                    } else {
                        if($auth_token->active == 1) {
                        $user = DB::table('users')->where('id', $auth_token->user_id)->where('in_archive', 0)->first();
                            if ($user) {
                                /*$labels = $request->labels;
                                $address_fields = array(
                                            'firstName' => $request->address['firstName'],
                                            'lastName' => $request->address['lastName'],
                                            'city' => $request->address['city'],
                                            'state' => $request->address['state'],
                                            'postalCode' => $request->address['postalCode'],
                                            'address' => $request->address['address'],
                                  );*/
                                $firstName = $request->firstName;
                                $lastName = $request->lastName;
                                $city = $request->city;
                                $state = $request->state;
                                $postalCode = $request->postalCode;
                                $address = $request->address;

                                $labels_in = $request->labels;
                                
                                if ( ! is_array($labels_in))
                                {
                                    $response = [
                                        'requestId' => strtolower(Str::random(30)),
                                        'statusCode' => 400,
                                        'error'    => 'Field "labels" must be an array',
                                    ];
                                    return response()->json($response);
                                }


                                if ($is_test_request=='1')
                                {
                                    $labels_out = DB::table('labels')
                                            ->where('user_id', '=', $auth_token->user_id)
                                            ->where('is_test_label', '=', 1)
                                            ->whereIn('id', $labels_in)
                                            ->where('scan_id','=',NULL)
                                            ->orderby('id', 'DESC')
                                            ->get();
                                }
                                else
                                {
                                    $labels_out = DB::table('labels')
                                            ->where('user_id', '=', $auth_token->user_id)
                                            ->where('is_test_label', '=', 0)
                                            ->whereIn('id', $labels_in)
                                            ->where('scan_id','=',NULL)
                                            ->where('refund_id','=',NULL)
                                            ->whereBetween('created',array(date("Y-m-d 21:01:00", strtotime('-1 day')), date("Y-m-d 21:00:00")))
                                            ->get();
                                }
                                if(count($labels_in) != $labels_out->count())
                                {
                                    $response = [
                                        'requestId' => strtolower(Str::random(30)),
                                        'statusCode' => 400,
                                        'error'    => 'The requested number of labels does not match the number of founded.',
                                    ];
                                    return response()->json($response);

                                }
                                /*$query_address = array(
                                            'firstName' => $request->address['firstName'],
                                            'lastName' => $request->address['lastName'],
                                            'city' => $request->address['city'],
                                            'state' => $request->address['state'],
                                            'postalCode' => $request->address['postalCode'],
                                            'address' => $request->address['address'],
                                  );
                                $provider_params = array(
                                            'first_name' => $request->address['firstName'],
                                            'last_name' => $request->address['lastName'],
                                            'FromName' => $request->address['firstName'].' '.$request->address['lastName'],
                                            'FromCity' => $request->address['city'],
                                            'FromState' => $request->address['state'],
                                            'FromZipCode' => $request->address['postalCode'],
                                            'FromAddress' => $request->address['address'],
                                );*/

                                /*$label_groups = [];
                                foreach ($labels_out as $label)
                                {
                                   
                                    $label_groups[$label->get_provider_alias()][] = $label;
                                }*/
                                 
                                //$filter_label = Provider_Filter::factory('Scan', NULL, $provider_params, $this->user);

                                if($auth_token->user_id) {
                                    $rules = array (
                                                //'address' => 'required|array|min:3',
                                                'address.firstName' => 'required|max:24|regex:/^[a-zA-Z0-9_\s]+$/',
                                                'address.lastName' => 'required|max:24|regex:/^[a-zA-Z0-9_\s]+$/',
                                                'address.address' => 'required|max:200|regex:/^[a-zA-Z0-9_\s]+$/',
                                                'address.city' => 'required|max:50|regex:/^[a-zA-Z0-9_\s]+$/',
                                                'address.postalCode' => 'required|min:5|max:5|regex:/^[a-zA-Z0-9_\s]+$/',
                                                'address.state' => 'required|max:2|regex:/^[a-zA-Z0-9_\s]+$/',
                                            );
                                } else {
                                    $rules = array (
                                                //'address' => 'required|array|min:3',
                                                'address.firstName' => 'required|max:24|regex:/^[a-zA-Z0-9_\s]+$/',
                                                'address.lastName' => 'required|max:24|regex:/^[a-zA-Z0-9_\s]+$/',
                                                'address.address' => 'required|max:200|regex:/^[a-zA-Z0-9_\s]+$/',
                                                'address.city' => 'required|max:50|regex:/^[a-zA-Z0-9_\s]+$/',
                                                'address.postalCode' => 'required|min:5|max:5|regex:/^[a-zA-Z0-9_\s]+$/',
                                                'address.state' => 'required|max:2|regex:/^[a-zA-Z0-9_\s]+$/',
                                            );
                                }
                                $validator = Validator::make($request->all(), $rules);
                                if($validator->fails()) {
                                    $response = [
                                    'requestId' => strtolower(Str::random(30)),
                                    'message'    => 'Validation Error.',
                                    'error'    => $validator->errors(),
                                    'statusCode' => 422,
                                   ];
                                   return response()->json($response);

                                 } else {
                                $data = array();
                                $data = array (
                                                'user_id' => $auth_token->user_id,
                                                'first_name' => $request->address['firstName'],
                                                'last_name' => $request->address['lastName'],
                                                'city' => $request->address['city'],
                                                'state' => $request->address['state'],
                                                'zip' => $request->address['postalCode'],
                                                'address' => $request->address['address'],
                                                'is_test' => '0',
                                                //'zip4' => $request->zip4,
                                                //'image' => '',
                                                'created' => date('Y-m-d H:i:s'),
                                                'updated' => date('Y-m-d H:i:s'),
                                            );
                                if ($is_test_request=='1')
                                {
                                    $data['image'] = '/upload/scan/2014-05-06/dcc95e7fccba4ce.gif';
                                }

                                else
                                {
                                    /*foreach ($label_groups as $provider_alias => $labels)
                                    {
                                        $provider_account = ORM::factory('Setting')->get_setting()->provider_account(null, $provider_alias);
                                        $provider_scan = Service_Provider::factory('Provider_Scan', $provider_account)
                                              ->labels($labels)
                                              ->is_test_request($this->is_test_request)
                                              ->send_request($filter_label->get_filter_data());

                                        $scan_image = $provider_scan->get_image_path();
                                        $provider_has_errors = $provider_scan->response->has_errors();

                                        if ($provider_has_errors OR !$scan_image)
                                        {
                                            if ($provider_has_errors)
                                            {
                                                $error_message = $provider_scan->response->error_text();
                                            }
                                            else
                                            {
                                                $error_message = 'Invalid request';
                                            }

                                            $this->_answer['statusCode'] = 400;
                                            $this->response->status(400);
                                            return $this->_answer['error'] = $error_message;
                                        }
                                        $scan_images[$provider_alias]['image'] = $scan_image;

                                        foreach ($labels as $label)
                                        {
                                            $scan_images[$provider_alias]['labels'][] = $label->id;
                                        }
                                    }*/
                                }

                           
                                 $insId = DB::table('scans')->insertGetId($data);
                                 $scandata = DB::table('scans')->where('id', $insId)->get();
                                 $show_data = array();
                                 foreach ($scandata as $scandatas) {
                                    $site_url = url('/');
                                    $show_data = array(
                                        'id' => (int)$scandatas->id,
                                        'printImage' => $site_url.$scandatas->image,
                                    );
                                    array_push($final_scan_data, $show_data);
                                 }
                                 $response = [
                                    'requestId' => strtolower(Str::random(30)),
                                    'scans' => $final_scan_data,
                                    'statusCode' => 201,
                                 ];
                                 return response()->json($response);
                               } 
                                
                               $api_statistics = DB::table('user_api_statistics')->where('user_id', $auth_token->user_id)->where('resource_path', $request->path())->first();

                                if (is_null($api_statistics)) { 
                                   $insId = DB::table('user_api_statistics')->insertGetId(
                                     ['user_id' => $auth_token->user_id, 'resource_path' => $request->path(), 'count_requests' => 1, 'created' => date('Y-m-d H:i:s')]
                                    );
                                } else {
                                    DB::table('user_api_statistics')->where('id', $api_statistics->id)->update(['count_requests' => $api_statistics->count_requests + 1, 'updated' => date('Y-m-d H:i:s')]);
                                }
                              } else {
                                $response = [
                                'requestId' => strtolower(Str::random(30)),
                                'statusCode' => 403,
                                'message'    => "You don't have permission to access.",
                               ];
                               return response()->json($response);
                            }
                        } else {
                            $response = [
                            'requestId' => strtolower(Str::random(30)),
                            'message' => 'Token is locked.',
                            'statusCode'    => 403,
                            ];
                            return response()->json($response);
                        }
                    }  
                } else {
                        $response = [
                        'requestId' => strtolower(Str::random(30)),
                        'statusCode'    => 405,
                        'error' => 'The requested URL '. $uri .'was not found on this server.',
                        ];
                        return response()->json($response);
                }
            } else {
                $response = [
                    'requestId' => strtolower(Str::random(30)),
                    'statusCode' => 400,
                    'error'    => 'Required parameter not passed: authToken',
                ];
                return response()->json($response);
            }
           // return response()->json($response);
        } catch (\Exception $e) {
            return $e->getMessage();
        }        
    }

        public function get_getList(Request $request)
    {   
        try {
            $token = trim($request->authToken);
            $uri = $request->path();
            $records = array();
            $finlarr = array();
            if($token) {
                if($uri == 'api/v1/shipping/scanForm/getList') {
                    $auth_token = DB::table('user_api')->where('auth_token', $token)->first();
                    if (is_null($auth_token)) { 
                        $response = [
                        'requestId' => strtolower(Str::random(30)),
                        'message'    => 'Token does not exist.',
                        'statusCode' => 401,
                        ];
                        return response()->json($response);
                    } else {
                        if($auth_token->active == 1) {
                            $user = DB::table('users')->where('id', $auth_token->user_id)->where('in_archive', 0)->first();
                            if ($user) {
                            $date = new Carbon\Carbon;
                            $dates = $date->subDays(7);
                            $scans = DB::table('scans')->where('user_id', $auth_token->user_id)->where('is_test', 1)->get();
                            $scanss = $scans->toArray();


                            if(count($scanss) > 0) {
                                $records = $address = array();
                                foreach($scanss as $getscandata) {
                                        $records['id'] = (int)$getscandata->id;
                                        $records['created'] = $getscandata->created;

                                        $address['firstName'] = $getscandata->first_name;
                                        $address['lastName'] = $getscandata->last_name;
                                        $address['city'] =  $getscandata->city;
                                        $address['state'] = $getscandata->state;
                                        $address['postalCode'] = $getscandata->zip;
                                        $address['zip4'] = $getscandata->zip4;
                                        $address['address'] = $getscandata->address;
                                        $address['created'] = $getscandata->created;

                                        $records['address'] = $address;

                                        
                                        $scan_id = $getscandata->id;
                                        DB::enableQueryLog();
                                        $labels = DB::table('labels')
                                                ->join('label_scans', 'label_scans.label_id', '=', 'labels.id')
                                                ->where('label_scans.scan_id',$scan_id)
                                                ->orderBy('labels.id','DESC')
                                                ->get();

                                        $records['labels'] = array();
                                        foreach ($labels as $label) {
                                            $records['labels'][]= array(
                                                'id' => (int) $label->id,
                                                'trackingNumber' => $label->tracking_number,
                                                'created' => $label->created,
                                            );
                                        }
                                        array_push($finlarr,$records);
                              }
                            }
                            if (count($records) >0) {
                                    $response = [
                                    'requestId' => strtolower(Str::random(30)),
                                    'records'    => [$finlarr],
                                    'statusCode' => $this->successStatus,
                                   ];
                                 return response()->json($response);
                                  } else {
                                    $response = [
                                    'requestId' => strtolower(Str::random(30)),
                                    'records'    => $finlarr,
                                    'statusCode' => $this->successStatus,
                                   ];
                                 return response()->json($response);
                                }
                              } else {
                                $response = [
                                'requestId' => strtolower(Str::random(30)),
                                'statusCode' => 403,
                                'message'    => "You don't have permission to access.",
                               ];
                               return response()->json($response);
                            }
                        } else {
                            $response = [
                            'requestId' => strtolower(Str::random(30)),
                            'message' => 'Token is locked.',
                            'statusCode'    => 403,
                            ];
                            return response()->json($response);
                        }
                    }  
                } else {
                        $response = [
                        'requestId' => strtolower(Str::random(30)),
                        'statusCode'    => 405,
                        'error' => 'The requested URL '. $uri .'was not found on this server.',
                        ];
                        return response()->json($response);
                }
            } else {
                $response = [
                    'requestId' => strtolower(Str::random(30)),
                    'statusCode' => 400,
                    'error'    => 'Required parameter not passed: authToken',
                ];
                return response()->json($response);

        }
    }
        catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function get_getLabels(Request $request)
    {   
        try {
            $token = trim($request->authToken);
            $uri = $request->path();
            $records = array();
            if($token) {
                if($uri == 'api/v1/shipping/scanForm/getLabels') {
                    $auth_token = DB::table('user_api')->where('auth_token', $token)->first();
                    if (is_null($auth_token)) { 
                        $response = [
                        'requestId' => strtolower(Str::random(30)),
                        'message'    => 'Token does not exist.',
                        'statusCode' => 401,
                        ];
                        return response()->json($response);
                    } else {
                        if($auth_token->active == 1) {
                            $user = DB::table('users')->where('id', $auth_token->user_id)->where('in_archive', 0)->first();
                            if ($user) {
                                $user_id = $auth_token->user_id;
                                    DB::enableQueryLog();
                                    $labels = DB::table('labels')
                                        ->where('user_id',$auth_token->user_id)
                                        ->where('is_test_label','1')
                                        ->where('scan_id',NULL)
                                        ->orderBy('id','DESC')
                                        ->get();
                                        
                                foreach ($labels as $key => $label) {
                                    $records[$key] = array(
                                        'id' => $label->id,
                                        'trackingNumber' => $label->tracking_number,
                                        'created' => $label->created,
                                    );
                                }
                                if (count($records) >0) {
                                    $response = [
                                    'requestId' => strtolower(Str::random(30)),
                                    'records'    => $records,
                                    'statusCode' => $this->successStatus,
                                   ];
                                    return response()->json($response);
                                  } else {
                                    $response = [
                                    'requestId' => strtolower(Str::random(30)),
                                    'records'    => $records,
                                    'statusCode' => $this->successStatus,
                                   ];
                                   return response()->json($response);
                                }    
                            } else {
                                $response = [
                                'requestId' => strtolower(Str::random(30)),
                                'statusCode' => 403,
                                'message'    => "You don't have permission to access.",
                               ];
                               return response()->json($response);
                            }
                        } else {
                            $response = [
                            'requestId' => strtolower(Str::random(30)),
                            'message' => 'Token is locked.',
                            'statusCode'    => 403,
                            ];
                            return response()->json($response);
                        }
                    }  
                } else {
                        $response = [
                        'requestId' => strtolower(Str::random(30)),
                        'statusCode'    => 405,
                        'error' => 'The requested URL '. $uri .'was not found on this server.',
                        ];
                        return response()->json($response);
                }
            } else {
                $response = [
                    'requestId' => strtolower(Str::random(30)),
                    'statusCode' => 400,
                    'error'    => 'Required parameter not passed: authToken',
                ];
                return response()->json($response);

        }
    }
        catch (\Exception $e) {
            return $e->getMessage();
        } 
    }      

}
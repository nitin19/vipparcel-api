<?php

namespace App\Http\Controllers\Api\Shipping;

use App\User;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Api\APIBaseController as APIBaseController;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Str;
use App\Balance;
use Auth;
use DB;
use Exception;
use Log;
use Session;
use Hash;
use Image;
use Carbon;

class PickupController extends APIBaseController
{

    public $successStatus = 200;

    public function __construct()
    {
       //
    }

    public function get_getList(Request $request)
    {   
        try {
            $token = trim($request->authToken);
            $uri = $request->path();
            $fullUrl = $request->fullUrl(); 
            $records = array();
            if($token) {
                if($uri == 'api/v1/shipping/pickup/getList') {
                    $auth_token = DB::table('user_api')->where('auth_token', $token)->first();
                    if (is_null($auth_token)) { 
                        $response = [
                        'requestId' => strtolower(Str::random(30)),
                        'message'    => 'Token does not exist.',
                        'statusCode' => 401,
                        ];
                        return response()->json($response);
                    } else {
                        if($auth_token->active == 1) {
                        $user = DB::table('users')->where('id', $auth_token->user_id)->where('in_archive', 0)->first();
                            if ($user) {
                                $limit = trim($request->limit);
                                if($limit) {
                                    if($limit <= 150) {
                                        $limit = trim($request->limit);
                                    } else {
                                        $response = [
                                            'requestId' => strtolower(Str::random(30)),
                                            'statusCode' => 400,
                                            'error'    => "Invalid parameter: limit",
                                           ];
                                        return response()->json($response);   
                                    }
                                } else {
                                    $limit = 30;
                                }

                                $offset = trim($request->offset);
                                if($offset) {
                                   $offset = trim($request->offset);
                                } else {
                                   $offset = 0; 
                                }
                                
                                $orderByArr = array('id', 'created');
                                if (is_array($request->orderBy)) {
                                   foreach($request->orderBy as $Key=>$Value) {
                                     break;
                                   }

                                  if($Key) {
                                    if(in_array(trim($Key), $orderByArr)) {
                                    if(($Value!='') && ( $Value == 'asc' || $Value == 'ASC' || $Value == 'desc' || $Value == 'DESC' )){
                                           $sortKey = $Key;
                                           $sortValue = $Value;
                                       } else {
                                        $response = [
                                            'requestId' => strtolower(Str::random(30)),
                                            'statusCode' => 400,
                                            'error'    => "Invalid parameter: orderBy",
                                           ];
                                        return response()->json($response);
                                       }
                                    } else {
                                    $response = [
                                            'requestId' => strtolower(Str::random(30)),
                                            'statusCode' => 400,
                                            'error'    => "Field \"$Key\" is not sortable",
                                           ];
                                        return response()->json($response);
                                   }
                                 } else {
                                   $response = [
                                            'requestId' => strtolower(Str::random(30)),
                                            'statusCode' => 400,
                                            'error'    => "Field \"0\" is not sortable",
                                           ];
                                        return response()->json($response);
                                  } 

                                } else {

                                $orderBy = trim($request->orderBy);
                                if($orderBy) {
                                   if(in_array(trim($request->orderBy), $orderByArr)) {
                                     switch ($orderBy) {
                                        case 'id':
                                            $sortKey = 'id';
                                            $sortValue = 'asc';
                                            break;
                                        case 'created':
                                            $sortKey = 'created';
                                            $sortValue = 'asc';
                                            break;
                                        } 
                                   } else {
                                    $response = [
                                            'requestId' => strtolower(Str::random(30)),
                                            'statusCode' => 400,
                                            'error'    => "Field \"$orderBy\" is not sortable",
                                           ];
                                   return response()->json($response);
                                   }
                                } else {
                                 if (strpos($fullUrl,'orderBy') !== false) {
                                      $response = [
                                            'requestId' => strtolower(Str::random(30)),
                                            'statusCode' => 400,
                                            'error'    => "Invalid parameter: orderBy",
                                           ];
                                        return response()->json($response);
                                     } else {
                                      $sortKey = 'id';
                                      $sortValue = 'asc';
                                    }
                                  }     
                            }

                            $user_pickups_all = DB::table('carrier_requests')->where('user_id', '=', $auth_token->user_id)->where('is_test', 1)->get();

                            $user_pickups = DB::table('carrier_requests')->where('user_id', '=', $auth_token->user_id)->where('is_test', 1)->orderBy($sortKey, $sortValue)->offset($offset)->limit($limit)->get(); 
                           // $pickups = $user_pickups->toArray();
                               
                                if(count($user_pickups_all) > 0) {

                                    foreach($user_pickups as $key => $pickup) {
                         
                            $labels = DB::table('labels')->join('carrier_request_labels', 'carrier_request_labels.label_id', '=', 'labels.id')->where('carrier_request_labels.carrier_request_id',$pickup->id)->orderBy('labels.id','DESC')->get();

                                $records[$key] = array(
                                            'id' => (int) $pickup->id,
                                            'firstName' => $pickup->FirstName,
                                            'lastName' => $pickup->LastName,
                                            'state' => $pickup->State,
                                            'city' => $pickup->City,
                                            'address' => $pickup->Address,
                                            'phone' => $pickup->Phone,
                                            'packageLocation' => $pickup->PackageLocation,
                                            'specialInstructions' => $pickup->SpecialInstructions,
                                            'zip5' => $pickup->ZIP5,
                                            'zip4' => $pickup->ZIP4,
                                            'created' => $pickup->created
                                        );

                                    $records[$key]['trackNumbers'] = array();
                                    if( count($labels) > 0 ) {
                                        foreach($labels as $label) {   
                                            $records[$key]['trackNumbers'][] = $label->tracking_number;
                                        }
                                    } else {
                                      $records[$key]['trackNumbers'] = array();
                                    }  
                                }

                                   $response = [
                                    'requestId' => strtolower(Str::random(30)),
                                    'records'    => $records,
                                    'totalRecords'    => (int) count($user_pickups_all),
                                    'statusCode' => $this->successStatus,
                                   ];
                                   return response()->json($response);

                                } else {
                                   $response = [
                                    'requestId' => strtolower(Str::random(30)),
                                    'totalRecords'    => (int) count($user_pickups_all),
                                   // 'message'    => 'Record does not exist.',
                                    'statusCode' => 200, //401,
                                   ];
                                   return response()->json($response);
                                }

                                $api_statistics = DB::table('user_api_statistics')->where('user_id', $auth_token->user_id)->where('resource_path', $request->path())->first();

                                if (is_null($api_statistics)) { 
                                   $insId = DB::table('user_api_statistics')->insertGetId(
                                     ['user_id' => $auth_token->user_id, 'resource_path' => $request->path(), 'count_requests' => 1, 'created' => date('Y-m-d H:i:s')]
                                    );
                                } else {
                                    DB::table('user_api_statistics')->where('id', $api_statistics->id)->update(['count_requests' => $api_statistics->count_requests + 1, 'updated' => date('Y-m-d H:i:s')]);
                                }
                               
                              } else {
                                $response = [
                                'requestId' => strtolower(Str::random(30)),
                                'statusCode' => 403,
                                'message'    => "You don't have permission to access.",
                               ];
                               return response()->json($response);
                            }
                         } else {
                            $response = [
                            'requestId' => strtolower(Str::random(30)),
                            'message' => 'Token is locked.',
                            'statusCode'    => 403,
                            ];
                            return response()->json($response);
                        }
                    }  
                } else {
                        $response = [
                        'requestId' => strtolower(Str::random(30)),
                        'statusCode'    => 405,
                        'error' => 'The requested URL '. $uri .'was not found on this server.',
                        ];
                        return response()->json($response);
                }
            } else {
                $response = [
                    'requestId' => strtolower(Str::random(30)),
                    'statusCode' => 400,
                    'error'    => 'Required parameter not passed: authToken',
                ];
                return response()->json($response);
            }
           // return response()->json($response);
        } catch (\Exception $e) {
            return $e->getMessage();
        }    
    }

    public function get_getInfo(Request $request, $id)
    {   
        try {
            $token = trim($request->authToken);
            $uri = $request->path(); 
            $records = array();
            if($token) {
                if($uri == 'api/v1/shipping/pickup/getInfo/'.$id) {
                    $auth_token = DB::table('user_api')->where('auth_token', $token)->first();
                    if (is_null($auth_token)) { 
                        $response = [
                        'requestId' => strtolower(Str::random(30)),
                        'message'    => 'Token does not exist.',
                        'statusCode' => 401,
                        ];
                        return response()->json($response);
                    } else {
                        if($auth_token->active == 1) {
                        $user = DB::table('users')->where('id', $auth_token->user_id)->where('in_archive', 0)->first();
                            if ($user) {
                        $pickup = DB::table('carrier_requests')->where('id', $id)->where('user_id', '=', $auth_token->user_id)->where('is_test', 1)->first();
                            if ($pickup) {

                              $labels = DB::table('labels')->join('carrier_request_labels', 'carrier_request_labels.label_id', '=', 'labels.id')->where('carrier_request_labels.carrier_request_id',$id)->orderBy('labels.id','DESC')->get();

                              $records = array(
                                            'requestId'    => strtolower(Str::random(30)),
                                            'id' => (int) $pickup->id,
                                            'firstName' => $pickup->FirstName,
                                            'lastName' => $pickup->LastName,
                                            'state' => $pickup->State,
                                            'city' => $pickup->City,
                                            'address' => $pickup->Address,
                                            'phone' => $pickup->Phone,
                                            'packageLocation' => $pickup->PackageLocation,
                                            'specialInstructions' => $pickup->SpecialInstructions,
                                            'zip5' => $pickup->ZIP5,
                                            'zip4' => $pickup->ZIP4,
                                            'created' => $pickup->created,
                                        );
                              
                              $records['trackNumbers'] = array();
                                    if( count($labels) > 0 ) {
                                        foreach($labels as $label) { 
                                            $records['trackNumbers'][] = $label->tracking_number;
                                        }
                                    } else {
                                       $records['trackNumbers'] = array(); 
                                    }        

                              $records['statusCode'] = $this->successStatus;
                              $response = $records;
                              return response()->json($response);
                                    
                                  } else {
                                    $response = [
                                    'requestId' => strtolower(Str::random(30)),
                                    'message'    => 'Record does not exist.',
                                    'statusCode' => 401,
                                   ];
                                   return response()->json($response);
                                }   

                               $api_statistics = DB::table('user_api_statistics')->where('user_id', $auth_token->user_id)->where('resource_path', $request->path())->first();

                                if (is_null($api_statistics)) { 
                                   $insId = DB::table('user_api_statistics')->insertGetId(
                                     ['user_id' => $auth_token->user_id, 'resource_path' => $request->path(), 'count_requests' => 1, 'created' => date('Y-m-d H:i:s')]
                                    );
                                } else {
                                    DB::table('user_api_statistics')->where('id', $api_statistics->id)->update(['count_requests' => $api_statistics->count_requests + 1, 'updated' => date('Y-m-d H:i:s')]);
                                }
                              } else {
                                $response = [
                                'requestId' => strtolower(Str::random(30)),
                                'statusCode' => 403,
                                'message'    => "You don't have permission to access.",
                               ];
                               return response()->json($response);
                            }
                        } else {
                            $response = [
                            'requestId' => strtolower(Str::random(30)),
                            'message' => 'Token is locked.',
                            'statusCode'    => 403,
                            ];
                            return response()->json($response);
                        }
                    }  
                } else {
                        $response = [
                        'requestId' => strtolower(Str::random(30)),
                        'statusCode'    => 405,
                        'error' => 'The requested URL '. $uri .'was not found on this server.',
                        ];
                        return response()->json($response);
                }
            } else {
                $response = [
                    'requestId' => strtolower(Str::random(30)),
                    'statusCode' => 400,
                    'error'    => 'Required parameter not passed: authToken',
                ];
                return response()->json($response);
            }
          //  return response()->json($response);
        } catch (\Exception $e) {
            return $e->getMessage();
        }    
    }


    public function get_getLabels(Request $request)
    {   
        try {
            $token = trim($request->authToken);
            $uri = $request->path();
            $fullUrl = $request->fullUrl(); 
            $records = array();
            if($token) {
                if($uri == 'api/v1/shipping/pickup/getLabels') {
                    $auth_token = DB::table('user_api')->where('auth_token', $token)->first();
                    if (is_null($auth_token)) { 
                        $response = [
                        'requestId' => strtolower(Str::random(30)),
                        'message'    => 'Token does not exist.',
                        'statusCode' => 401,
                        ];
                        return response()->json($response);
                    } else {
                        if($auth_token->active == 1) {
                        $user = DB::table('users')->where('id', $auth_token->user_id)->where('in_archive', 0)->first();
                            if ($user) {
                                $limit = trim($request->limit);
                                if($limit) {
                                    if($limit <= 150) {
                                        $limit = trim($request->limit);
                                    } else {
                                        $response = [
                                            'requestId' => strtolower(Str::random(30)),
                                            'statusCode' => 400,
                                            'error'    => "Invalid parameter: limit",
                                           ];
                                        return response()->json($response);   
                                    }
                                } else {
                                    $limit = 30;
                                }

                                $offset = trim($request->offset);
                                if($offset) {
                                   $offset = trim($request->offset);
                                } else {
                                   $offset = 0; 
                                }
                                
                                $orderByArr = array('id', 'created', 'updated');
                                if (is_array($request->orderBy)) {
                                   foreach($request->orderBy as $Key=>$Value) {
                                     break;
                                   }

                                  if($Key) {
                                    if(in_array(trim($Key), $orderByArr)) {
                                    if(($Value!='') && ( $Value == 'asc' || $Value == 'ASC' || $Value == 'desc' || $Value == 'DESC' )){
                                           $sortKey = $Key;
                                           $sortValue = $Value;
                                       } else {
                                        $response = [
                                            'requestId' => strtolower(Str::random(30)),
                                            'statusCode' => 400,
                                            'error'    => "Invalid parameter: orderBy",
                                           ];
                                        return response()->json($response);
                                       }
                                    } else {
                                    $response = [
                                            'requestId' => strtolower(Str::random(30)),
                                            'statusCode' => 400,
                                            'error'    => "Field \"$Key\" is not sortable",
                                           ];
                                        return response()->json($response);
                                   }
                                 } else {
                                   $response = [
                                            'requestId' => strtolower(Str::random(30)),
                                            'statusCode' => 400,
                                            'error'    => "Field \"0\" is not sortable",
                                           ];
                                        return response()->json($response);
                                  } 

                                } else {

                                $orderBy = trim($request->orderBy);
                                if($orderBy) {
                                   if(in_array(trim($request->orderBy), $orderByArr)) {
                                     switch ($orderBy) {
                                        case 'id':
                                            $sortKey = 'id';
                                            $sortValue = 'asc';
                                            break;
                                        case 'created':
                                            $sortKey = 'created';
                                            $sortValue = 'asc';
                                            break;
                                        case 'updated':
                                            $sortKey = 'updated';
                                            $sortValue = 'asc';
                                            break;
                                        } 
                                   } else {
                                    $response = [
                                            'requestId' => strtolower(Str::random(30)),
                                            'statusCode' => 400,
                                            'error'    => "Field \"$orderBy\" is not sortable",
                                           ];
                                   return response()->json($response);
                                   }
                                } else {
                                 if (strpos($fullUrl,'orderBy') !== false) {
                                      $response = [
                                            'requestId' => strtolower(Str::random(30)),
                                            'statusCode' => 400,
                                            'error'    => "Invalid parameter: orderBy",
                                           ];
                                        return response()->json($response);
                                     } else {
                                      $sortKey = 'id';
                                      $sortValue = 'asc';
                                    }
                                  }     
                            }

                            $date = \Carbon\Carbon::today()->subDays(29);

                            $pickup_labels_all = DB::table('labels AS label')->select("label.*")
                                            ->leftJoin('carrier_request_labels AS rl', 'rl.label_id', '=', 'label.id')
                                            ->leftJoin("carrier_requests AS cr",function($join){
                                                $join->on("cr.id","=","rl.carrier_request_id")
                                                     ->on("cr.user_id","=","label.user_id");
                                            })
                                        ->where('label.user_id', '=', $auth_token->user_id)
                                        ->where('label.is_test_label', '=', 1)
                                        ->whereNull('cr.id')
                                        ->where('label.created', '>=', $date)
                                        ->get();

                            $pickup_labels = DB::table('labels AS label')->select("label.*")
                                            ->leftJoin('carrier_request_labels AS rl', 'rl.label_id', '=', 'label.id')
                                            ->leftJoin("carrier_requests AS cr",function($join){
                                                $join->on("cr.id","=","rl.carrier_request_id")
                                                     ->on("cr.user_id","=","label.user_id");
                                            })
                                        ->where('label.user_id', '=', $auth_token->user_id)
                                        ->where('label.is_test_label', '=', 1)
                                        ->whereNull('cr.id')
                                        ->where('label.created', '>=', $date)
                                        ->orderBy($sortKey, $sortValue)
                                        ->offset($offset)
                                        ->limit($limit)
                                        ->get();            
                               
                                if(count($pickup_labels_all) > 0) {

                                foreach($pickup_labels as $label) {
                                    
                                    $records[] = array(
                                             'id'          => (int) $label->id,
                                              'trackNumber' => $label->tracking_number,
                                              'created'     => $label->created,
                                              'updated'     => $label->updated
                                        );   
                                }

                                   $response = [
                                    'requestId' => strtolower(Str::random(30)),
                                    'countItems'    => (int) count($pickup_labels_all),
                                    'records'    => $records,
                                    'statusCode' => $this->successStatus,
                                   ];
                                   return response()->json($response);

                                } else {
                                   $response = [
                                    'requestId' => strtolower(Str::random(30)),
                                    'countItems'    => (int) count($pickup_labels_all),
                                    'records'    => $records,
                                    'statusCode' => 200, //401,
                                   ];
                                   return response()->json($response);
                                }

                                $api_statistics = DB::table('user_api_statistics')->where('user_id', $auth_token->user_id)->where('resource_path', $request->path())->first();

                                if (is_null($api_statistics)) { 
                                   $insId = DB::table('user_api_statistics')->insertGetId(
                                     ['user_id' => $auth_token->user_id, 'resource_path' => $request->path(), 'count_requests' => 1, 'created' => date('Y-m-d H:i:s')]
                                    );
                                } else {
                                    DB::table('user_api_statistics')->where('id', $api_statistics->id)->update(['count_requests' => $api_statistics->count_requests + 1, 'updated' => date('Y-m-d H:i:s')]);
                                }
                               
                              } else {
                                $response = [
                                'requestId' => strtolower(Str::random(30)),
                                'statusCode' => 403,
                                'message'    => "You don't have permission to access.",
                               ];
                               return response()->json($response);
                            }
                         } else {
                            $response = [
                            'requestId' => strtolower(Str::random(30)),
                            'message' => 'Token is locked.',
                            'statusCode'    => 403,
                            ];
                            return response()->json($response);
                        }
                    }  
                } else {
                        $response = [
                        'requestId' => strtolower(Str::random(30)),
                        'statusCode'    => 405,
                        'error' => 'The requested URL '. $uri .'was not found on this server.',
                        ];
                        return response()->json($response);
                }
            } else {
                $response = [
                    'requestId' => strtolower(Str::random(30)),
                    'statusCode' => 400,
                    'error'    => 'Required parameter not passed: authToken',
                ];
                return response()->json($response);
            }
           // return response()->json($response);
        } catch (\Exception $e) {
            return $e->getMessage();
        }    
    }

    public function post_request(Request $request)
    {   
        try {
            $token = trim($request->authToken);
            $uri = $request->path();
            $fullUrl = $request->fullUrl(); 
            $records = array();
            if($token) {
                if($uri == 'api/v1/shipping/pickup/request') {
                    $auth_token = DB::table('user_api')->where('auth_token', $token)->first();
                    if (is_null($auth_token)) { 
                        $response = [
                        'requestId' => strtolower(Str::random(30)),
                        'message'    => 'Token does not exist.',
                        'statusCode' => 401,
                        ];
                        return response()->json($response);
                    } else {
                        if($auth_token->active == 1) {
                        $user = DB::table('users')->where('id', $auth_token->user_id)->where('in_archive', 0)->first();
                            if ($user) {

                                $packages = $request->packages;

                                if (empty($packages) OR ! is_array($packages))
                                {
                                  $response = [
                                            'requestId' => strtolower(Str::random(30)),
                                            'statusCode' => 400,
                                            'error'    => "Invalid parameter: packages",
                                           ];
                                        return response()->json($response);
                                }

                                $packages = array_unique($packages);

                                $date = \Carbon\Carbon::today()->subDays(29);
                             
                              //  DB::enableQueryLog();
                                $labels = DB::table('labels AS label')->select("label.*")
                                            ->leftJoin('carrier_request_labels AS rl', 'rl.label_id', '=', 'label.id')
                                            ->leftJoin("carrier_requests AS cr",function($join){
                                                $join->on("cr.id","=","rl.carrier_request_id")
                                                     ->on("cr.user_id","=","label.user_id");
                                            })
                                        ->where('label.user_id', '=', $auth_token->user_id)
                                        ->where('label.is_test_label', '=', 1)
                                        ->whereNull('cr.id')
                                        ->where('label.created', '>=', $date)
                                        ->whereIn('label.tracking_number', $packages)
                                       // ->groupBy('label.tracking_number')
                                        ->get();         
                                       // dd(DB::getQueryLog());

                                $label_items_arr = array();

                                if (count($labels) != count($packages)) {

                                  foreach($labels as $label_items) {
                                    $label_items_arr[] = $label_items->tracking_number;
                                   }

                                    $diff = array_diff($packages, $label_items_arr);
                                    $response = [
                                            'requestId' => strtolower(Str::random(30)),
                                            'statusCode' => 400,
                                            'error'    => "Invalid package list. Not allowed: ".implode(', ', $diff),
                                           ];
                                        return response()->json($response);    
                                    }

                              if (trim($request->packageLocation) == 'ot' AND ! trim($request->specialInstructions)) {

                                  $response = [
                                            'requestId' => strtolower(Str::random(30)),
                                            'statusCode' => 400,
                                            'error'    => "specialInstructions must not be empty",
                                           ];
                                        return response()->json($response);
                                     }
                              
                              $address = $request->address;

                              if (array_key_exists('state', $request->address)) {
                                    
                                    $state = $address['state'];
                                    if($state) {
                                      $stateItem = DB::table('states')->where('abbr', '=', $state)->first();
                                      if(!$stateItem) {

                                        $response = [
                                            'requestId' => strtolower(Str::random(30)),
                                            'statusCode' => 400,
                                            'error'    => "Invalid parameter: address.state",
                                           ];
                                        return response()->json($response);

                                      }
                                    }
                                }

                              $is_usps = TRUE;

                              $status = TRUE;

                              $is_test = TRUE;

                              $rules = [];

                              $rules['address.firstName'] = 'required|max:100|regex:/^[a-zA-Z0-9_\s]+$/';

                              $rules['address.lastName'] = 'required|max:100|regex:/^[a-zA-Z0-9_\s]+$/';

                              $rules['address.state'] = 'regex:/^[a-zA-Z]+$/|max:2|min:2';

                              $rules['address.address'] = 'required|max:100';

                              $rules['address.city'] = 'required|max:100';

                              $rules['address.zip5'] = 'required|max:5|regex:/^[0-9-]+$/';

                              $rules['address.phone'] = 'regex:/^[0-9]+$/|max:10';

                              $rules['packageLocation'] = 'regex:/^[a-zA-Z]+$/|max:2|min:2';

                              $validator = Validator::make($request->all(), $rules);

                              if($validator->fails()) {
                                    $response = [
                                    'requestId' => strtolower(Str::random(30)),
                                    'message'    => 'Validation Error.',
                                    'error'    => $validator->errors(),
                                    'statusCode' => 422,
                                   ];
                                   return response()->json($response);

                              }

                              $carrier_values = array(
                                              'user_id' => $auth_token->user_id,
                                              'status' => $status,
                                              'FirstName' => trim($address['firstName']),
                                              'LastName' => trim($address['lastName']),
                                              'State' => trim($address['state']),
                                              'City' => trim($address['city']),
                                              'ZIP5' => trim($address['zip5']),
                                              'ZIP4' => trim($address['zip4']),
                                              'Address' => trim($address['address']),
                                              'Phone' => trim($address['phone']),
                                              'PackageLocation' => trim($request->packageLocation),
                                              'SpecialInstructions' => trim($request->specialInstructions),
                                              'created' => date('Y-m-d H:i:s'),
                                              'request_carrier' => '',
                                              'response_carrier' => '',
                                              'is_usps' => $is_usps,
                                              'is_test' => $is_test,
                                          );

                                $insId = DB::table('carrier_requests')->insertGetId($carrier_values);

                                if( $insId > 0 ) {

                                    foreach ($labels as $lavel_id) {

                                            $data[] = [
                                                'carrier_request_id' => $insId,
                                                'label_id' => $lavel_id->id
                                            ];
                                      }
                                      
                                      DB::table('carrier_request_labels')->insert($data);
                                }

                                 $response = [
                                    'requestId' => strtolower(Str::random(30)),
                                    'statusCode' => 201,
                                    'id' => (int)$insId,
                                   ];
                                   return response()->json($response);
                               

                                $api_statistics = DB::table('user_api_statistics')->where('user_id', $auth_token->user_id)->where('resource_path', $request->path())->first();

                                if (is_null($api_statistics)) { 
                                   $insId = DB::table('user_api_statistics')->insertGetId(
                                     ['user_id' => $auth_token->user_id, 'resource_path' => $request->path(), 'count_requests' => 1, 'created' => date('Y-m-d H:i:s')]
                                    );
                                } else {
                                    DB::table('user_api_statistics')->where('id', $api_statistics->id)->update(['count_requests' => $api_statistics->count_requests + 1, 'updated' => date('Y-m-d H:i:s')]);
                                }
                               
                              } else {
                                $response = [
                                'requestId' => strtolower(Str::random(30)),
                                'statusCode' => 403,
                                'message'    => "You don't have permission to access.",
                               ];
                               return response()->json($response);
                            }
                         } else {
                            $response = [
                            'requestId' => strtolower(Str::random(30)),
                            'message' => 'Token is locked.',
                            'statusCode'    => 403,
                            ];
                            return response()->json($response);
                        }
                    }  
                } else {
                        $response = [
                        'requestId' => strtolower(Str::random(30)),
                        'statusCode'    => 405,
                        'error' => 'The requested URL '. $uri .'was not found on this server.',
                        ];
                        return response()->json($response);
                }
            } else {
                $response = [
                    'requestId' => strtolower(Str::random(30)),
                    'statusCode' => 400,
                    'error'    => 'Required parameter not passed: authToken',
                ];
                return response()->json($response);
            }
           // return response()->json($response);
        } catch (\Exception $e) {
            return $e->getMessage();
        }    
    }
   


 
}
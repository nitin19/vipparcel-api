<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Str;

class APIToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {   
        $token = trim($request->authToken);
        if($token){
          return $next($request);
        }
        $response = [
                'requestId' => strtolower(Str::random(30)),
                'statusCode' => 400,
                'error'    => 'Required parameter not passed: authToken',
            ];
        return response()->json($response);

        /*if($request->header('Authorization')){
          return $next($request);
        }*/
       /* $url = $request->url();
       $url = $request->fullUrl();
       $method = $request->method();
       $uri = $request->path();
       */
    }
}

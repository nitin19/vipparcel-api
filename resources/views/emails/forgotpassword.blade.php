<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns:v="urn:schemas-microsoft-com:vml">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;" />
    <meta name="viewport" content="width=600,initial-scale = 2.3,user-scalable=no">
    <!--[if !mso]><!-- -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <!-- <![endif]-->

    <title></title>


    <!-- [if gte mso 9]><style type=”text/css”>
        body {
        font-family: arial, sans-serif!important;
        }
        </style>
    <![endif]-->
</head>

<body  leftmargin="0" topmargin="0"  marginwidth="0" marginheight="0" style="background-image:url('{{url('/public')}}/images/emailimages/forgot-email.jpg');background-size: 100% 100%;background-repeat: no-repeat;background-position: center;background-color:transparent;">
    <!-- big image section -->

    <table border="0" width="100%" cellpadding="0" cellspacing="0" style="font-family: 'Raleway', sans-serif;">
<tr>
                        <td style="font-size: 50px; line-height: 50px;padding-top:300px">&nbsp;</td>
                    </tr>
        <tr>  
            <td align="center">
                <table border="0" align="center" width="590" cellpadding="0" cellspacing="0" class="container590">
                    
                    <tr>
                        <td height="30" style="font-size: 30px; line-height: 30px;">&nbsp;</td>
                    </tr>
                    <tr> 
                        <td align="center" style="color: #333333; font-size: 34px;font-weight:700;letter-spacing: 3px; line-height: 35px;" class="main-header">

                            <h2 style="font-size: 17px;letter-spacing: 5px;line-height: 34px;color: #666666;text-align: center;font-weight: bold;text-transform: uppercase;">Password Recovery
                            </h2>
                        </td>
                    </tr>

                    

                    <tr>
                        <td align="center">
                            <table border="0" align="center" cellpadding="0" cellspacing="0" class="container590">
                                <tr>
                                    <td align="center">

                                        <h1 style="font-size: 44px;letter-spacing: 7px;line-height: 34px;color: #04a5dc;font-weight: bold;text-align: center;text-transform: uppercase;margin: 20px 0px;">Forgot password
                                        </h1>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td align="center">
                            <table border="0" width="540" align="center" cellpadding="0" cellspacing="0" class="container590">
                                <tr>
                                    <td align="center">
                                        <p style="font-size: 14px;line-height: 25px;color: #959494;text-align: center;">We have received a request to change your account password. Here we are providing you a link to reset your password. To keep your account safe please don’t share the credentials of your account with anyone. If you didn’t make this request, please ignore this email. </p>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

             <tr>
                        <td align="center">
                            <a href="" style="font-size: 17px;letter-spacing: 1px;line-height: 34px;color: #e63c2b;font-weight: bold;text-align: center;text-decoration: none;">To reset password click below</a>
                        </td>
                    </tr>


                    <tr>
                        <td align="center">
                            <table border="0" align="center" width="160" cellpadding="0" cellspacing="0">

                                <tr>
                                    <td height="10" style="font-size: 10px; line-height: 10px;">&nbsp;</td>
                                </tr>

                                <tr>
                                    <td align="center" style="color: #ffffff; font-size: 14px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 26px;">
                            <?php 
                               $token_url = 'http://localhost:3000/reset-password/'.$url.'/'.$email;
                            ?>
                            <!-- <a href="{{$token_url}}">Reset Password</a> -->
                             <a href="{{$token_url}}"><img src="{{url('/public')}}/images/emailimages/reset-password-btn.png" style="display: block;background:none;"  border="0"  alt=""></a>
                                        </td>
                                </tr>

                                <tr>
                                    <td height="30" style="font-size: 30px; line-height: 30px;">&nbsp;</td>
                                </tr>

                               

                            </table>
                        </td>
                    </tr>


                </table>

            </td>
        </tr>

    </table>

    <!-- end section -->

    <!-- ======= divider ======= -->

    <table border="0" align="center" width="600" cellpadding="0" cellspacing="0" style="font-family: 'Raleway', sans-serif;">

        <tr class="hide">
            <td height="10" style="font-size: 10px; line-height: 10px;">&nbsp;</td>
        </tr>
      

            <tr>
                <td align="center" style="color: #ffffff; font-size: 14px; line-height: 26px;">
                    <a href="https://www.facebook.com/vipparcel" target="_blank"><img src="{{url('/public')}}/images/emailimages/facebook.png" style="display:inline-block;background:none;"  border="0"  alt=""></a>
                    <a href="#"><img src="{{url('/public')}}/images/emailimages/pint.png" style="display:inline-block;background:none;"  border="0"  alt=""></a>
                    <a href="#"><img src="{{url('/public')}}/images/emailimages/in.png" style="display:inline-block;background:none;"  border="0"  alt=""></a>
                    <a href="#"><img src="{{url('/public')}}/images/emailimages/google.png" style="display:inline-block;background:none;"  border="0"  alt=""></a>
                    <a href="https://twitter.com/VIPparcel" target="_blank"><img src="{{url('/public')}}/images/emailimages/twiter-img.png" style="display:inline-block;background:none;"  border="0"  alt=""></a>

                                        </td>
                                </tr>

            <tr>
            <td height="20" style="font-size: 20px; line-height: 20px;">&nbsp;</td>
        </tr>

    </table>


    

    <!-- footer ====== -->
    <table border="0" width="100%" cellpadding="0" cellspacing="0" >

        <tr>
            <td height="25" style="font-size: 25px; line-height: 25px;">&nbsp;</td>
        </tr>

        <tr>
            <td align="center">

                <table border="0" align="center" width="600" cellpadding="0" cellspacing="0" class="container590">

                    <tr>
                        <td>
                          

                            <table border="0" align="left" width="5" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="container590">
                                <tr>
                                    <td height="20" width="5" style="font-size: 20px; line-height: 20px;">&nbsp;</td>
                                </tr>
                            </table>

                            <table border="0" align="center" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="container590">

                                <tr>
                                    <td align="center">
                                        <table align="center" border="0" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td align="center">
                        
                         
                                    <a style="font-size: 15px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px;color: #333333; text-decoration: none;font-weight:400; display: inline-block;    border-bottom: 1px solid #999999;" href="#">Unsubscribe Emails </a>
                                     <a style="font-size: 15px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px;color: #333333; text-decoration: none;font-weight:400; display: inline-block;" href="#">|</a>
                            <a style="font-size: 15px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px;color: #333333; text-decoration: none;font-weight:400; display: inline-block;    border-bottom: 1px solid #999999;" href="#">ContactUs</a>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                            </table>
                        </td>
                    </tr>

                </table>
            </td>
        </tr>

        <tr>
            <td height="25" style="font-size: 25px; line-height: 25px;">&nbsp;</td>
        </tr>

    </table>
    <!-- end footer ====== -->

</body>

</html>
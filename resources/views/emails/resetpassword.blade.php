<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns:v="urn:schemas-microsoft-com:vml">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;" />
    <meta name="viewport" content="width=600,initial-scale = 2.3,user-scalabel=no">
    <!--[if !mso]><!-- -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <!-- <![endif]-->
    <title></title>
    <!-- [if gte mso 9]><style type=”text/css”>
        body {
        font-family: arial, sans-serif!important;
        }
        </style>
    <![endif]-->
</head>

<body  leftmargin="0" topmargin="0"  marginwidth="0" marginheight="0" style="background-image:url('{{url('/public')}}/images/emailimages/email_template.jpg');background-size: 100% 100%;background-repeat: no-repeat;background-position: center;background-color:transparent;">
    <!-- pre-header -->
    <table style="display:none!important;">
        <tr>
            <td>
                <div style="overflow:hidden;display:none;font-size:1px;color:#ffffff;line-height:1px;font-family:Arial;maxheight:0px;max-width:0px;opacity:0;">
                    Pre-header for the newsletter template
                </div>
            </td>
        </tr>
    </table>
    <!-- pre-header end -->
 
    <!-- big image section -->

    <table border="0" width="100%" cellpadding="0" cellspacing="0" style="font-family: 'Raleway', sans-serif;">
            <tr>
                <td style="font-size: 50px; line-height: 50px;padding-top:100px">&nbsp;</td>
            </tr>
            <tr>
                <td align="center">
                    <img src="{{url('/public')}}/images/emailimages/download.png" alt="" />
                </td>
            </tr>
        <tr>  
            <td align="center">
                <table border="0" align="center" width="590" cellpadding="0" cellspacing="0" class="container590">
                    
                    <tr>
                        <td height="30" style="font-size: 30px; line-height: 30px;">&nbsp;</td>
                    </tr>
        
                    <tr>
                        <td align="center">
                            <table border="0" align="center" cellpadding="0" cellspacing="0" class="container590">
                                <tr>
                                    <td align="center">
                                        <img src="{{url('/public')}}/images/emailimages/download.png" alt="" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center">
                                        <h1 style="font-size: 50px;letter-spacing: 7px;line-height: 34px;color: #04a5dc;font-weight: bold;text-align: center;text-transform: uppercase;margin: 20px 0px;">Stephen
                                        </h1>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td align="center">
                            <table border="0" width="540" align="center" cellpadding="0" cellspacing="0" class="container590">
                                <tr>
                                    <td align="center">
                                        <p style="font-size: 14px;line-height: 34px;color: #959494;text-align: center;">You've successfully changed your Account password.</p>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    
                    <tr>
                        <td align="center">
                            <table border="0" width="540" align="center" cellpadding="0" cellspacing="0" class="container590">
                                <tr>
                                    <td align="center">
                                        <h4 style="font-size: 18px;color: red;">Click Here for Login Below:</h4>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="color: #ffffff; font-size: 14px;" align="center">
                            <?php 
                                $url = 'http://localhost:3000/login';
                            ?>
                            <a href="{{$url}}"><img src="{{url('/public')}}/images/emailimages/login.png" style="display: block;background:none;" alt="" border="0"></a>
                        </td>
                    </tr>

                    <tr>
                        <td height="5" style="font-size: 10px; line-height: 10px;">&nbsp;</td>
                    </tr>

                    <tr>
                        <td align="center">
                            <table border="0" align="center" width="160" cellpadding="0" cellspacing="0">

                                <tr>
                                    <td height="10" style="font-size: 10px; line-height: 10px;">&nbsp;</td>
                                </tr>

                                <tr>
                                    <td height="30" style="font-size: 30px; line-height: 30px;">&nbsp;</td>
                                </tr>
                                
                               <tr>
                                    <td height="30" style="font-size: 30px; line-height: 30px;">&nbsp;</td>
                                </tr>

                            </table>
                        </td>
                    </tr>


                </table>

            </td>
        </tr>

    </table>

    <!-- end section -->

    <!-- ======= divider ======= -->

    <table border="0" align="center" width="600" cellpadding="0" cellspacing="0" style="font-family: 'Raleway', sans-serif;">

        <tr class="hide">
            <td height="10" style="font-size: 10px; line-height: 10px;">&nbsp;</td>
        </tr>
      

        <tr>
            <td align="center" style="color: #ffffff; font-size: 14px; line-height: 26px;">
                <a href="https://www.facebook.com/vipparcel"><img src="{{url('/public')}}/images/emailimages/facebook.png" style="display:inline-block;background:none;"  border="0"  alt=""></a>
                <a href="#"><img src="{{url('/public')}}/images/emailimages/pint.png" style="display:inline-block;background:none;"  border="0"  alt=""></a>
                <a href="#"><img src="{{url('/public')}}/images/emailimages/in.png" style="display:inline-block;background:none;"  border="0"  alt=""></a>
                <a href="#"><img src="{{url('/public')}}/images/emailimages/google.png" style="display:inline-block;background:none;"  border="0"  alt=""></a>
                <a href="https://twitter.com/VIPparcel"><img src="{{url('/public')}}/images/emailimages/twiter-img.png" style="display:inline-block;background:none;"  border="0"  alt=""></a>

                </td>
           </tr>
        <tr>
            <td height="20" style="font-size: 20px; line-height: 20px;">&nbsp;</td>
        </tr>

    </table>


    

    <!-- footer ====== -->
    <table border="0" width="100%" cellpadding="0" cellspacing="0" >

        <tr>
            <td height="25" style="font-size: 25px; line-height: 25px;">&nbsp;</td>
        </tr>

        <tr>
            <td align="center">

                <table border="0" align="center" width="600" cellpadding="0" cellspacing="0" class="container590">

                    <tr>
                        <td>
                          

                            <table border="0" align="left" width="5" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="container590">
                                <tr>
                                    <td height="20" width="5" style="font-size: 20px; line-height: 20px;">&nbsp;</td>
                                </tr>
                            </table>

                            <table border="0" align="center" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="container590">

                                <tr>
                                    <td align="center">
                                        <table align="center" border="0" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td align="center">
                        
                         
                                    <a style="font-size: 15px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px;color: #333333; text-decoration: none;font-weight:400; display: inline-block;    border-bottom: 1px solid #999999;" href="#">Unsubscribe Emails </a>
                                     <a style="font-size: 15px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px;color: #333333; text-decoration: none;font-weight:400; display: inline-block;" href="#">|</a>
                            <a style="font-size: 15px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px;color: #333333; text-decoration: none;font-weight:400; display: inline-block;    border-bottom: 1px solid #999999;" href="#">Privacy</a>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                            </table>
                        </td>
                    </tr>

                </table>
            </td>
        </tr>

        <tr>
            <td height="25" style="font-size: 25px; line-height: 25px;">&nbsp;</td>
        </tr>

    </table>
    <!-- end footer ====== -->

</body>

</html>